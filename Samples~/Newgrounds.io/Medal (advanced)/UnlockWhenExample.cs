﻿using UnityEngine;

namespace io.newgrounds.unity.samples
{
    [RequireComponent(typeof(MedalComponent))]
    public class UnlockWhenExample : MonoBehaviour
    {
        [SerializeField, Tooltip("The minimal value of this object's 'X' position until a medal is unlocked"), Range(1, 50)]
        float requiredTransformX = 20;

        MedalComponent medal;

        void Awake()
        {
            medal = GetComponent<MedalComponent>();
            medal.UnlockWhen(Decide);
        }

        bool Decide()
        {
            return transform.position.x > requiredTransformX;
        }

        public void Yell(string what)
        {
            print(what);
        }
    }

}