﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace io.newgrounds.unity.samples
{
    public class Trigger : MonoBehaviour
    {
        [Space(10)] [SerializeField] MedalPopup popup = default;

        [Space(10)]

        // Icon of the medal.
        [SerializeField] Dropdown medalIconField = default;
        // Medal's name
        [SerializeField] InputField medalNameField = default;
        // Medal's point value
        [SerializeField] Dropdown medalPointsField = default;

        [SerializeField] Button unlockButton = default;
        [SerializeField] Button addButton = default;
        [SerializeField] Button runQueueButton = default;
        Text runQueueText;
        string runQueueBaseText;

        readonly List<MedalData> queuedMedalData = new List<MedalData>();

        [Space(10)] 
        [SerializeField] Button[] realMedalButtons = new Button[3];
        readonly Queue<int> realIDQueue = new Queue<int>();

        void Start()
        {
            unlockButton.onClick.AddListener(DisplayPreviewMedal);
            addButton.onClick.AddListener(AddToPreviewQueue);
            runQueueButton.onClick.AddListener(RunPreviewQueue);
            
            InitRealMedalButtons();
            runQueueText = runQueueButton.GetComponentInChildren<Text>(includeInactive: true);
            runQueueBaseText = runQueueText.text;
        }

        /// <summary> Initializes the buttons which unlock actual medals in the demo. </summary>
        void InitRealMedalButtons()
        {
            // TODO: Separate queuing from this sample component
            
            foreach (Button medalButton in realMedalButtons)
            {
                var medal = medalButton.GetComponent<MedalComponent>();
                if (medal == null)
                {
                    Debug.LogWarning($"The button {medalButton.name} is supposed to unlock an actual medal, " +
                                     "but has no <i>MedalComponent</i> on it.", medalButton);
                    continue;
                }
                MedalData medalData = medal.medalData;
                if (medalData == null)
                {
                    GameObject medalObject = medal.gameObject;
                    Debug.LogError($"The <i>MedalComponent</i> of {medalObject.name} has no assigned MedalData.", medalObject);
                    continue;
                }
                medalButton.onClick.AddListener(() =>
                {
                    queuedMedalData.Add(medalData);
                    runQueueText.text = $"{runQueueBaseText} ({queuedMedalData.Count})";
                    realIDQueue.Enqueue(medalData.Id);
                });
            }
            
        }

        void DisplayPreviewMedal()
        {
            MedalData medalData = CreareMedalData();
            popup.ShowPopup(medalData);
        }

        void AddToPreviewQueue()
        {
            queuedMedalData.Add(CreareMedalData());
            runQueueText.text = runQueueBaseText + " (" + queuedMedalData.Count + ")";
        }

        void RunPreviewQueue()
        {
            foreach (var medalData in queuedMedalData)
            {
                popup.ShowPopup(medalData);
                RealMedalUnlockCheck();
            }
            queuedMedalData.Clear();
            runQueueText.text = runQueueBaseText;
        }

        void RealMedalUnlockCheck()
        {
            if (realIDQueue.Count > 0)
            {
                UnlockMedal(realIDQueue.Dequeue());
            }
        }

        /// <summary> Creates a fake runtime <see cref="MedalData"/> from chosen values in the fields. </summary>
        MedalData CreareMedalData()
        {
            int medalPoints = new[] {5, 10, 25, 50, 100}[medalPointsField.value];
            var medalData = ScriptableObject.CreateInstance<MedalData>();
            medalData.SetMedalData(medalIconField.options[medalIconField.value].image, medalNameField.text, MedalData.GetDifficulty(medalPoints));
            return medalData;
        }

        /// <summary> Attempts to unlock an actual medal predefined on Newgrounds. </summary>
        /// <param name="medalId"> The numeric ID of the medal. </param>
        static void UnlockMedal(int medalId)
        {
            var medalUnlock = new components.Medal.unlock {id = medalId};
            Debug.LogFormat("Trying to unlock medal '{0}' with {1} for user '{2}'.", medalId, CoreComponent.ngioCore.name, CoreComponent.ngioCore.current_user.name);
            medalUnlock.callWith(CoreComponent.ngioCore, OnMedalUnlocked);
        }

        // This will get called whenever a medal gets unlocked via UnlockMedal()
        static void OnMedalUnlocked(results.Medal.unlock result)
        {
            objects.medal medal = result.medal;
            if (result.success)
            {
                Debug.LogFormat("Medal unlocked: '{0}' ({1} points)", medal.name, medal.value);
            }
            else
            {
                // Most commonly no Ngio core in the scene, or no user session (see Setup Wizard)
                Debug.LogWarningFormat("The medal failed to unlock. {0}", result.error.message);
            }
        }
    }
}
