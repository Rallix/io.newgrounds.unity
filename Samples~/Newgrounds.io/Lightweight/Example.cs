﻿using UnityEngine;

namespace io.newgrounds.unity.samples
{
    public class Example : MonoBehaviour
    {

        [Header("Newgrounds.io core")] public core ngio_core;

        // User currently playing the game
        objects.user user;

        [Header("Medal IDs")] [SerializeField] int startMedalTest = default;
        [SerializeField] int clickMedalTest = default;

        // Check if the user has a saved login when your game starts
        void Start()
        {
            // Wait until the core has been initialized
            ngio_core.onReady(() =>
            {
                unlockMedal(startMedalTest);

                // Call the server to check login status
                ngio_core.checkLogin(logged_in =>
                {
                    if (logged_in)
                    {
                        onLoggedIn();
                    }
                    else
                    {
                        /*
                     * This is where you would ask them if the want to sign in.
                     * If they want to sign in, call requestLogin()
                     */
                    }
                });
            });
        }

        void OnGUI()
        {
            if (user != null)
            {
                GUI.Label(new Rect(10, 10, 300, 20), "Your name is " + user.name + ".");
            }
            else GUI.Label(new Rect(10, 10, 300, 20), "You are not logged in.");

            if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 15, 200, 30), "Unlock medal"))
            {
                unlockMedal(clickMedalTest);
            }
        }

        // This will get called whenever a medal gets unlocked via unlockMedal()
        void onMedalUnlocked(results.Medal.unlock result)
        {
            objects.medal medal = result.medal;
            Debug.LogFormat("Medal unlocked: {0} ({1} points)", medal.name, medal.value);
        }

        // call this method whenever you want to unlock a medal.
        void unlockMedal(int medal_id)
        {
            // create the component
            var medal_unlock = new components.Medal.unlock
            {
                    id = medal_id // set required parameters
            };

            // call the component on the server, and tell it to fire onMedalUnlocked() when it's done.
            medal_unlock.callWith(ngio_core, onMedalUnlocked);
        }

        void onLoginChecked(bool is_logged_in)
        {
            // do something
        }

        void checkLogin()
        {
            ngio_core.checkLogin(onLoginChecked);
        }

        // Gets called when the player is signed in.
        void onLoggedIn()
        {
            // Do something. You can access the player's info with:
            user = ngio_core.current_user;
        }

        // Gets called if there was a problem with the login (expired sessions, server problems, etc).
        void onLoginFailed()
        {
            // Do something. You can access the login error with:
            objects.error error = ngio_core.login_error;
            Debug.Log($"<b>Login failed</b>: {error.message}", gameObject);
        }

        // Gets called if the user cancels a login attempt.
        void onLoginCancelled()
        {
            // Do something...
        }

        // When the user clicks your log-in button
        void requestLogin()
        {
            // This opens passport and tells the core what to do when a definitive result comes back.
            ngio_core.requestLogin(onLoggedIn, onLoginFailed, onLoginCancelled);
        }

        /*
         * You should have a 'cancel login' button in your game and have it call this, just to be safe.
         * If the user simply closes the browser tab rather than clicking a button to cancel, we won't be able to detect that.
         */
        void cancelLogin()
        {
            /*
         * This will call onLoginCancelled because you already added it as a callback via ngio_core.requestLogin()
         * for ngio_core.onLoginCancelled()
         */
            ngio_core.cancelLoginRequest();
        }

        // And finally, have your 'sign out' button call this
        void logOut()
        {
            ngio_core.logOut();
        }

    }

}
