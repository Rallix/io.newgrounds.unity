﻿using UnityEngine;
using UnityEngine.UI;

using System.Linq;
using System.Collections.Generic;

namespace io.newgrounds.unity.samples {

    /// <summary> An example of a component making use of the <see cref="SpyglassComponent"/>. </summary>
    public class UserDataDisplay : MonoBehaviour
    {
        [SerializeField, HideInInspector] SpyglassComponent spyglass;

        /// <summary> The user to retrieve information about. </summary>
        [SerializeField, Tooltip("The user to retrieve information about.")] InputField usernameField = default;
        const string DefaultUser = "TomFulp";

        /// <summary>The icon of the specified user.</summary>
        [SerializeField] RawImage userImage = default;
        [SerializeField] Button loadStatsButton = default;
        [Space]
        [SerializeField] GameObject labels = default;
        [SerializeField] GameObject values = default;

        void Start()
        {
            spyglass.OnStatsLoaded += OnStatsLoaded; // C# event
            spyglass.onIconLoaded.AddListener(OnIconLoaded); // Unity Event
            
            loadStatsButton.onClick.AddListener(LoadUser);
        }

        void OnValidate()
        {
            // Cache *serialized* fields in the Editor,
            // instead of every time when entering the playmode
            spyglass = GetComponent<SpyglassComponent>();
        }

        /// <summary> Resets the <see cref="InputField"/> to the default user. </summary>
        public void Reset()
        {
            usernameField.text = DefaultUser;
        }

        /// <summary> Loads info about the user whose name is currently in the <see cref="InputField"/>. </summary>
        public void LoadUser()
        {
            loadStatsButton.interactable = false;
            spyglass.LoadStats(usernameField.text);
        }

        /// <summary> A method called when all textual stats about a user are loaded. </summary>
        /// <remarks>
        ///     Not every user has every stat, so you should provide some sort of default values.
        ///     <see cref="SpyglassComponent.HasStat"/> can be used to check for emptiness.
        /// </remarks>
        void OnStatsLoaded()
        {
            IEnumerable<UserStats.Stats> stats = spyglass.UserStats.Keys;

            Text[] statLabels = labels.GetComponentsInChildren<Text>();
            Text[] statValues = values.GetComponentsInChildren<Text>();

            // Random labels
            List<UserStats.Stats> randomStats = stats.Where(stat => spyglass.HasStat(stat))
                                                     .OrderBy(stat => Random.value).ToList();

            for (int i = 0; i < statValues.Length && i < statLabels.Length; i++)
            {
                statLabels[i].text = randomStats[i].ToString();
                statValues[i].text = spyglass.UserStats[randomStats[i]];
            }

            loadStatsButton.interactable = true;
        }

        /// <summary> . </summary>
        /// <param name="avatar"></param>
        void OnIconLoaded(Texture2D avatar)
        {
            userImage.texture = avatar;
        }
    }
}