---
uid: third-party
---

### Third Party Notice

The repository license does *not* apply to the following libraries included in the project, therefore you should read their own licenses and terms.

* The original [Newgrounds.io](https://www.newgrounds.io/) library by [Josh Tuttle](https://psychogoldfish.newgrounds.com/)
  > **[MIT License](https://bitbucket.org/newgrounds/newgrounds.io-for-unity-c/src/master/LICENSE.md)**
* Newgrounds icons, other site resources…
  > See Newgrounds **[Terms of Use](https://www.newgrounds.com/wiki/help-information/terms-of-use?path=/wiki/help-information/terms-of-use)**

* [**UniGif**](https://github.com/WestHillApps/UniGif) by WestHillApps (Hironari Nishioka) is used here for downloading user icons in a GIF format.
  > **[MIT License](https://github.com/WestHillApps/UniGif/blob/master/LICENSE)**
* [**DotNetZip**](https://archive.codeplex.com/?p=dotnetzip) is used for zipping the project after the build.
  >**Can I distribute the binary DLL with my own project?**  
Yes - that's allowed by the license.   