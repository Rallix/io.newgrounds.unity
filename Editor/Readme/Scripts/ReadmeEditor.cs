﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Reflection;
using Object = UnityEngine.Object;

namespace io.newgrounds.unity.editor
{
    [InitializeOnLoad]
    [CustomEditor(typeof(Readme))]
    class ReadmeEditor : Editor
    {
        static string kShowedReadmeSessionStateName = "ReadmeEditor.showedReadme";

        static float kSpace = 16f;

        static ReadmeEditor()
        {
            EditorApplication.delayCall += SelectReadmeAutomatically;
        }

        static void SelectReadmeAutomatically()
        {
            if (!SessionState.GetBool(kShowedReadmeSessionStateName, false))
            {
                Readme readme = SelectReadme();
                SessionState.SetBool(kShowedReadmeSessionStateName, true);

                if (readme && !readme.loadedLayout)
                {
                    LoadLayout();
                    readme.loadedLayout = true;
                }
            }
        }

        static void LoadLayout()
        {
            Assembly assembly = typeof(EditorApplication).Assembly;
            Type windowLayoutType = assembly.GetType("UnityEditor.WindowLayout", true);
            MethodInfo method = windowLayoutType.GetMethod("LoadWindowLayout", BindingFlags.Public | BindingFlags.Static);
            string layoutPath = Path.Combine(Application.dataPath.Substring(0, Application.dataPath.Length - "/Assets".Length), "Packages/newgrounds.io/Editor/Readme/Layout.wlt");
            if (method != null) method.Invoke(null, new object[] {layoutPath, false});
            else Debug.LogException(new NullReferenceException("Cannot find the UnityEditor.WindowLayout.LoadWindowLayout method."));
        }

        // [MenuItem("Tutorial/Show Tutorial Instructions")]
        static Readme SelectReadme()
        {
            string[] ids = AssetDatabase.FindAssets("Readme t:Readme");
            if (ids.Length == 1)
            {
                Object readmeObject = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GUIDToAssetPath(ids[0]));

                Selection.objects = new[] {readmeObject};

                return (Readme) readmeObject;
            }
            else if (ids.Length == 0)
            {
                Debug.Log("Couldn't find any readme file.");
            }
            return null;
        }

        protected override void OnHeaderGUI()
        {
            var readme = (Readme) target;
            Init();

            float iconWidth = Mathf.Min(EditorGUIUtility.currentViewWidth / 3f - 20f, 128f);

            GUILayout.BeginHorizontal("In BigTitle");
            {
                GUILayout.Label(readme.icon, GUILayout.Width(iconWidth), GUILayout.Height(iconWidth));
                GUILayout.Label(readme.title, TitleStyle);
            }
            GUILayout.EndHorizontal();
        }

        public override void OnInspectorGUI()
        {
            var readme = (Readme) target;
            Init();

            foreach (Readme.Section section in readme.sections)
            {
                if (!string.IsNullOrEmpty(section.heading))
                {
                    GUILayout.Label(section.heading, HeadingStyle);
                }
                if (section.image != null)
                {
                    var content = new GUIContent(section.image);
                    var style = new GUIStyle
                    {
                            stretchWidth = true,
                            stretchHeight = false,
                            imagePosition = ImagePosition.ImageOnly,
                            alignment = TextAnchor.MiddleLeft
                    };
                    GUILayout.Box(content, style, GUILayout.MaxWidth(400), GUILayout.MaxHeight(250));
                }
                if (!string.IsNullOrEmpty(section.text))
                {
                    GUILayout.Label(section.text, BodyStyle);
                }
                if (!string.IsNullOrEmpty(section.linkText))
                {
                    if (LinkLabel(new GUIContent(section.linkText)))
                    {
                        Application.OpenURL(section.url);
                    }
                }
                GUILayout.Space(kSpace);
            }
        }


        bool m_Initialized;

        GUIStyle LinkStyle
        {
            get { return m_LinkStyle; }
        }

        [SerializeField] GUIStyle m_LinkStyle;

        GUIStyle TitleStyle
        {
            get { return m_TitleStyle; }
        }

        [SerializeField] GUIStyle m_TitleStyle;

        GUIStyle HeadingStyle
        {
            get { return m_HeadingStyle; }
        }

        [SerializeField] GUIStyle m_HeadingStyle;

        GUIStyle ImageStyle
        {
            get { return m_ImageStyle; }
        }

        [SerializeField] GUIStyle m_ImageStyle;

        GUIStyle BodyStyle
        {
            get { return m_BodyStyle; }
        }

        [SerializeField] GUIStyle m_BodyStyle;

        void Init()
        {
            if (m_Initialized) return;
            m_BodyStyle = new GUIStyle(EditorStyles.label) {wordWrap = true, fontSize = 14, richText = true};
            m_TitleStyle = new GUIStyle(m_BodyStyle) {fontSize = 26};
            m_HeadingStyle = new GUIStyle(m_BodyStyle) {fontSize = 18};
            m_ImageStyle = new GUIStyle(m_BodyStyle) {alignment = TextAnchor.MiddleLeft};
            m_LinkStyle = new GUIStyle(m_BodyStyle)
            {
                    wordWrap = false,
                    // Match selection color which works nicely for both light and dark skins
                    normal = {textColor = new Color32(0x00, 0x78, 0xDA, 0xFF)},
                    stretchWidth = false
            };

            m_Initialized = true;
        }

        bool LinkLabel(GUIContent label, params GUILayoutOption[] options)
        {
            Rect position = GUILayoutUtility.GetRect(label, LinkStyle, options);

            Handles.BeginGUI();
            Handles.color = LinkStyle.normal.textColor;
            Handles.DrawLine(new Vector3(position.xMin, position.yMax), new Vector3(position.xMax, position.yMax));
            Handles.color = Color.white;
            Handles.EndGUI();

            EditorGUIUtility.AddCursorRect(position, MouseCursor.Link);

            return GUI.Button(position, label, LinkStyle);
        }
    }
}
