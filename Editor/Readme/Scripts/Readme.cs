﻿using System;
using UnityEngine;
#pragma warning disable 0649 // Unity serialization

namespace io.newgrounds.unity.editor
{
    [CreateAssetMenu(fileName = "Readme", menuName = "Newgrounds/Readme File", order = int.MinValue)]
    class Readme : ScriptableObject
    {
        public Texture2D icon;
        public string title = "Readme";
        public Section[] sections = {};
        public bool loadedLayout;

        [Serializable]
        public class Section
        {
            public string heading;
            public Texture2D image;
            public string text;
            public string linkText;
            public string url;
        }
    }

}