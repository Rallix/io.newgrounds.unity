﻿using System.Linq;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

namespace io.newgrounds.unity.editor
{
    /// <summary> The preview of the <see cref="MedalData"/> asset. </summary>
    [CustomPreview(typeof(MedalData))]
    class MedalDataPreview : ObjectPreview
    {

        /// <summary> Check if the object is allowed to have a preview window. </summary>
        public override bool HasPreviewGUI()
        {
            return medalPopupTexture != null && Selection.objects.Length == 1; // TODO: Allow multiple
        }


        Texture medalPopupTexture;

        Font medalFont;
        Color fontColor = new Color32(0x95, 0x85, 0x6A, byte.MaxValue);
        GUIStyle medalFontStyle;

        MedalData[] currentMedals;

        /// <summary>
        ///   <para>Called when the Preview gets created with the objects being previewed.</para>
        /// </summary>
        /// <param name="targets">The objects being previewed.</param>
        public override void Initialize(Object[] targets)
        {
            if (!medalPopupTexture) InitializeMedalPopupTexture();
            if (!medalFont) InitializePopupFont();

            currentMedals = targets.Cast<MedalData>().ToArray();
            base.Initialize(targets);
        }

        /// <summary> Draw the preview window. </summary>
        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            if (Event.current.type == EventType.Repaint) DrawMedalPopup(r, currentMedals[0]);
        }

        void DrawMedalPopup(Rect r, MedalData medal)
        {
            GUI.DrawTexture(r, medalPopupTexture, ScaleMode.ScaleToFit);
            Rect previewRect = GUILayoutUtility.GetLastRect();

            Rect medalRect = GetTextureRect(medalPopupTexture, previewRect);

            var textureReferenceSize = new Vector2(250, 72);
            if (!string.IsNullOrWhiteSpace(medal.Name))
            {
                Rect textRect = GetNameRect(medalRect, textureReferenceSize, out GUIStyle textStyle);
                GUI.Label(textRect, medal.Name, textStyle);
            }
            if (medal.Points > 0)
            {
                Rect pointsRect = GetPointsRect(medalRect, textureReferenceSize, out GUIStyle textStyle);
                GUI.Label(pointsRect, medal.Points.ToString(), textStyle);
            }
            if (medal.Icon)
            {
                Rect iconRect = GetIconRect(medalRect, textureReferenceSize);
                GUI.DrawTexture(iconRect, medal.Icon.ToTexture(), ScaleMode.StretchToFill);
            }
        }

        void InitializeMedalPopupTexture()
        {
            string medalPopupTexturePath = NgEditorUtils.FindNgAssets("t:Texture Medal Popup",
                                                                      "Unity/Components/Medal/Medal Popup")
                                                        .FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(medalPopupTexturePath))
            {
                medalPopupTexture = AssetDatabase.LoadAssetAtPath<Texture>(medalPopupTexturePath);
            }
        }

        /// <summary> Gets the actual rectangle of a texture drawn inside another rectangle. </summary>
        /// <param name="texture">The texture being drawn.</param>
        /// <param name="previewRect">The rectangle inside which the texture is drawn.</param>
        /// <remarks> Mainly intended to get the actual rectangle of <see cref="GUI.DrawTexture(Rect, Texture, ScaleMode)"/></remarks>
        static Rect GetTextureRect(Texture texture, Rect previewRect)
        {
            float widthMultiplier = previewRect.width / texture.width;
            float heightMultiplier = previewRect.height / texture.height;

            // The preview box draws the texture over the full width and leaves space above and below
            // Once there's no space above and below, it starts shrinking the width

            var size = new Vector2(texture.width * heightMultiplier, texture.height * widthMultiplier);
            Vector2 position = previewRect.position;

            if (widthMultiplier < heightMultiplier)
            {
                size.x = Mathf.Clamp(size.x, 0, previewRect.size.x);
                position.y += (previewRect.size.y - size.y) / 2;
            }
            else
            {
                size.y = Mathf.Clamp(size.y, 0, previewRect.size.y);
                position.x = (previewRect.size.x - size.x) / 2;
            }
            return new Rect(position, size);
        }

        Rect GetNameRect(Rect medalRect, Vector2 referenceSize, out GUIStyle textStyle)
        {
            var nameRect = new Rect(71f, 35f, 168f, 24f);
            float fontSizeMultiplier = nameRect.height.RemapRange0F(referenceSize.y, medalRect.height);
            textStyle = new GUIStyle(GUI.skin.label)
            {
                    font = medalFont,
                    alignment = TextAnchor.MiddleLeft,
                    fontSize = Mathf.RoundToInt(fontSizeMultiplier),
                    normal = new GUIStyleState {textColor = fontColor}
            };

            return nameRect.RemapRect(medalRect, referenceSize);
        }

        Rect GetPointsRect(Rect medalRect, Vector2 referenceSize, out GUIStyle textStyle)
        {
            var pointsRect = new Rect(180f, 10f, 40f, 15f);

            float fontSizeMultiplier = pointsRect.height.RemapRange0F(referenceSize.y, medalRect.height);
            textStyle = new GUIStyle(GUI.skin.label)
            {
                    font = medalFont,
                    alignment = TextAnchor.MiddleRight,
                    fontSize = Mathf.RoundToInt(fontSizeMultiplier * 0.8f),
                    normal = new GUIStyleState {textColor = fontColor}
            };

            return pointsRect.RemapRect(medalRect, referenceSize);
        }

        static Rect GetIconRect(Rect medalRect, Vector2 referenceSize)
        {
            var iconRect = new Rect(9f, 9f, 52f, 52f);
            return iconRect.RemapRect(medalRect, referenceSize);
        }

        void InitializePopupFont()
        {
            string popupFontPath = NgEditorUtils.FindNgAssets("t:Font TekutekuAL",
                                                              "Unity/Data/Fonts")
                                                .FirstOrDefault();
            if (string.IsNullOrWhiteSpace(popupFontPath)) return;

            medalFont = AssetDatabase.LoadAssetAtPath<Font>(popupFontPath);
        }
    }
}
