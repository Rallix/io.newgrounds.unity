﻿using UnityEditor;

namespace io.newgrounds.unity.editor
{
    /// <summary> A shared custom editor base for all Newgrounds components. </summary>
    [CustomEditor(typeof(NewgroundsComponent))]
    class NewgroundsComponentEditor : Editor
    {
        [MenuItem("CONTEXT/NewgroundsComponent/Open Settings")]
        static void OpenPreferences() => settings.NewgroundsSettings.OpenSettings();

        [MenuItem("CONTEXT/NewgroundsComponent/Open Window")]
        static void OpenWindow() => NewgroundsMenuTools.OpenWelcomeWindow();
    }
}
