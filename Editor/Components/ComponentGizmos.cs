﻿using System;
using UnityEditor;
using UnityEngine;

// ReSharper disable UnusedParameter.Local
// ReSharper disable BitwiseOperatorOnEnumWithoutFlags

namespace io.newgrounds.unity.editor
{
    /// <summary>A class handling drawing <see cref="Gizmos"/> for components.</summary>
    static class ComponentGizmos
    {

        const float GizmosHideDistance = 10f;
        const GizmoType SelectedOnly = GizmoType.Selected | GizmoType.Active;

        [DrawGizmo(SelectedOnly)]
        static void CoreGizmos(CoreComponent core, GizmoType gizmoType)
        {
            Vector3 position = core.transform.position;

            if (Vector3.Distance(position, Camera.current.transform.position) > GizmosHideDistance)
            {
                Gizmos.DrawIcon(position, GetGizmosIconPath<CoreComponent>());
            }
        }

        /// <summary>Gets the presumed path to a <see cref="Gizmos"/> icon given its <see langword="namespace"/> and <see langword="class"/> name.
        /// It's not guaranteed that the path actually contains the icon.</summary>
        /// <typeparam name="T"><see cref="Component"/> type</typeparam>
        /// <returns>The presumed path to a <see cref="Gizmos"/> icon.</returns>
        static string GetGizmosIconPath<T>() where T : Component
        {
            Type type = typeof(T);
            string @namespace = type.Namespace;
            string subfolder = (!string.IsNullOrEmpty(@namespace)) ? @namespace.Replace(".", "/") + "/" : string.Empty;

            return string.Format("{0}{1} Icon.png", subfolder, typeof(T).Name); // = io/newgrounds/unity/CoreComponent Icon.png
        }

    }
}
