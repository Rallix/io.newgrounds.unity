﻿using System;
using System.Linq;
using io.newgrounds.unity.editor.icons;
using UnityEditor;
using UnityEngine;
using SmallIcons = io.newgrounds.unity.editor.icons.NewgroundsIcons.SmallIcons;

namespace io.newgrounds.unity.editor
{
    [CustomEditor(typeof(CoreComponent))]
    class CoreEditor : Editor
    {
        // TODO: Refactor into parts

        CoreComponent core;                         // Target of this editor script        
        static readonly bool GlobalSettings = true; // All changes are applied to all core instances (resp. to the prefab)        

        // Buttons        
        const string ExportPackageName = "Newgrounds.io for Unity";

        // API Tools
        bool apiToolsFoldout = true;

        // Switches         
        bool switchesFoldout = true;

        // Warnings        
        bool alreadyOneCore;

        SerializedProperty app_id;
        SerializedProperty aes_base64_key;

        SerializedProperty output_network_data;

        void OnEnable()
        {
            core = target as CoreComponent;

            app_id = serializedObject.FindProperty(nameof(core.app_id));
            aes_base64_key = serializedObject.FindProperty(nameof(core.aes_base64_key));

            output_network_data = serializedObject.FindProperty(nameof(core.output_network_data));

            if (GlobalSettings) core = target as CoreComponent;
            else
            {
                core = CoreInitializer.CorePrefab;
            }
            if (FindObjectsOfType<CoreComponent>().Count(obj => obj.gameObject.activeInHierarchy) > 1)
            {
                alreadyOneCore = true;
            }

            // Add AppID and encryption key if it was added before            
            RefreshCoreSettings();
        }

        /// <summary> Applies settings from the Project Settings / Newgrounds. </summary>
        void ApplySettings()
        {
            core.app_id = settings.NgSettingsManager.GetAppId();
            core.aes_base64_key = settings.NgSettingsManager.GetEncryptonKey();

            // Apply to properties as well
            app_id.stringValue = settings.NgSettingsManager.GetAppId();
            aes_base64_key.stringValue = settings.NgSettingsManager.GetEncryptonKey();
            serializedObject.ApplyModifiedPropertiesWithoutUndo();
        }

        // Editor Prefs

        void RefreshCoreSettings()
        {
            if (core != null)
            {
                ApplySettings();
            }
            else
            {
                NgDebug.LogWarning("Attempting to refresh the settings of Newgrounds Core, but there's no active core component.");
            }
        }

        /// <summary>
        /// Periodically called function responsible for drawing and rendering all buttons, icons, fields etc. and handling user input.
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawGUI();
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary> The main method for drawing inspector elements. </summary>
        void DrawGUI()
        {
            EditorMethods.DisplayScriptField(core);
            EditorGUILayout.Space();
            DrawMultipleCoresWarning();
            // DrawInformationPanel(); // TODO: Keep & prune or remove
            DrawSwitches();
            DrawStatusBars();
            DrawAPITools();
        }

        void DrawMultipleCoresWarning()
        {
            if (alreadyOneCore && Selection.activeTransform)
            {
                EditorGUILayout.HelpBox("There are multiple Core components in the current scene.", MessageType.Error);
            }
        }

        void DrawInformationPanel()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                DrawNewgroundsLargeIcon();
                DrawLinkButtons();
            }
            EditorGUILayout.Space();
        }

        void DrawStatusBars()
        {
            using (new EditorGUI.DisabledScope(true))
            {
                bool ready = core.isReady();
                bool connected;
                try
                {
                    string username = core.current_user.name;
                    connected = !string.IsNullOrWhiteSpace(username);
                }
                catch (NullReferenceException)
                {
                    connected = false;
                }
                Color toggleColor = (connected) ? Color.cyan : Color.green;
                EditorMethods.ColorToggle(ready, ready ? (connected ? "Connected" : "Ready") : "Inactive", toggleColor);
            }
        }

        void DrawAPITools()
        {
            EditorMethods.HeaderField("API Tools", TextAnchor.MiddleCenter);
            EditorGUILayout.Space();
            apiToolsFoldout = EditorGUILayout.Foldout(apiToolsFoldout, apiToolsFoldout ? "Hide API Tools" : "Show API Tools");
            if (apiToolsFoldout)
            {
                EditorGUILayout.HelpBox("These settings can be changed in Project Settings > Newgrounds.", MessageType.None);
                using (new EditorGUI.DisabledScope(true))
                {
                    NgEditorUtils.IconTextField(SmallIcons.Puzzle, "App ID", core.app_id, GUI.skin.textField,
                                                "The unique ID of your app found in Project System.");

                    NgEditorUtils.IconTextField(SmallIcons.Key, "AES Encryption Key", core.aes_base64_key, GUI.skin.textField,
                                                "The encryption key of your app found in Project System.");
                    if (Application.isPlaying && !string.IsNullOrEmpty(core.session_id))
                    {
                        NgEditorUtils.IconTextField(SmallIcons.Rss, "Session ID", core.session_id, GUI.skin.textField,
                                                    "A unique session id used to identify the active user.");
                    }
                }
            }
        }

        void DrawSwitches()
        {
            EditorMethods.HeaderField("Switches", TextAnchor.MiddleCenter);
            EditorGUILayout.Space();
            switchesFoldout = EditorGUILayout.Foldout(switchesFoldout, switchesFoldout ? "Hide switches" : "Show switches");
            if (switchesFoldout)
            {
                DrawCoreModeToolbar();
                DrawToggles();
            }
        }

        //---------------------------------------------- Custom inspector fields
        
        static void DrawNewgroundsLargeIcon()
        {
            Texture2D icon = NewgroundsIcons.GetIcon(NewgroundsIcons.UtilityIcons.Tank);
            EditorMethods.IconBox(icon, new Vector2(150, 150), TextAnchor.MiddleLeft);
        }

        // TODO: This shouldn't be in the Inspector. move it to welcome window or elsewhere
        static void DrawLinkButtons()
        {
            var buttonStyle = new GUIStyle(GUI.skin.button) { alignment = TextAnchor.MiddleLeft, imagePosition = ImagePosition.ImageLeft };
            using (new EditorGUILayout.VerticalScope())
            {
                EditorGUILayout.Space();
                // Open Project System in browser
                if (GUILayout.Button(NgEditorUtils.GUIContentIcon("Project System", SmallIcons.Projects), buttonStyle))
                {
                    Application.OpenURL("https://www.newgrounds.com/projects/games");
                }
                // Open "Newgrounds.io Help" in browser
                if (GUILayout.Button(NgEditorUtils.GUIContentIcon("Introduction", SmallIcons.Link), buttonStyle))
                {
                    Application.OpenURL("https://www.newgrounds.io/help/");
                }
                // Open online documentation
                if (GUILayout.Button(NgEditorUtils.GUIContentIcon("Documentation", SmallIcons.Info), buttonStyle))
                {
                    Application.OpenURL("https://rallix.gitlab.io/io.newgrounds.unity/manual/index.html");
                }
                // Open Demo scene and instruction folder
                if (GUILayout.Button(NgEditorUtils.GUIContentIcon("Demo scene", SmallIcons.Star), buttonStyle))
                {
                    AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath<SceneAsset>(NgEditorUtils.NewgroundsFolder + "/Demo/Demo.unity"));
                }
                // Remove all
                if (GUILayout.Button(new GUIContent { text = "　See Package Manager", image = NewgroundsIcons.GetIcon(SmallIcons.Dump) }, buttonStyle))
                {
                    // This will wipe out the whole Newgrounds.io folder, irreversibly
                    if (EditorUtility.DisplayDialog("Remove Newgrounds.io", "Are you sure you want to remove the whole Newgrounds.io folder (" + NgEditorUtils.NewgroundsFolder + ") and its contents?\n\n" +
                                                                            "This is irreversible, unless you download and reimport the package.", "Remove all", "Cancel"))
                    {
                        settings.NgSettingsManager.Reset();
                        AssetDatabase.DeleteAsset(NgEditorUtils.NewgroundsFolder);
                        GlobalDefines.RemovePreprocessorDirective(GlobalDefines.PreprocessorDirective, true);
                    }
                }
            }
        }

        void DrawCoreModeToolbar()
        {
            string[] modeTooltip =
            {
                    "Adds a core instance in the first scene and keeps it through the whole game. In the editor, it's added whenever you enter the play mode.",
                    "Let's you handle adding and removing core instance(s) on your own. Without a core component, you won't be connected to Newgrounds and most other components won't work properly."
            };
            EditorGUILayout.HelpBox(modeTooltip[(int)CoreComponent.coreMode], MessageType.None);
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                CoreComponent.coreMode = (CoreComponent.CoreMode)GUILayout.Toolbar((int)CoreComponent.coreMode, new[]
                {
                        new GUIContent { image = NewgroundsIcons.GetIcon(SmallIcons.Move), text = "　" + CoreComponent.CoreMode.Automatic },
                        new GUIContent { image = NewgroundsIcons.GetIcon(SmallIcons.Fist), text = "　" + CoreComponent.CoreMode.Manual }
                });

                if (check.changed)
                {
                    // Debug.Log($"CoreMode changed to: {CoreComponent.coreMode}");
                    switch (CoreComponent.coreMode)
                    {
                        default:
                            goto case CoreComponent.CoreMode.Automatic;
                        case CoreComponent.CoreMode.Automatic:
                            core.dontDestroyOnLoad = true; // Automatic → never destroy on load                    
                            core.hideInHierarchy = true;   // Recommended to avoid unnecessary clutter in the scene
                            break;
                        case CoreComponent.CoreMode.Manual:
                            core.dontDestroyOnLoad = false; // Possible, but probably not what you want in Manual mode
                            core.hideInHierarchy = false;
                            break;
                    }
                    EditorPrefs.SetInt("coreMode", (int)CoreComponent.coreMode); // Save Core mode for later use            
                }
            }
        }

        void DrawToggles()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new EditorGUILayout.VerticalScope())
                {
                    using (new EditorGUI.DisabledScope(CoreComponent.coreMode == CoreComponent.CoreMode.Automatic))
                    {
                        core.dontDestroyOnLoad = EditorMethods.ColorToggle(core.dontDestroyOnLoad, "Don't destroy on load", Color.green);
                    }
                    core.hideInHierarchy = EditorMethods.ColorToggle(core.hideInHierarchy, "Hide in hierarchy", Color.cyan);
                }

                using (new EditorGUILayout.VerticalScope())
                {
                    core.output_network_data = EditorMethods.ColorToggle(output_network_data.boolValue, "Output network data", Color.yellow);
                    using (new EditorGUI.DisabledScope(true)) // RESERVED when needed
                    {
                        EditorMethods.ColorToggle(false, "　", Color.white);
                    }
                }
            }
        }
    }
}
