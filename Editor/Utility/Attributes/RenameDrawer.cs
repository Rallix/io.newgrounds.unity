using UnityEngine;
using UnityEditor;

namespace io.newgrounds.unity.editor
{
	/// <summary> A drawer which displays a different name of a variable in the Inspector than the auto-generated one. </summary>
	[CustomPropertyDrawer(typeof(RenameAttribute))]
	 class RenameDrawer : PropertyDrawer
	 {
		 RenameAttribute rename => (attribute as RenameAttribute);
		 
		 public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		 {
			 EditorGUI.PropertyField(position, property, new GUIContent(rename.NewName));			 
		 }
	 }
}
