﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace io.newgrounds.unity.editor
{
    /// <summary>Provides a handful of all-purpose helpful Editor methods.</summary>
    static class EditorMethods
    {
        /// <summary> Adds a component to all selected objects. </summary>
        /// <typeparam name="T">Type of a component or a script to add.</typeparam>
        internal static void AddComponentToSelected<T>() where T : MonoBehaviour
        {
            foreach (GameObject obj in Selection.gameObjects) obj.AddComponent(typeof(T));
        }

        /// <summary> Compute the approximate distance (similarity) between two strings. The less, the better. </summary>
        internal static int LevenshteinDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            var d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0) return m;
            if (m == 0) return n;

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++) { }
            for (int j = 0; j <= m; d[0, j] = j++) { }

            // Step 3
            for (int i = 1; i <= n; i++)
                    // Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = t[j - 1] == s[i - 1] ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                                       Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                                       d[i - 1, j - 1] + cost);
                }

            // Step 7
            return d[n, m];
        }

        /// <summary>Checks whether a number is a power of two.</summary>
        /// <param name="number">A number to test.</param>
        /// <returns>True if <paramref name="number" /> is a power of two.</returns>
        internal static bool IsPowerOfTwo(int number)
        {
            if (number < 0) return false;
            return number != 0 && (number & (number - 1)) == 0;
        }

        /// <summary>Checks whether a given file or folder name is valid.</summary>
        /// <param name="name">A file or folder name.</param>
        /// <returns>True if the <paramref name="name" /> is a valid file or folder name.</returns>
        internal static bool IsValidFilename(string name)
        {
            string invalidCharacters = new string(Path.GetInvalidFileNameChars());
            var invalidCharactersRegex = new Regex(string.Concat("[", Regex.Escape(invalidCharacters), "]"));

            return !invalidCharactersRegex.IsMatch(name);
        }

        /// <summary>Gets the path to the object excluding topmost <see cref="Transform" />.</summary>
        internal static string GetRootPath(Transform transform)
        {
            string fullPath = transform.GetPath().Remove(0, 1);

            int numberofSlashes = fullPath.Split('/').Length - 1;
            string rootPath;
            if (numberofSlashes > 1) rootPath = fullPath.Substring(fullPath.IndexOf('/') + 1);
            else if (numberofSlashes == 1) rootPath = fullPath.Split('/')[1];
            else rootPath = fullPath;
            return rootPath;
        }

        /// <summary>Displays a default readonly script field inside the editor Inspector.</summary>
        internal static void DisplayScriptField(MonoBehaviour monoBehaviour)
        {
            using (new EditorGUI.DisabledGroupScope(true))
            {
                MonoScript script = MonoScript.FromMonoBehaviour(monoBehaviour);
                EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false);
            }
        }

        /// <summary>Displays a default readonly script field inside the editor Inspector.</summary>
        internal static void DisplayScriptField(ScriptableObject scriptableObject)
        {
            using (new EditorGUI.DisabledGroupScope(true))
            {
                MonoScript script = MonoScript.FromScriptableObject(scriptableObject);
                EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false);
            }
        }

        /// <summary>Returns an icon of a given component type.</summary>
        /// <param name="type">The type of a <see cref="Component" /> to get an icon from.</param>
        /// <returns>A new <see cref="Texture" /> component icon (if it has one).</returns>
        internal static Texture GetComponentIcon(Type type)
        {
            GUIContent guiContent = EditorGUIUtility.ObjectContent(null, type);
            return guiContent.image;
        }

        /// <summary>Returns an icon of a given component type.</summary>
        /// <typeparam name="T">The type of a <see cref="Component" /> to get an icon from.</typeparam>
        /// <returns>A new <see cref="Texture" /> component icon (if it has one).</returns>
        internal static Texture GetComponentIcon<T>() where T : Component
        {
            return GetComponentIcon(typeof(T));
        }

        /// <summary>Make a bold label field with space above.</summary>
        /// <param name="label">The header text.</param>
        /// <param name="alignment">The header text's <see cref="TextAnchor" /> placement.</param>
        /// <param name="fontSize">The size of the header's font.</param>
        internal static void HeaderField(string label, TextAnchor alignment = TextAnchor.UpperLeft, int fontSize = 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(label, new GUIStyle(GUI.skin.label) {fontStyle = FontStyle.Bold, fontSize = fontSize, alignment = alignment});
        }

        /// <summary>Draws a horizontal line across the whole width of the inspector.</summary>
        internal static void HorizontalLine()
        {
            EditorGUILayout.LabelField(string.Empty, GUI.skin.horizontalSlider);
        }

        /// <summary>
        ///     Shorthand for a <see cref="GUILayout.Toggle(bool, string, GUILayoutOption[])" /> button with a
        ///     given color.
        /// </summary>
        /// <param name="active">True if the toggle is currently pressed.</param>
        /// <param name="label">Text on the toggle.</param>
        /// <param name="activeColor">The colour of the toggle when pressed.</param>
        /// <param name="options">An optional list of layout options that specify extra layouting properties.</param>
        /// <returns>The <paramref name="active" /> state of the toggle.</returns>
        internal static bool ColorToggle(bool active, string label, Color activeColor, params GUILayoutOption[] options)
        {
            GUI.color = active ? activeColor : Color.white;
            bool isActive = GUILayout.Toggle(active, label, GUI.skin.button);
            GUI.color = Color.white;

            return isActive;
        }

        /// <summary>Shorthand for a <see cref="GUILayout.Button(string, GUILayoutOption[])" />.</summary>
        /// <param name="color">The background colour of the button.</param>
        /// <param name="label">Text on the button.</param>
        /// <param name="options">An optional list of layout options that specify extra layouting properties.</param>
        /// <returns><c>True</c> if the button was pressed.</returns>
        internal static bool ColorButton(Color color, string label, params GUILayoutOption[] options)
        {
            Color oldColor = GUI.color;
            GUI.color = color;
            bool result = GUILayout.Button(label, options);
            GUI.color = oldColor;
            return result;
        }

        /// <summary>Make a text field whose label has an icon in a box.</summary>
        /// <param name="icon">The label icon.</param>
        /// <param name="label">A label to display in front of the text field.</param>
        /// <param name="text">The text to edit.</param>
        /// <param name="tooltip">The text displayed when the cursor hovers over the text label.</param>
        /// <param name="style">
        ///     The <see cref="GUIStyle" /> of the text field; defaults to the one defined by
        ///     <see cref="GUI.skin" />.
        /// </param>
        /// <param name="labelWidth">The reserved area for the label. The smaller it is, the wider is the text field.</param>
        /// <returns>The text entered by the user.</returns>
        internal static string IconTextField(Texture icon, string label, string text, GUIStyle style = null, string tooltip = "", float labelWidth = 145f)
        {
            if (style == null) style = GUI.skin.textField;

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Box(icon); // Icon
                
                using (new EditorGUILayout.VerticalScope())
                {
                    // Vertical space to align the text field with the icon
                    if (icon) GUILayout.Space(Mathf.Round(icon.height / 2f));

                    float oldWidth = EditorGUIUtility.labelWidth;
                    EditorGUIUtility.labelWidth = labelWidth;
                    var guiContent = new GUIContent(label, tooltip);
                    text = EditorGUILayout.TextField(guiContent, text, style);
                    EditorGUIUtility.labelWidth = oldWidth;
                }
            }
            return text;
        }

        /// <summary>Draws a box with an icon in it.</summary>
        /// <param name="icon">The icon to show.</param>
        /// <param name="iconSize">The size of the icon (width, height).</param>
        /// <param name="alignment">The alignment of the icon within the given area.</param>
        internal static void IconBox(Texture2D icon, Vector2 iconSize, TextAnchor alignment)
        {
            var iconContent = new GUIContent
            {
                    image = icon
            };
            var iconStyle = new GUIStyle(GUI.skin.box)
            {
                    alignment = alignment,
                    fixedWidth = iconSize.x,
                    fixedHeight = iconSize.y
            };

            EditorGUILayout.LabelField(iconContent, iconStyle);
        }

        internal static int IndentedIntField(GUIContent label, int value, GUIStyle style, float labelWidth = 145f, params GUILayoutOption[] options)
        {
            using (new EditorGUILayout.VerticalScope())
            {
                // Vertical space to align the label
                GUILayout.Space(GUI.skin.button.lineHeight / 2f);

                float oldWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = labelWidth;
                value = EditorGUILayout.IntField(label, value, style, options);
                EditorGUIUtility.labelWidth = oldWidth;
            }
            return value;
        }

        /// <summary>Make a help box with a message to the user, indented from top and bottom.</summary>
        /// <param name="message">The message text.</param>
        /// <param name="type">The type of message.</param>
        /// <param name="space">The space inserted in the current layout group.</param>
        internal static void IndentedHelpBox(string message, MessageType type, float space = 0)
        {
            GUILayout.Space(space);
            EditorGUILayout.HelpBox(message, type);
            GUILayout.Space(space);
        }

        /// <summary>Gets a window and docks it to a specified window.</summary>
        /// <typeparam name="T">The type of <see cref="EditorWindow" /> to create.</typeparam>
        /// <param name="windowToDockNextTo">The name of a window class within the <see cref="UnityEditor" /> namespace.</param>
        /// <returns>A new, docked window.</returns>
        internal static T GetDockedWindow<T>(string windowToDockNextTo) where T : EditorWindow
        {
            T window;
            if (!string.IsNullOrEmpty(windowToDockNextTo) && windowToDockNextTo != "None")
                try
                {
                    Type inspectorType = Type.GetType($"UnityEditor.{windowToDockNextTo}, UnityEditor.dll", true);
                    window = EditorWindow.GetWindow<T>(inspectorType);
                }
                catch (TypeLoadException)
                {
                    Debug.LogWarningFormat("A window type <b>{0}</b> to dock to couldn't be found.", windowToDockNextTo);
                    window = EditorWindow.GetWindow<T>();
                }
            else window = EditorWindow.GetWindow<T>();

            return window;
        }

        /// <summary> Gets the file size of a file and returns a human-readable string. </summary>
        /// <param name="filePath">An absolute path to a file.</param>
        /// <returns>A human-readable representation of the file size.</returns>
        internal static string GetFormattedFileSize(string filePath)
        {
            string[] units = {"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
            double len = GetFileSize(filePath);
            int order = 0;
            while (len >= 1024 && order < units.Length - 1)
            {
                order++;
                len /= 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            string result = $"{len:0.##} {units[order]}";
            return result;
        }

        /// <summary>Returns the file size, given a full file path.</summary>
        /// <param name="filePath">An absolute path to a file.</param>
        /// <returns>File size in bytes.</returns>
        internal static long GetFileSize(string filePath)
        {
            var file = new FileInfo(filePath);
            if (!file.Exists) Debug.LogError("The specified file doesn't exist.");
            return file.Length;
        }

        /// <summary>Clears the Editor's console.</summary>
        internal static void ClearConsole()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
            Type type = assembly.GetType("UnityEditor.LogEntries");
            MethodInfo clearMethod = type.GetMethod("Clear");
            if (clearMethod != null) clearMethod.Invoke(new object(), null);
        }

        /// <summary>Returns all enums defined within a type.</summary>
        /// <param name="type">A type defining enums inside.</param>
        /// <returns>A collection of enums defined within the type.</returns>
        internal static IEnumerable<Type> GetNestedEnums(Type type)
        {
            return type.GetNestedTypes(BindingFlags.NonPublic |
                                       BindingFlags.Public |
                                       BindingFlags.Static)
                       .Where(nestedType => nestedType.IsEnum);
        }

        /// <summary>Tries to get pixels out of a texture. This fails if the texture isn't readable.</summary>
        /// <param name="texture">A texture to <see cref="Texture2D.GetPixels()" /> from.</param>
        /// <param name="pixels">An array of colours to assign the pixels into.</param>
        /// <remarks>This is intended as a test of texture readability, which can only be used from the import settings.</remarks>
        /// <returns>
        ///     <see langword="True" /> if the pixel information was obtained successfully, <see langword="false" /> if the
        ///     texture isn't readable.
        /// </returns>
        internal static bool TryGetPixels(Texture texture, out Color[] pixels)
        {
            pixels = null;
            try
            {
                pixels = ((Texture2D) texture).GetPixels();
                return true;
            }
            catch (UnityException)
            {
                //! Texture is not readable
                return false;
            }
        }

        /// <summary>Checks whether the object at the target path is a directory or a file.</summary>
        /// <param name="path">Full path to the object.</param>
        /// <returns><c>True</c> if the target object is a directory, <c>false</c> if a file and null if no such path exists.</returns>
        internal static bool? IsDirectory(string path)
        {
            if (Directory.Exists(path)) return true; // a directory
            if (File.Exists(path)) return false;     // a file 
            return null;                             // nonexistent
        }

        /// <summary>Checks whether the build target platform is set to WebGL.</summary>
        /// <returns><c>True</c> if the build target platform is WebGL.</returns>
        internal static bool IsBuildTargetWebGL()
        {
            return EditorUserBuildSettings.activeBuildTarget == BuildTarget.WebGL;
        }

        /// <summary>Sets the current build target and build target group.</summary>
        /// <remarks>This triggers reimport of assets, so it shouldn't be set in the middle of any setup.</remarks>
        internal static void SetTargetPlatform(BuildTarget target)
        {
            if (EditorUserBuildSettings.activeBuildTarget == target)
            {
                Debug.LogWarningFormat("Trying to set target build platform to <b>{0}</b> when it already is {0}.", target.ToString());
                return;
            }

            BuildTargetGroup group = BuildPipeline.GetBuildTargetGroup(target);
            EditorUserBuildSettings.SwitchActiveBuildTarget(@group, target);
        }

        /// <summary> Converts the height of an area in font size to fit inside. </summary>
        /// <param name="height"> Height in pixels. </param>
        internal static int HeightToFontSize(float height)
        {
            return Mathf.FloorToInt(Screen.height * 1.2f * height);
        }

        /// <summary> Load the script filed at a specific path. </summary>
        /// <param name="relativePath">A folder relative to the project folder (e.g. Assets/Sprites/Icons).</param>
        /// <returns>A dictionary of names and their corresponding scripts.</returns>
        internal static Dictionary<string, MonoScript> LoadComponentScripts(string relativePath)
        {
            return GetAssetsAtPath<MonoScript>(relativePath, true).ToDictionary(scriptAsset => scriptAsset.name);
        }

        /// <summary> Returns an array with all assets of a type in a specified folder. </summary>
        /// <typeparam name="T">Type of assets to find (e.g Sprite, Texture2D, GameObject).</typeparam>
        /// <param name="relativePath">A folder relative to the project folder (e.g. Assets/Sprites/Icons).</param>
        /// <param name="usePackagePath">Use the Newgrounds package as the root instead of the Assets folder.</param>
        /// <returns>An array of assets of specified type.</returns>
        internal static T[] GetAssetsAtPath<T>(string relativePath, bool usePackagePath = false) where T : Object
        {
            // Format path
            if (Regex.IsMatch(relativePath, @"^Assets/(.*?)/?$")) relativePath = Regex.Match(relativePath, @"^Assets/(.*?)/?$").Groups[1].Value; // Assets/Sprites/Icons → Sprites/Icons
            if (relativePath[0] == '/') relativePath = relativePath.Remove(0);                                                                   // /Sprites/Icons → Sprites/Icons
            if (relativePath[relativePath.Length - 1] == '/') relativePath = relativePath.Remove(relativePath.Length - 1);                       // Sprites/Icons/ → Sprites/Icons

            var assets = new List<T>();
            string path = !usePackagePath
                    ? Path.Combine(Application.dataPath, relativePath)
                    : NgPath.CombinePackageRoot("Runtime", relativePath);
            string[] fileEntries = Directory.GetFiles(path);
            foreach (string fileName in fileEntries)
            {
                string forwardSlashFileName = fileName.Replace("\\", "/");
                int index = forwardSlashFileName.LastIndexOf("/", StringComparison.Ordinal);
                string localPath = !usePackagePath
                        ? Path.Combine("Assets", relativePath)
                        : NgPath.CombinePackageRoot(relativePath);

                if (index > 0) localPath += forwardSlashFileName.Substring(index);

                var obj = AssetDatabase.LoadAssetAtPath<T>(localPath);

                if (obj != null) assets.Add(obj);
            }

            return assets.ToArray();
        }

        /// <summary> Checks if the user is currently connected to the Internet. </summary>
        public static bool CheckConnectivity()
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }

        /// <summary> Sets the script execution order to a new value. </summary>
        /// <param name="newOrder"> The new execution order of the script. Zero is the default, negative gets executed sooner. </param>
        public static void ChangeScriptExecutionOrder<T>(int newOrder) where T : MonoBehaviour
        {
            var temp = new GameObject("Script Execution Manager (temp)", typeof(T))
            {
                    hideFlags = HideFlags.HideAndDontSave
            };
            var behaviour = (MonoBehaviour) temp.AddComponent(typeof(T));
            MonoScript monoScript = MonoScript.FromMonoBehaviour(behaviour);

            // Getting the current execution order of that MonoScript
            // int currentExecutionOrder = MonoImporter.GetExecutionOrder(monoScript);
 
            // Changing the MonoScript's execution order
            MonoImporter.SetExecutionOrder(monoScript, newOrder);
            Object.DestroyImmediate(temp);
        }
    }
}
