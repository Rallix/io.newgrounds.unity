﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Component = UnityEngine.Component;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace io.newgrounds.unity.editor
{
    static class EditorExtensions
    {
        /// <summary>
        ///     Gets the description of an <see langword="enum" /> value from a
        ///     <see cref="System.ComponentModel.DescriptionAttribute" />.
        /// </summary>
        /// <param name="enumValue">An enum value to get the description from.</param>
        /// <returns>The description attribute's text or the enum value converted to string if it's not there.</returns>
        internal static string GetEnumDescription(this Enum enumValue)
        {
            DescriptionAttribute attribute = enumValue.GetType()
                                                      .GetMember(enumValue.ToString())
                                                      .First()
                                                      .GetCustomAttributes(typeof(DescriptionAttribute), false)
                                                      .Cast<DescriptionAttribute>()
                                                      .FirstOrDefault();
            return attribute != null ? attribute.Description : enumValue.ToString();
        }

        /// <summary>Returns a path to the given transform separated by forward slashes.</summary>
        /// <example>/Level/Player/Armature</example>
        internal static string GetPath([NotNull] this Transform current)
        {
            return current.parent == null 
                    ? $"/{current.name}" 
                    : $"{current.parent.GetPath()}/{current.name}";
        }

        /// <summary>Clears all components of type <typeparamref name="T" /> from the <see cref="GameObject" /> and its children.</summary>
        /// <typeparam name="T">The type of component to delete.</typeparam>
        /// <param name="root">The root <see cref="GameObject" /> to start looking for components to delete.</param>
        internal static void ClearComponentsInChildren<T>(this GameObject root) where T : Component
        {
            if (root == null) return;

            var components = new List<T>();
            root.GetComponentsInChildren(true, components);
            components.ForEach(Object.DestroyImmediate);
        }

        /// <summary>Returns the string closest to a specified string.</summary>
        /// <param name="strings">A collection of strings to choose from.</param>
        /// <param name="str">The string we ideally want to find.</param>
        /// <remarks>
        ///     Checks how many edits would be necessary to change one string into the other. In large collections, there may
        ///     be a significant number of strings with the same distance, in which case it selects the first one.
        /// </remarks>
        /// <returns>The string we're looking for, or the one which is closest to that.</returns>
        internal static string GetClosestStringTo(IEnumerable<string> strings, string str)
        {
            return strings.OrderBy(@string => EditorMethods.LevenshteinDistance(@string, str)).FirstOrDefault();
        }

        /// <summary> Make a texture out of a given sprite. </summary>
        /// <param name="sprite">Sprite to put into a texture.</param>
        /// <returns>The part of a texture containing only the given sprite.</returns>
        /// <remarks>Use to cut a smaller texture out of a large sprite sheet.</remarks>
        internal static Texture2D ToTexture(this Sprite sprite)
        {
            //? Works at runtime as well (move method if needed)

            if (Mathf.Approximately(sprite.rect.width, sprite.texture.width) && Mathf.Approximately(sprite.rect.height, sprite.texture.height))
            {
                // The whole texture has only one sprite
                sprite.texture.name = sprite.name;
                return sprite.texture;
            }
            var croppedTexture = new Texture2D(Mathf.RoundToInt(sprite.textureRect.width), Mathf.RoundToInt(sprite.textureRect.height));
            Color[] pixels = sprite.texture.GetPixels(Mathf.RoundToInt(sprite.textureRect.x),
                                                      Mathf.RoundToInt(sprite.textureRect.y),
                                                      Mathf.RoundToInt(sprite.textureRect.width),
                                                      Mathf.RoundToInt(sprite.textureRect.height));
            croppedTexture.SetPixels(pixels);
            croppedTexture.Apply();
            croppedTexture.name = sprite.name;
            return croppedTexture;
        }

        /// <summary> Compute the approximate distance (similarity) between two strings. The less, the better. </summary>
        internal static int DistanceTo(this string s, string t)
        {
            return EditorMethods.LevenshteinDistance(s, t);
        }

        /// <summary>Remaps a value from an old range to a new range.</summary>
        /// <param name="value">The value to remap.</param>
        /// <param name="currentRange">The current value range.</param>
        /// <param name="targetRange">The new value range to remap to.</param>
        /// <returns>A value remapped from the old range to the new range.</returns>
        internal static float RemapRange(this float value, Vector2 currentRange, Vector2 targetRange)
        {
            return (value - currentRange.x) / (currentRange.y - currentRange.x) * (targetRange.y - targetRange.x) + targetRange.x;
        }

        /// <summary> Remaps a value from an old range to a new range, starting from 0. </summary>
        /// <param name="value">The value to remap.</param>
        /// <param name="currentTop">The current top value of the range.</param>
        /// <param name="targetTop">The target top value of the range.</param>
        /// <returns>A value remapped from the old range to the new range.</returns>
        internal static float RemapRange0F(this float value, float currentTop, float targetTop)
        {
            return value.RemapRange(new Vector2(0, currentTop), new Vector2(0, targetTop));
        }

        /// <summary> Remaps a value from an old range to &lt;0,1&gt; range. </summary>
        /// <param name="value">The value to remap.</param>
        /// <param name="currentRange">The current value range.</param>
        /// <returns>A value remapped from the old range to the new range.</returns>
        internal static float RemapRange01(this float value, Vector2 currentRange)
        {
            return value.RemapRange(currentRange, new Vector2(0, 1));
        }

        /// <summary> Adusts a rectangle to fit inside another rectangle. </summary>
        /// <param name="innerRect"> The rectangle to remap. Should fit within <paramref name="referenceSize"></paramref>. </param>
        /// <param name="outerRect"> The new rectangle to remap to.</param>
        /// <param name="referenceSize"> The original area containing the <paramref name="innerRect"/>. </param>
        /// <returns> A resized and moved rectangle. </returns>
        internal static Rect RemapRect(this Rect innerRect, Rect outerRect, Vector2 referenceSize)
        {
            //! Remap coordinates from the original to the resized
            float x = innerRect.x.RemapRange0F(referenceSize.x, outerRect.width) + outerRect.x;
            float y = innerRect.y.RemapRange0F(referenceSize.y, outerRect.height) + outerRect.y;
            float width = innerRect.width.RemapRange0F(referenceSize.x, outerRect.width);
            float height = innerRect.height.RemapRange0F(referenceSize.y, outerRect.height);

            return new Rect(x, y, width, height);
        }

        /// <summary>Clamps all three vector components between a minimum float and a maximum float value.</summary>
        /// <returns>
        ///     A vector with all its components restricted to a given &lt;<paramref name="min" />, <paramref name="max" />
        ///     &gt; range.
        /// </returns>
        internal static Vector3 Clamp(this Vector3 vector, float min, float max)
        {
            return new Vector3(Mathf.Clamp(vector.x, min, max),
                               Mathf.Clamp(vector.y, min, max),
                               Mathf.Clamp(vector.z, min, max));
        }

        /// <summary>Clamps both vector components between a minimum float and a maximum float value.</summary>
        /// <returns>
        ///     A vector with both its components restricted to a given &lt;<paramref name="min" />,
        ///     <paramref name="max" />&gt; range.
        /// </returns>
        internal static Vector2 Clamp(this Vector2 vector, float min, float max)
        {
            return ((Vector3) vector).Clamp(min, max);
        }

        /// <summary>Creates a copy of a given texture.</summary>
        /// <param name="texture">A texture to copy.</param>
        /// <remarks>
        ///     This is useful when a texture is only temporary, but it's still needed after it would have been destroyed,
        ///     or if the original texture isn't readable.
        /// </remarks>
        /// <returns>A new texture which is a copy of another texture.</returns>
        [Pure]
        internal static Texture2D Copy(this Texture texture)
        {
            if (EditorMethods.TryGetPixels(texture, out Color[] oldPixels))
            {
                var newTexture = new Texture2D(texture.width, texture.height) {name = texture.name};
                newTexture.SetPixels(oldPixels);
                return newTexture;
            }
            return texture.CopyUnreadable();
        }

        /// <summary>Creates a copy of a texture which can't be read from.</summary>
        /// <param name="texture">An unreadable texture.</param>
        /// <remarks>
        ///     This is a slower process done via a hack and also completely unnecessary
        ///     if the <paramref name="texture"/> in question is actually readable.
        /// </remarks>
        /// <returns>A new texture which is a copy of the unreadable <paramref name="texture"/>.</returns>
        static Texture2D CopyUnreadable(this Texture texture)
        {
            // Create a temporary RenderTexture of the same size as the texture
            RenderTexture tmp = RenderTexture.GetTemporary(texture.width, texture.height, 0,
                                                           RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
            // Blit the pixels on texture to the RenderTexture
            Graphics.Blit(texture, tmp);
            // Backup the currently set RenderTexture
            RenderTexture previous = RenderTexture.active;
            // Set the current RenderTexture to the temporary one we created
            RenderTexture.active = tmp;

            // Create a new readable Texture2D to copy the pixels to it
            var newTexture = new Texture2D(texture.width, texture.height) {name = texture.name};
            // Copy the pixels from the RenderTexture to the new Texture
            newTexture.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
            newTexture.Apply();

            // Reset the active RenderTexture
            RenderTexture.active = previous;
            // Release the temporary RenderTexture
            RenderTexture.ReleaseTemporary(tmp); // "newTexture" now has the same pixels from "texture" and it's readable.

            return newTexture;
        }

        /// <summary>Tints a colour with another colour.</summary>
        /// <param name="source">The colour to change.</param>
        /// <param name="tint">A colour used to tint the <paramref name="source" /> colour.</param>
        /// <param name="amount">How much tint to apply; a percentage value between 0 and 1.</param>
        /// <returns>An altered colour.</returns>
        internal static Color Tint(this Color source, Color tint, float amount)
        {
            amount = Mathf.Clamp01(amount);
            for (int i = 0; i < 3; i++) source[i] += (tint[i] - source[i]) * amount; // apply to each color
            return source;
        }

        /// <summary>Changes the hue component of a colour to match another colour.</summary>
        /// <param name="source">The original colour to alter.</param>
        /// <param name="hue">The new hue to set.</param>
        /// <param name="saturationFactor">Multiply the old colour's saturation by this factor.</param>
        /// <param name="valueFactor">Multiply the old colour's brightness by this factor.</param>
        internal static Color AdjustHsv(this Color source, Color hue, float saturationFactor, float valueFactor)
        {
            Color.RGBToHSV(hue, out float newHue, out _, out _);
            Color.RGBToHSV(source, out _, out float saturation, out float value);

            return Color.HSVToRGB(newHue, Mathf.Clamp01(saturation * saturationFactor), Mathf.Clamp01(value * valueFactor));
        }

        /// <summary>Centers a <see cref="GUIContent" /> within a <see cref="Rect" /> area, horizontally and vertically.</summary>
        /// <param name="content">A text or icon to center.</param>
        /// <param name="area">The original area in whose center the <paramref name="content" /> should be placed.</param>
        /// <param name="textStyle">The <see cref="GUIStyle" /> of the text. Usually <c>GUI.skin.label</c>.</param>
        /// <returns>An area for placing the <paramref name="content" />.</returns>
        [Pure]
        internal static Rect CenterIn(this GUIContent content, Rect area, GUIStyle textStyle)
        {
            Vector2 textSize = textStyle.CalcSize(content);

            var centerArea = new Rect(area);
            centerArea.position += new Vector2((area.width - textSize.x) / 2, (area.height - textSize.y) / 2);

            return centerArea;
        }

        /// <summary>Returns the <see langword="string"/> converted to make the first letter of each word capitalised.</summary>
        /// <param name="str">The <see langword="string"/> to be converted to title case.</param>
        /// <remarks>As each language has different capitalisation rules, this should not be used as a method to create work titles.</remarks>
        /// <returns>A string with the first letter of each word capitalised.</returns>
        [Pure]
        internal static string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary> Trims the specified string once from the start of the target string if the match is exact.
        /// Otherwise it returns the same string. </summary>
        /// <param name="target">The string to trim.</param>
        /// <param name="trimString">What to trim from the start of the target string.</param>
        /// <returns> A string with its start trimmed. </returns>
        [Pure]
        internal static string TrimStart(this string target, string trimString)
        {
            if (string.IsNullOrEmpty(trimString)) return target;

            return target.StartsWith(trimString)
                    ? target.Substring(trimString.Length)
                    : target;
        }

        /// <summary> Trims the specified string once from the end of the target string if the match is exact.
        /// Otherwise it returns the same string. </summary>
        /// <param name="target">The string to trim.</param>
        /// <param name="trimString">What to trim from the end of the target string.</param>
        /// <returns> A string with its end trimmed. </returns>
        [Pure]
        internal static string TrimEnd(this string target, string trimString)
        {
            if (string.IsNullOrEmpty(trimString)) return target;

            string result = target;
            while (result.EndsWith(trimString))
            {
                result = result.Substring(0, result.Length - trimString.Length);
            }

            return result;
        }

        /// <summary> Randomly shuffles the order of elements in a collection. </summary>
        /// <typeparam name="T">Collection type.</typeparam>
        /// <param name="source">The collection to shuffle.</param>
        /// <returns>A randomly shuffled list.</returns>
        internal static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            List<T> list = source.ToList();
            for (int n = 0; n < list.Count; n++)
            {
                int i = Random.Range(n, list.Count);
                yield return list[i];
                list[i] = list[n];
            }
        }

        /// <summary> Replaces only the first occurence of a string within the text. </summary>
        internal static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search, StringComparison.InvariantCulture);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        /// <summary> Adds the component and marks the scene as dirty. </summary>
        /// <typeparam name="T"> Component to add. </typeparam>
        /// <param name="go"> Gameobject to add the component to. </param>
        public static T AddComponentDirty<T>(this GameObject go) where T : Component
        {
            var component = go.AddComponent<T>();
            FoldComponent(component, true);
            EditorSceneManager.MarkSceneDirty(component.gameObject.scene);
            return component;
        }

        /// <summary> Folds or expands a component in the Inspector window. </summary>
        /// <param name="component">The component to fold or expand.</param>
        /// <param name="fold">True to fold the component.</param>
        static void FoldComponent(this Component component, bool fold)
        {
            UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(component, !fold);
            GameObject gameObject = Selection.activeGameObject;
            Selection.activeGameObject = null;
            Selection.activeGameObject = gameObject;
        }
    }
}
