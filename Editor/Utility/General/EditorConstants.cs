﻿using System;
using UnityEditor;

namespace io.newgrounds.unity.editor
{
    static class EditorConstants  {

        /// <summary>A new line symbol based on the operating system.</summary>
        /// <remarks>Identical to <see cref="Environment.NewLine"/>.</remarks>
        /// <returns>A string containing <c>"\r\n"</c> for non-Unix platforms, or a string containing <c>"\n"</c> for Unix platforms.</returns>
        internal static readonly string NewLine = Environment.NewLine;

        /// <summary>Checks whether the user is using the grey (Personal) Editor skin or the dark (Professional) one.</summary>
        internal static bool IsDarkSkin
        {
            get { return EditorGUIUtility.isProSkin; }
        }
    }
}
