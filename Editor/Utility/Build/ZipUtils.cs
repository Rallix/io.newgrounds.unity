using System.IO;
using System.Text;
using Ionic.Zip;

namespace io.newgrounds.unity.editor
{
    static class ZipUtils
    {
        internal static void Unzip(string zipFilePath, string location)
        {
            Directory.CreateDirectory(location);

            using (ZipFile zip = ZipFile.Read(zipFilePath))
            {
                zip.ExtractAll(location, ExtractExistingFileAction.OverwriteSilently);
            }
        }

        /// <summary>Compresses the specified contents into a ZIP file.</summary>
        /// <param name="folderPath">Full path to the folder containing files to be compressed and the resulting ZIP file.</param>
        /// <param name="zipPath">The full path to the ZIP file created.</param>
        /// <param name="contents">Files or directories to add to the ZIP.</param>
        /// <remarks>Intended for Unity which only uses '/' separators. Therefore <see cref="Path.Combine(string[])"/> is unnecessary.</remarks>
        internal static void Zip(string folderPath, out string zipPath, params string[] contents)
        {
            folderPath = folderPath.TrimEnd('/');

            string name = folderPath.Substring(folderPath.LastIndexOf('/'));
            NgDebug.Log(folderPath);
            zipPath = $@"{folderPath}/{name}.zip";

            using (var zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.Always;
                zip.AlternateEncoding = Encoding.UTF8;

                foreach (string content in contents)
                {
                    string fullPath = folderPath + "/" + content;

                    bool? contentType = EditorMethods.IsDirectory(fullPath);
                    if (!contentType.HasValue)
                    {
                        NgDebug.LogWarningFormat("There's no file or directory at path: <b>{0}</b>", fullPath);
                        continue;
                    }

                    if (contentType.Value) zip.AddDirectory(fullPath, content);
                    else zip.AddFile(fullPath, string.Empty);
                }

                zip.Save(zipPath);
            }
        }
    }
}
