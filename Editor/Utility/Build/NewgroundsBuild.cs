﻿using io.newgrounds.unity.editor.settings;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace io.newgrounds.unity.editor
{
    class NewgroundsBuild : IPreprocessBuildWithReport, IPostprocessBuildWithReport, IProcessSceneWithReport
    {
        const string AutoCoreName = "Newgrounds Core (auto)";

        static Scene currentScene;
        static Scene firstScene;

        /// <summary>The conversion ratio to convert 1 MB to bytes.</summary>
        const long MBtoBytes = 1048576;

        /// <summary>The Newgrounds size limit for game builds in megabytes.</summary>        
        const int NewgroundsSizeLimit = 1000;

        /// <summary>The Newgrounds size limit for game builds in bytes.</summary>
        internal static long BuildSizeLimit
        {
            get { return NewgroundsSizeLimit * MBtoBytes; }
        }

        // Before build
        public int callbackOrder
        {
            get { return 0; }
        }

        /// <summary>
        ///   <para>Implement this function to receive a callback for each Scene during the build.</para>
        /// </summary>
        /// <param name="scene">The current Scene being processed.</param>
        /// <param name="report">A report containing information about the current build. When this callback is invoked for Scene loading during Editor playmode, this parameter will be null.</param>
        public void OnProcessScene(Scene scene, BuildReport report)
        {
            UpdateCoresInScene(scene);
        }

        /// <summary>
        /// Just before building the project in automatic mode, this inserts a core prefab in the first scene if none is found.
        /// </summary>
        /// <param name="report">A report containing information about the build, such as its target platform and output path.</param>
        public void OnPreprocessBuild(BuildReport report)
        {
            // if (report.summary.platform == BuildTarget.WebGL) AddCoreToFirstScene(report);
        }


        /// <summary>
        /// Destroy the core instance created before the build.
        /// </summary>
        /// <param name="report">A report containing information about the build, such as its target platform and output path.</param>
        public void OnPostprocessBuild(BuildReport report)
        {
            if (report.summary.platform == BuildTarget.WebGL) PostProcessWebGLBuild(report);
        }

        static void PostProcessWebGLBuild(BuildReport report)
        {
            // CheckCoreInFirstScene();
            if (NgSettingsManager.Get<bool>("build.zipAfterBuild")) CompressIntoZip(report.summary.outputPath);
        }

        /// <summary> Makes sure each scene uses the correct API info from the settings. </summary>
        /// <param name="scene"> The scene being processed. </param>
        public static void UpdateCoresInScene(Scene scene)
        {
            core[] cores = Object.FindObjectsOfType<core>();
            if (cores.Length == 0) return;

            string appId = NgSettingsManager.GetAppId();
            string encryptionKey = NgSettingsManager.GetEncryptonKey();

            foreach (core core in cores)
            {
                core.app_id = appId;
                core.aes_base64_key = encryptionKey;
            }
        }

        static void AddCoreToFirstScene(BuildReport report)
        {
            if (CoreComponent.coreMode == CoreComponent.CoreMode.Automatic)
            {
                currentScene = SceneManager.GetActiveScene();
                firstScene = SceneManager.GetSceneByBuildIndex(0);
                if (currentScene != firstScene) EditorSceneManager.OpenScene(firstScene.path, OpenSceneMode.Additive);
                if (Object.FindObjectOfType<CoreComponent>() == null)
                {
                    // Instantiate core in the first scene                    
                    CoreInitializer.tempCore = (GameObject) PrefabUtility.InstantiatePrefab(CoreInitializer.CorePrefab.gameObject, firstScene);
                    CoreInitializer.tempCore.name = AutoCoreName;
                    NgDebug.LogFormat("Initializing a Newgrounds core {0} in scene <b>{1}</b> because the Core Mode in Newgrounds Preferences " +
                                      "is set to <i>Automatic</i>.", CoreInitializer.tempCore.name, firstScene);
                }
            }
        }

        static void CheckCoreInFirstScene()
        {
            if (CoreComponent.coreMode == CoreComponent.CoreMode.Automatic)
            {
                if (currentScene != firstScene) EditorSceneManager.OpenScene(firstScene.path, OpenSceneMode.Additive);
                if (!CoreInitializer.tempCore) CoreInitializer.tempCore = GameObject.Find(AutoCoreName);
                if (CoreInitializer.tempCore)
                {
                    Object.DestroyImmediate(CoreInitializer.tempCore);
                    NgDebug.Log("Destroying temporary core: " + CoreInitializer.tempCore.name, CoreInitializer.tempCore);
                }
                else
                {
                    if (!Object.FindObjectOfType<CoreComponent>())
                    {
                        NgDebug.Log("No temporary Newgrounds core found in scene <color=orange>" + currentScene.name + "</color>.");
                    }
                    else
                    {
                        // The user manually added a core which would be otherwise added automatically
                        NgDebug.Log("A Newgrounds core found in scene <color=orange>" + currentScene.name + "</color>, but it wasn't by the build preprocessor.");
                    }
                }
                if (currentScene != firstScene) EditorSceneManager.CloseScene(firstScene, false);
            }
        }

        internal static void CompressIntoZip(string buildPath)
        {
            string[] webGLStructure = {"Build", "TemplateData", "index.html"}; 
                        
            string zipPath = string.Empty;
            // Minimal WebGL template does not have 'TemplateData' --> avoid a warning
            NgDebug.SilentCall(() => ZipUtils.Zip(buildPath, out zipPath, webGLStructure));     
            
            long fileSize = EditorMethods.GetFileSize(zipPath);
            string sizeString = EditorMethods.GetFormattedFileSize(zipPath);
            if (fileSize > BuildSizeLimit)
            {
                NgDebug.LogWarningFormat("The size of your created game build (<b>{0}</b>) exceeds the allowed limit for Newgrounds game builds ({1}).\n" +
                                         "Consider lowering your build size by some of the following:\n" +
                                         "　– Make sure you're NOT building the game as a <i>Development Build</i>\n" +
                                         "　– Lower the texture quality and <i>Max Size</i> (search for t:Texture)\n" + 
                                         "　– Use <i>Mesh Compression</i> (search for t:Model)\n",
                                         sizeString, NewgroundsSizeLimit);

            }
            else
            {
                NgDebug.LogFormat("The game build was compressed into a zip file (<b>{0}</b>) at: \n{1}", sizeString, zipPath);
                // TODO: Optionally open Project System after the build
                // TODO: Once it's possible, upload to NG from code
            }
        }

    }
}
