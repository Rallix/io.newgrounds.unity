﻿using System.Text.RegularExpressions;
using UnityEditor;

namespace io.newgrounds.unity.editor
{
    /// <summary>
    /// Extension maintaining global #define preprocessor directives. Adds <see cref="PreprocessorDirective"/> automatically on load. 
    /// The directives list is also accessible via built-in Scripting Define Symbols in Edit > Project Settings > Player.
    /// </summary>
    /// <remarks>Allows for <c>#if DIRECTIVE [your code] #endif</c> usage or [Conditional(DIRECTIVE)] attribute.</remarks>
    [InitializeOnLoad]
    static class GlobalDefines
    {

        /// <summary> The preprocessor directive added to the Scripting Define Symbols in Edit > Project Settings > Player.
        /// Allows for <c>#if NEWGROUNDS_IO [your code] #endif</c> usage. Added in WebGL and removed on other platforms. </summary>
        internal const string PreprocessorDirective = "NEWGROUNDS_IO";

        /// <summary> Constructor initializing on Unity Engine's start and after each compilation. </summary>
        static GlobalDefines()
        {
            if (EditorMethods.IsBuildTargetWebGL())
            {
                #if !NEWGROUNDS_IO
                AddPreprocessorDirective(PreprocessorDirective, true);
                #endif
            }
            else
            {
                #if NEWGROUNDS_IO
                RemovePreprocessorDirective(PreprocessorDirective, true);
                #endif
            }
        }

        /// <summary> Appends a preprocessor #define directive to the currently existing defines. </summary>
        /// <param name = "define">Global #define preprocessor directive.</param>
        /// <param name = "silent">False if defining the directive should be logged to the console.</param>
        internal static void AddPreprocessorDirective(string define, bool silent = false)
        {
            // Get current defines
            BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            string defines = GetCurrentDefines(buildTargetGroup);

            // Append only if the define is not already defined
            Regex directivePattern = GetScriptingDefineSymbolsPattern(define);
            if (directivePattern.IsMatch(defines))
            {
                if (!silent)
                {
                    NgDebug.LogWarning(string.Format("Selected build target ({0}) already contains <b>{1}</b> <i>Scripting Define Symbol</i>.\n",
                                                     EditorUserBuildSettings.activeBuildTarget, define));
                }

                return;
            }

            // Append
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, defines + ";" + define);
            if (!silent)
            {
                NgDebug.Log(string.Format("<b>{0}</b> added to <i>Scripting Define Symbols</i> for selected build target ({1}).\n" +
                                          "Code wrapped between <b>#if {0}</b> and <b>#endif</b> <i>will</i> now be compiled.\n",
                                          define, EditorUserBuildSettings.activeBuildTarget)
                           );
            }
        }

        /// <summary> This removes a preprocessor #define directive from current defines. </summary>
        /// <param name = "define">Global #define preprocessor directive.</param>
        /// <param name = "silent">Removing the directive is logged to the console.</param>
        internal static void RemovePreprocessorDirective(string define, bool silent = false)
        {
            // Get current defines
            BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            string defines = GetCurrentDefines(buildTargetGroup);
            // Remove only if the define is defined
            Regex directivePattern = GetScriptingDefineSymbolsPattern(define);
            if (!directivePattern.IsMatch(defines))
            {
                if (!silent)
                {
                    NgDebug.LogWarningFormat("Selected build target ({0}) does not contains <b>{1}</b> <i>Scripting Define Symbol</i> you're trying to remove.", EditorUserBuildSettings.activeBuildTarget, define);
                }
                return;
            }

            // Remove
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, directivePattern.Replace(defines, string.Empty));
            if (!silent)
                NgDebug.LogFormat("<b>{0}</b> removed from <i>Scripting Define Symbols</i> for selected build target ({1}).\n" +
                                  "Code wrapped between <b>#if {0}</b> and <b>#endif</b> <i>won't</i> now be compiled.\n", define, EditorUserBuildSettings.activeBuildTarget);
        }

        /// <summary>Creates a regular expression which can be used to check whether a define symbol appears in a semicolon-separated list of symbols.</summary>
        /// <param name="define">The scripting define symbol (preprocessor #define directive) to look for.</param>
        /// <returns>A regular expression which checks whether a given symbol is defined in Scripting Define Symbols.</returns>
        static Regex GetScriptingDefineSymbolsPattern(string define)
        {
            // Just one directive | It's the first one | It's in the middle / it's the last one
            return new Regex(string.Format("^{0}$|^{0};|;{0}(?=;|$)", define));
        }

        /// <summary> Clears ALL currently defined scripting define symbols (global #defines), including those not added via this extension. </summary>
        /// <param name = "buildTargetGroup">Platform you're building on. If unknown, currently selected platform will be used.</param>
        /// <returns>String with all currently defined global #defines.</returns>
        static void ClearAllCurrentDefines(BuildTargetGroup buildTargetGroup = BuildTargetGroup.Unknown)
        {
            if (buildTargetGroup == BuildTargetGroup.Unknown) buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, string.Empty);
        }

        /// <summary> Get currently defined preprocessor directives. </summary>
        /// <param name = "buildTargetGroup">Platform you're building on. If unknown, currently selected platform will be used.</param>
        /// <returns>String of all currently defined global #defines.</returns>
        static string GetCurrentDefines(BuildTargetGroup buildTargetGroup = BuildTargetGroup.Unknown)
        {
            if (buildTargetGroup == BuildTargetGroup.Unknown) buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            return PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
        }

        /// <summary> Get currently defined preprocessor directives. </summary>
        /// <param name = "buildTargetGroup">Platform you're building on. If unknown, currently selected platform will be used.</param>
        /// <returns>String array with all currently defined global #defines.</returns>
        static string[] GetCurrentDefinesArray(BuildTargetGroup buildTargetGroup = BuildTargetGroup.Unknown)
        {
            string defines = GetCurrentDefines(buildTargetGroup);
            return defines.Split(';');
        }

    }
}
