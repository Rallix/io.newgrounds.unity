﻿using System;
using System.Collections;

namespace io.newgrounds.unity.editor.utility
{
    /// <summary> An implementation of <see cref="UnityEngine.WaitWhile"/> which can be used in editor scripts. </summary>
    /// <remarks>
    ///     Editor coroutines work with custom yield instructions which implement the <see cref="MoveNext"/> method.
    /// </remarks>
    /// <seealso cref="UnityEngine.WaitWhile">
    /// <a href="https://docs.unity3d.com/ScriptReference/CustomYieldInstruction.html">CustomYieldInstruction</a>
    /// </seealso>
    class WaitWhile : IEnumerator
    {
        Func<bool> predicate;

        public object Current
        {
            get { return null; }
        }

        public bool MoveNext()
        {
            return predicate();
        }

        public void Reset() { }

        public WaitWhile(Func<bool> predicate)
        {
            this.predicate = predicate;
        }
    }
}
