﻿using System.Collections;
using UnityEngine;

namespace io.newgrounds.unity.editor.utility
{
    /// <summary> Waits while the user is attempting to log in. </summary>
    public class WaitForLogin : IEnumerator
    {
        readonly core core;
        readonly float timeout = float.MaxValue;        
        float startTime;

        static float currentTime
        {
            get
            {
                #if UNITY_EDITOR
                return (float) UnityEditor.EditorApplication.timeSinceStartup; // editor tools
                #else
                return Time.time; // playmode
                #endif
            }
        }

        public float remainingTime => Mathf.Clamp(timeout - (currentTime - startTime), 0, float.MaxValue);

        public object Current
        {
            get { return null; }
        }

        public bool MoveNext()
        {
            if (Mathf.Approximately(remainingTime, 0))
            {
                NgDebug.LogWarning("Timed out while attempting to log in.");
                return false;
            }
            if (core.waiting_for_login) core.updateLoginStatus();
            return core.waiting_for_login;
        }

        public void Reset() { 
			startTime = currentTime;			
		}

        public WaitForLogin(core core)
        {
            this.core = core;
			this.timeout = float.MaxValue;
            Reset();
        }

        public WaitForLogin(core core, float timeout) : this(core)
        {
            this.timeout = timeout;
        }

    }
}
