﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace io.newgrounds.unity.editor.utility {
    /// <summary> <see cref="UnityWebRequest.SendWebRequest"/> which can work in editor scripts. Sends the request and waits for its completion (successful or failed). </summary>
    /// <remarks>
    ///     Yielding doesn't work in editor scripts with editor coroutines, unless the <see cref="MoveNext"/> method has been implemented.
    /// </remarks>
    public class WaitForWebRequest : IEnumerator
    {
        readonly UnityWebRequestAsyncOperation www;

        /// <summary> What's the operation's progress. </summary>
        public float Progress
        {
            get { return www.progress; }
        }

        public object Current
        {
            get { return null; }
        }

        public bool MoveNext()
        {
            return !www.isDone;
        }

        public void Reset() { }

        public WaitForWebRequest(UnityWebRequest www)
        {
            this.www = www.SendWebRequest();
        }
    }
}
