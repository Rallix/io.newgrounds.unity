﻿using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor
{
    /// <summary> A class allowing users to drag & drop <see cref="NgData"/> components in scenes to instantiate <see cref="NewgroundsComponent">NewgroundsComponents</see>. </summary>
    [InitializeOnLoad]
    [HelpURL("https://answers.unity.com/answers/530634/view.html")]
    class NgDataDragging : Editor
    {
        static NgDataDragging()
        {
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemCallback;
        }

        /// <summary> Returns true if dragged items are being dropped inside the specified selection area. </summary>
        /// <param name="selectionArea"> The rectangular area in which dragging and dropping is performed. </param>
        /// <remarks> The current event is <see cref="EventType.DragPerform"/> and the mouse is within the selection area. </remarks> 
        static bool IsDragDropEvent(Rect selectionArea)
        {
            Event current = Event.current;
            bool isDragDrop = current.type == EventType.DragPerform 
                           && selectionArea.Contains(current.mousePosition);
            return isDragDrop;
        }

        static void HierarchyWindowItemCallback(int instanceID, Rect selectionArea)
        {
            // Happens when an acceptable item is released over the GUI window
            if (IsDragDropEvent(selectionArea))
            {
                var gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
                if (gameObject != null) PerformDrag(gameObject);
            }
        }

        /// <summary> Accepts a drag and performs the related actions. </summary>
        /// <param name="gameObject">The target GameObject.</param>
        static void PerformDrag(GameObject gameObject)
        {
            DragAndDrop.AcceptDrag();
            foreach (Object dragged in DragAndDrop.objectReferences)
            {
                bool eventUsed = false;
                switch (dragged)
                {
                    case MedalData medalData:
                    {
                        var medalComponent = gameObject.AddComponentDirty<MedalComponent>();
                        medalComponent.medalData = medalData;
                        eventUsed = true;
                        break;
                    }
                    case ScoreboardData scoreboardData:
                    {
                        var scoreboardComponent = gameObject.AddComponentDirty<ScoreboardComponent>();
                        scoreboardComponent.scoreboard = scoreboardData;
                        eventUsed = true;
                        break;
                    }
                }
                if (eventUsed) Event.current.Use(); // Make sure the event doesn't get picked by something else
            }
        }
    }
}
