﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace io.newgrounds.unity.editor {
    [CustomPropertyDrawer(typeof(SceneAttribute))]
    class SceneDrawer : PropertyDrawer {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (property.propertyType == SerializedPropertyType.String) {
                SceneAsset sceneObject = GetSceneObject(property.stringValue);

                Object scene = EditorGUI.ObjectField(position, label, sceneObject, typeof(SceneAsset), true);

                if (scene == null) {
                    property.stringValue = "";
                } else if (scene.name != property.stringValue) {
                    SceneAsset sceneObj = GetSceneObject(scene.name);
                    if (sceneObj == null) {
                        NgDebug.LogWarningFormat("The scene {0} cannot be used. To use this scene add it to the build settings for the project.", scene.name);
                    } else {
                        property.stringValue = scene.name;
                    }
                }
            } else EditorGUI.LabelField(position, label.text, "Use strings with [Scene] attribute.");
        }

        /// <summary>Gets a scene with a specified name from the build settings.</summary>
        /// <param name="sceneObjectName">Name of the scene to look for.</param>
        protected SceneAsset GetSceneObject(string sceneObjectName) {
            if (string.IsNullOrEmpty(sceneObjectName)) {
                return null;
            }

            foreach (EditorBuildSettingsScene editorScene in EditorBuildSettings.scenes) {
                if (editorScene.path.IndexOf(sceneObjectName, StringComparison.Ordinal) != -1) {
                    return AssetDatabase.LoadAssetAtPath(editorScene.path, typeof(SceneAsset)) as SceneAsset;
                }
            }
            NgDebug.LogWarningFormat(@"Scene <b>{0}</b> cannot be used. Add this scene to the <i>Scenes in the Build</i> in build settings.", sceneObjectName);
            return null;
        }

    }
}
