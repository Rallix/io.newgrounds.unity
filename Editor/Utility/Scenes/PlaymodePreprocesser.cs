﻿using UnityEditor;
using UnityEngine.SceneManagement;
using io.newgrounds.unity.editor.icons;

namespace io.newgrounds.unity.editor.utility
{
    /// <summary> Contains necessary checks and updates when entering/exiting Playmode. </summary>
    [InitializeOnLoad]
    static class PlaymodePreprocesser
    {
        static PlaymodePreprocesser()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode) OnPreprocessPlaymode();
            else if (state == PlayModeStateChange.ExitingPlayMode) OnPostprocessPlaymode();
        }

        /// <summary> Called every time the Editor is about to enter play mode. </summary>
        static void OnPreprocessPlaymode()
        {
            Scene scene = SceneManager.GetActiveScene();
            NewgroundsBuild.UpdateCoresInScene(scene); // Change appId and encryption key to the match the settings

            // TODO: Check if it works with Don't Destroy on Load
            if (CoreInitializer.SceneNeedsACore()) CoreInitializer.AddTemporaryCore();
        }

        /// <summary> Called every time the Editor is about to exit play mode. </summary>
        static void OnPostprocessPlaymode()
        {
            // Otherwise Ng Inspector icons tend to disappear
            NewgroundsIcons.RefreshIcons();
            // CoreInitializer.DestroyCore();
        }
    }
}
