﻿using io.newgrounds.unity.editor.icons;
using io.newgrounds.unity.editor.settings;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor
{
    /// <summary>
    /// This class makes sure that whenever you enter the playmode in Unity Editor, there's a core instance spawned.
    /// </summary>
    static class CoreInitializer
    {
        // TODO: IProcessSceneWithReport.OnProcessScene (report is null)
        
        // TODO: Unite CorePrefab with TempCore

        internal static GameObject tempCore;

        const string AutoCoreName = "Newgrounds Core (auto)";

        /// <summary> Checks if the scene requires a <see cref="CoreComponent"/>. </summary>
        /// <remarks> Must be enabled in the settings, and the scene should have <see cref="NewgroundsComponent"/>. </remarks>
        internal static bool SceneNeedsACore()
        {
            bool enabled = NgSettingsManager.Get<bool>("editor.addCoreAutomatically");
            return enabled && Object.FindObjectsOfType<NewgroundsComponent>().Length > 0 && Object.FindObjectOfType<core>() == null;
        }

        /// <summary> Instantiates a hidden, temporary core prefab in the current scene. </summary>
        /// <param name="name"> The name of the added core instance. </param>
        internal static void AddTemporaryCore(string name = AutoCoreName)
        {
            AddCorePrefab(name, HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor);
        }

        /// <summary> An editor button which will instantiate the core prefab when clicked. </summary>
        /// <param name="buttonWidth"> The width of the button. </param>
        internal static bool AddCoreButton(int buttonWidth)
        {
            GUILayoutOption[] guiOptions = {GUILayout.Width(buttonWidth), GUILayout.Height(buttonWidth / 2f)};
            Texture2D coreIcon = NewgroundsIcons.GetIcon(NewgroundsIcons.ComponentIcons.Core);
            bool result = GUILayout.Button(new GUIContent("Add\nCore", coreIcon, "Adds the core component maintaining the link to Newgrounds"), guiOptions);
            if (result) AddCorePrefab();
            return result;
        }

        static CoreComponent corePrefab;

        internal static CoreComponent CorePrefab
        {
            get
            {
                const string prefabName = "Core"; // TODO: Labels?

                if (corePrefab) return corePrefab;
                var core = Resources.Load<GameObject>(prefabName).GetComponent<CoreComponent>();
                if (core)
                {
                    corePrefab = core;
                    return core;
                }
                else if (CoreComponent.coreMode == CoreComponent.CoreMode.Automatic)
                {
                    NgDebug.LogErrorFormat("Cannot find a CoreComponent prefab named <b>{0}</b> in any of the <i>Resources</i> folders.", prefabName);
                }
                return null;
            }
        }

        /// <summary> Adds a <see cref="CoreComponent"/> prefab to the current scene. </summary>
        /// <param name="name"> The name of the instantiated gameobject. </param>
        /// <param name="hideFlags"> <see cref="HideFlags"/> of the added core gameobject. </param>
        /// <returns> The <see cref="CoreComponent"/> of the added prefab. </returns>
        internal static CoreComponent AddCorePrefab(string name = "Newgrounds Core", HideFlags hideFlags = HideFlags.None)
        {
            var core = (GameObject) PrefabUtility.InstantiatePrefab(CorePrefab.gameObject);
            core.hideFlags = hideFlags;
            core.name = name;
            Undo.RegisterCreatedObjectUndo(core, "Create " + core.name);
            return core.GetComponentInChildren<CoreComponent>();
        }
    }
}
