using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


namespace io.newgrounds.unity.editor
{
    static class EditorFileUtils
    {
        /// <summary>The location of script templates. Load a specific script template with <see cref="Resources.Load(string)"/>.</summary>
        const string ScriptTemplatesFolder = "ScriptTemplates/";

        // TODO: A template for a custom attribute / property drawer

        [MenuItem("Assets/Tools/Create Editor from MonoBehaviour")]
        static void GenerateEditor()
        {
            var monoScript = (MonoScript)Selection.activeObject;

            Type type = monoScript.GetClass();
            string folder = GetOrCreateFolderPath(monoScript);

            const string editorTemplatePath = ScriptTemplatesFolder + "EditorTemplate";
            const string defaultNamespace = "RPG";

            const string placeholderNamespace = "#NAMESPACE#";
            const string placeholderClass = "#SCRIPTNAME#";
            const string placeholderClassLower = "#SCRIPTNAME_LOWER#";
            const string placeholderSerializedProperties = "#SERIALIZED_PROPERTIES#";

            var editorTemplate = (TextAsset)Resources.Load(editorTemplatePath);

            string templateText = editorTemplate.text;

            string @namespace = !string.IsNullOrEmpty(type.Namespace) ? type.Namespace : defaultNamespace;
            string @class = type.Name;
            string classLower = char.ToLowerInvariant(type.Name[0]) +                       // First letter to lower
                                ((@class.Length > 1) ? @class.Substring(1) : string.Empty); // Add the rest of the name if it's not just one letter

            string tab = Regex.Match(templateText, $@"^([^\S\r\n]*){placeholderSerializedProperties}", RegexOptions.Multiline).Groups[1].Value;

            string serializedProperties = string.Join(separator: $"{EditorConstants.NewLine}{tab}",
                                                      value: type.GetSerializedFields()
                                                                 .Select(field => $"SerializedProperty {field.Name};")
                                                                 .ToArray());

            using (var writer = new StreamWriter($"{folder}/{@class}Editor.cs"))
            {
                templateText = templateText.Replace(placeholderNamespace, @namespace)
                                           .Replace(placeholderClass, @class)
                                           .Replace(placeholderClassLower, classLower)
                                           .Replace(placeholderSerializedProperties, serializedProperties);
                writer.WriteLine(templateText);
            }

            AssetDatabase.Refresh();
        }

        [MenuItem("Assets/Tools/Create Editor from MonoBehaviour", true)]
        static bool IsMonoScript()
        {
            return Selection.activeObject is MonoScript;
        }

        /// <summary>Gets a folder path or creates a new Editor folder in the object's directory.</summary>
        /// <returns>A path to the new (or old) folder.</returns>
        static string GetOrCreateFolderPath(Object obj)
        {
            const string editorFolderName = "Editor";

            string assetPath = AssetDatabase.GetAssetPath(obj);
            string parentFolder = assetPath.Substring(0, assetPath.LastIndexOf('/'));

            string editorFolder = $"{parentFolder}/{editorFolderName}";
            if (!AssetDatabase.IsValidFolder(editorFolder)) AssetDatabase.CreateFolder(parentFolder, editorFolderName);

            return editorFolder;
        }

        /// <summary>Checks a given folder path and prepares it to be used.</summary>        
        /// <returns>False if it's pointless to generate such file.</returns>
        static bool IsFolderPathValid(ref string folderPath)
        {
            if (folderPath.EndsWith("/"))
            {
                folderPath = folderPath.Remove(folderPath.Length - 1); // Remove last '/'
            }
            if (!AssetDatabase.IsValidFolder(folderPath))
            {
                NgDebug.LogError($"Trying to create a file in folder <b>{folderPath}</b>, which doesn't exist.");
                return false;
            }
            return true;
        }

        /// <summary>Check whether a folder is empty.</summary>
        /// <param name="path">Relative path from the Unity project folder.</param>
        internal static bool IsEmptyFolder(string path)
        {
            return !AssetDatabase.FindAssets(string.Empty, new[] { path }).Any();
        }

        /// <summary> Finds all specified data assets in the project. </summary>
        /// <typeparam name="TData"> The type of <see cref="NgData"/>, e.g. <see cref="MedalData"/> or <see cref="ScoreboardData"/>. </typeparam>
        internal static IEnumerable<TData> GetNgDataAssets<TData>() where TData : NgData
        {
            return AssetDatabase.FindAssets($"t:{typeof(TData)}")
                                .Select(guid => AssetDatabase.LoadAssetAtPath<TData>(AssetDatabase.GUIDToAssetPath(guid)));
        }

        /// <summary>Returns the path to a given asset. Defaults to <see cref="AssetDatabase.GetAssetPath(Object)"/>.</summary>
        /// <param name="asset">The asset to look for.</param>
        /// <param name="pathFormat">The path format to return. Defaults to <see cref="AssetPathFormat.RelativeFile"/></param>
        internal static string GetAssetPath(Object asset, AssetPathFormat pathFormat = AssetPathFormat.RelativeFile)
        {
            // TODO: Doesn't work for files outside of the Assets folder
            string relativeObjectPath = AssetDatabase.GetAssetPath(asset);
            string absoluteObjectPath = Application.dataPath + relativeObjectPath.Remove(0, 6); // remove duplicate "Assets"

            return TransformObjectPath(relativeObjectPath, absoluteObjectPath, pathFormat);
        }

        /// <summary> Returns the chosen path type from a given path. Defaults to the equivalent of <see cref="AssetDatabase.GetAssetPath(Object)"/>.</summary>
        /// <param name="relativePath">The relative path to the object (Assets/...).</param>
        /// <param name="pathFormat">The path format to return. Defaults to <see cref="AssetPathFormat.RelativeFile"/></param>
        internal static string GetPathFromRelative(string relativePath, AssetPathFormat pathFormat = AssetPathFormat.RelativeFile)
        {
            string absoluteObjectPath = Application.dataPath + relativePath.Remove(0, 6);
            return TransformObjectPath(relativePath, absoluteObjectPath, pathFormat);
        }

        /// <summary> Returns the chosen path type from a given path. Defaults to the equivalent of <see cref="AssetDatabase.GetAssetPath(Object)"/>.</summary>
        /// <param name="absolutePath">The absolute path to the object (C:/Unity/Projects/GAME/Assets/...).</param>
        /// <param name="pathFormat">The path format to return. Defaults to <see cref="AssetPathFormat.RelativeFile"/></param>
        internal static string GetPathFromAbsolute(string absolutePath, AssetPathFormat pathFormat = AssetPathFormat.RelativeFile)
        {
            string relativeObjectPath = absolutePath.ReplaceFirst(Application.dataPath, "Assets");
            return TransformObjectPath(relativeObjectPath, absolutePath, pathFormat);
        }

        static string TransformObjectPath(string relativeObjectPath, string absoluteObjectPath, AssetPathFormat pathFormat)
        {
            switch (pathFormat)
            {
                case AssetPathFormat.RelativeFile:
                    return relativeObjectPath;
                case AssetPathFormat.RelativeFolder:
                    return relativeObjectPath.Remove(relativeObjectPath.LastIndexOf('/'));
                case AssetPathFormat.AbsoluteFile:
                    return absoluteObjectPath;
                case AssetPathFormat.AbsoluteFolder:
                    return absoluteObjectPath.Remove(absoluteObjectPath.LastIndexOf('/'));
                case AssetPathFormat.NameFile:
                    return absoluteObjectPath.Split('/').Last();
                case AssetPathFormat.NameFolder:
                    return absoluteObjectPath.Split('/').Reverse().Skip(1).Take(1).First();
                case AssetPathFormat.ExtensionDot:
                    if (!relativeObjectPath.Contains('.')) return "";
                    else return "." + relativeObjectPath.Split('.').Last().ToLower();
                case AssetPathFormat.Extension:
                    if (!relativeObjectPath.Contains('.')) return "";
                    else return relativeObjectPath.Split('.').Last().ToLower();
                default:
                    NgDebug.LogErrorFormat("Asked for an unknown asset path type <b>{0}</b>.", pathFormat);
                    return null;
            }
        }

        /// <summary>Provides different formats available for representing a file.</summary>
        internal enum AssetPathFormat
        {
            /// <summary>Assets/Folder/File.exe</summary>
            RelativeFile = 0,

            /// <summary>C:/Path/Project/Assets/Folder/File.exe</summary>
            AbsoluteFile,

            /// <summary>Assets/Folder</summary>
            RelativeFolder,

            /// <summary>C:/Path/Project/Assets/Folder</summary>
            AbsoluteFolder,

            /// <summary>File.exe</summary>
            NameFile,

            /// <summary>Folder</summary>
            NameFolder,

            /// <summary>exe</summary>
            Extension,

            /// <summary>.exe</summary>
            ExtensionDot
        }

        internal static string GetSelectedAssetPath(AssetPathFormat pathFormat)
        {
            return GetAssetPath(Selection.activeObject, pathFormat);
        }

        static void LogPathsForSelectedObject(Object selectedObject)
        {
            foreach (AssetPathFormat pathType in Enum.GetValues(typeof(AssetPathFormat)))
            {
                NgDebug.LogFormat("<b>{0}</b>: {1}", pathType.ToString(), GetAssetPath(selectedObject, pathType));
            }
        }
    }
}
