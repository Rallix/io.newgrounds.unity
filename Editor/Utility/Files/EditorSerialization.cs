﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor {
    /// <summary>A class for handling automatic <see cref="SerializedProperty"/> initialization in custom editors.</summary>
    static class EditorSerialization {

        /// <summary> Initializes all serialized properties of a custom editor with serialized fields of the target class. The names must match. </summary>        
        /// <param name="editor">The custom <see cref="Editor"/> being created.</param>
        internal static void FindSerializedProperties(this Editor editor) {
            List<string> serializedFields = editor.target
                                                  .GetType()
                                                  .GetSerializedFields()
                                                  .Select(field => field.Name)
                                                  .ToList();
            List<FieldInfo> serializedProperties = editor.GetSerializedProperties().ToList();

            if (serializedProperties.Count != serializedFields.Count) {
                NgDebug.LogErrorFormat("The number of serialized properties in <b>{0}</b> ({1}) doesn't match the number of serialized fields in <b>{2}</b> ({3}).",
                                             editor.GetType(), serializedProperties.Count, editor.target.GetType(), serializedFields.Count);
                return;
            }

            foreach (FieldInfo field in editor.GetSerializedProperties()) {
                if (serializedFields.Contains(field.Name)) {
                    //! Set value
                    SerializedProperty value = editor.serializedObject.FindProperty(field.Name);
                    field.SetValue(editor, value);
                }
                else {
                    //! Invalid name
                    NgDebug.LogWarningFormat("There is no matching serialized field for the {0} property <b>{1}</b>.", editor.GetType(), field.Name);
                }
            }
        }

        /// <summary> Gets all fields of a class serializable by Unity. </summary>
        /// <param name="type">The target type of the custom <see cref="Editor"/> being created.</param>
        /// <returns> Returns the names of all public fields and private fields marked with the <see cref="SerializeField"/> attribute 
        /// whose classes are <see cref="System.SerializableAttribute"/>. </returns>
        internal static IEnumerable<FieldInfo> GetSerializedFields(this Type type) {
            var serializable = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                                   .Where(field => field.GetCustomAttribute<SerializableAttribute>() != null);
            return serializable.Where(field => field.IsPublic || field.GetCustomAttribute<SerializeField>() != null);
        }

        /// <summary> Gets all fields with <see cref="SerializedProperty"/> type of the custom editor. </summary>
        /// <param name="editor">The custom <see cref="Editor"/> being created.</param>
        /// <returns> Returns all non-static public and private <see cref="SerializedProperty"/> fields. </returns>
        internal static IEnumerable<FieldInfo> GetSerializedProperties(this Editor editor) {
            var serializedProperties = editor.GetType()
                                             .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                                             .Where(field => field.FieldType == typeof(SerializedProperty));
            return serializedProperties;
        }

    }
}
