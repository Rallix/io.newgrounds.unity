namespace io.newgrounds.unity.editor.icons
{
    static partial class NewgroundsIcons
    {
        
        //! Enum values defined here must be (currently) *either* all from a single sprite sheet OR all single icons within one folder.
        //? [Description("Alternative path"]
        //? Pull automatically from icon folder
        
        /// <summary>Small Newgrounds icons (orange, black, white).</summary>
        internal enum SmallIcons
        {

            Add,
            AddBlack,
            BadgeBlack,
            BadgeRed,
            Calendar,
            CalendarBlack,
            Collapse,
            CollapseBlack,
            Contact,
            ContactWhite,
            Delete,
            DeleteWhite,
            Display,
            DisplayBlack,
            Down,
            DownWhite,
            Dump,
            Edit,
            EditBlack,
            EditRed,
            EditWhite,
            Expand,
            ExpandBlack,
            Favourite,
            Fist,
            FistBlack,
            Flag,
            FlagBlack,
            Follow,
            FollowBlack,
            Friend,
            FullSize,
            FullSizeBlack,
            Info,
            InfoBlack,
            Key,
            KeyWhite,
            Left,
            LeftBlack,
            Light,
            LightBlack,
            Link,
            LinkBlack,
            Logout,
            Ribbon,
            RibbonBlack,
            Move,
            MoveBlack,
            Playlist,
            PlaylistBlack,
            Projects,
            Puzzle,
            PuzzleBlack,
            Refresh,
            RefreshGrey,
            RefreshWhite,
            Right,
            RightBlack,
            Rss,
            RssBlack,
            Settings,
            SettingsWhite,
            SmallSize,
            SmallSizeBlack,
            Star,
            Stars,
            StarsWhite,
            Trash,
            TrashBlack,
            TrashGrey,
            TrashWhite,
            Unfollow,
            UnfollowBlack,
            UnfollowWhite,
            Warning,
            WarningBlack

        }
        /// <summary>All-purpose Editor icons.</summary>
        internal enum UtilityIcons
        {
            /// <summary>Newgrounds small logo (NG).</summary>
            Logo,
            /// <summary>The custom (unofficial) logo of NG.io for Unity.</summary>
            Icon,
            Tank,
            BaseFolder,
            LargeFolder,
            SmallFolder,

            /// <summary>Recoloured Unity folder icon.</summary>
            Folder,
            /// <summary>Recoloured Unity empty folder icon.</summary>
            FolderEmpty

        }

        /// <summary>Newgrounds Component script icons.</summary>
        internal enum ComponentIcons
        {

            Core,
            Newgrounds,
            Login,
            Medal,
            SaveLoad,
            Scoreboard,
            Spyglass,
            User,
            Settings

        }
        /// <summary>Large HD Newgrounds icons, unsuitable for tiny UI elements.</summary>
        internal enum HdIcons
        {

            Tankman,
            Info,
            User,
            Users,
            Settings,
            Link,
            Games,
            Movies,
            Audio,
            Art,
            Quill,
            Flag,
            Star,
            Heart,
            Block,
            Dollar,
            QuestionMark,
            Alert,
            Warning,
            CreativeCommons,
            Folder,
            LayoutGrid,
            LayoutMedium,
            LayoutSmall,
            LayoutLarge,
            Paper,
            Message,
            Trophy,
            Puzzle,
            Fist,
            Medal,
            Badge,
            Bot,
            Skull,
            Chat,
            Rss,
            Down,
            Up,
            Search,
            Logout,
            Tick,
            RatingEveryone,
            RatingTeen,
            RatingMature,
            RatingAdult
        }
    }
}
