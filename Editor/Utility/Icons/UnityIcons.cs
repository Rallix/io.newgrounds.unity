﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.icons
{
    /// <summary>A class for locating and compiling a list of Unity built-in icons.</summary>
    // [InitializeOnLoad]
    static partial class UnityIcons
    {
        internal static InternalIcons[] AllIcons => (InternalIcons[]) Enum.GetValues(typeof(InternalIcons));

        /// <summary>Loads an icon from a list of internal Unity Editor icons.</summary>
        /// <param name="icon">An internal Unity Editor icon to look for.</param>
        /// <returns>The desired internal icon or a <see cref="NewgroundsIcons.DefaultIcon" /></returns>
        /// <remarks>
        ///     <para>Custom icons inside Assets/EditorDefaultResources/ take precedence over the internal icons.</para>
        ///     <para>Create a copy with <see cref="GetIconCopy" /> if you intend to use it further.</para>
        /// </remarks>
        [NotNull]
        internal static Texture2D GetIcon(InternalIcons icon)
        {
            string iconName = icon.GetEnumDescription();
            return EditorGUIUtility.Load(iconName) as Texture2D ?? NewgroundsIcons.DefaultIcon;
        }

        /// <summary>Creates a copy of an internal Unity Editor icon intended for further edits.</summary>
        /// <param name="icon">An internal Unity Editor icon to copy.</param>
        /// <remarks>Internal Unity icons can be displayed freely, but cannot be read or edited. This solves the problem.</remarks>
        /// <returns>
        ///     A readable and writeable copy of an internal editor icon or a copy of the
        ///     <see cref="NewgroundsIcons.DefaultIcon" />.
        /// </returns>
        internal static Texture2D GetIconCopy(InternalIcons icon)
        {
            Texture2D internalIcon = GetIcon(icon);
            return internalIcon.Copy();
        }

        /// <summary>Saves an internal icon in a given path.</summary>
        /// <param name="iconType"></param>
        /// <param name="folderPath">
        ///     A relative path to an existing folder inside the project ("Assets/Gizmos/Folder").
        ///     Defaults to the Assets folder.
        /// </param>
        static void SaveIcon(InternalIcons iconType, string folderPath = "Assets")
        {
            if (!AssetDatabase.IsValidFolder(folderPath))
            {
                NgDebug.LogErrorFormat("The path doesn't lead to an existing folder: <b>{0}</b>", folderPath);
                return;
            }
            Texture2D icon = GetIconCopy(iconType);
            byte[] pngBytes = icon.EncodeToPNG();

            File.WriteAllBytes(string.Concat(folderPath, "/", icon.name, ".png"), pngBytes);
            AssetDatabase.Refresh();
        }

        /// <summary>Checks whether all defined icons are in fact valid.</summary>
        // Debug: [MenuItem(NewgroundsMenuTools.BasePath + "Icons/Validate Available Icons", priority = 50)]
        static void ValidateAvailableIcons()
        {
            var icons = (InternalIcons[]) Enum.GetValues(typeof(InternalIcons));
            foreach (InternalIcons iconType in icons)
            {
                Texture2D icon = GetIcon(iconType);
                if (icon == NewgroundsIcons.DefaultIcon) NgDebug.LogWarningFormat("Couldn't load <b>{0}</b> with {1}", iconType, iconType.GetEnumDescription());
            }
        }


        /// <summary>Logs all currently available internal icons.</summary>
        /// <remarks>
        ///     The <see cref="InternalIcons" /> <see langword="enum" /> isn't complete. Some icons are only available
        ///     under specific condition – this method should find all currently availabĺe new icons.
        /// </remarks>
        // Debug: [MenuItem(NewgroundsMenuTools.BasePath + "Icons/Log New Internal Icons", priority = 51)]
        static void LogNewIcons()
        {
            // All textures in the Resources folder(s)     
            Texture2D[] everything = Resources.FindObjectsOfTypeAll<Texture2D>();

            // Filter internal icons
            List<string> iconNames = everything.Where(IsInternalIcon)
                                               .Select(texture => texture.name)
                                               .Distinct()
                                               .OrderBy(name => name).ToList();

            // All known icons defined in the BuiltInIcons
            List<string> enumNames = Enum.GetValues(typeof(InternalIcons))
                                         .Cast<InternalIcons>()
                                         .Select(iconType => iconType.GetEnumDescription())
                                         .OrderBy(name => name).ToList();

            // Get list difference (icon names are case-insensitive)
            string[] newNames = iconNames.Except(enumNames, StringComparer.OrdinalIgnoreCase).ToArray();
            if (newNames.Length > 0)
            {
                string newNamesAsEnum = string.Join("\n", newNames.Select(name =>
                {
                    string identifierName = name.Replace(".", string.Empty).Replace(" ", string.Empty).Replace('-', '_');
                    return $"[Description(\"{name}\")] {identifierName},";
                }).ToArray());
                NgDebug.LogFormat("<b>New icons found ({0}):</b>\n{1}\n", newNames.Length, newNamesAsEnum);
            }
        }

        /// <summary>Checks whether an icon is a internal Unity Editor icon.</summary>
        /// <param name="texture">A texture to test.</param>
        /// <remarks>
        ///     Since the test method doesn't throw an exception, the way the test works is by trying to retrieve
        ///     a internal icon and checking the result, silencing the error shown if it's a custom icon instead.
        /// </remarks>
        /// <returns>True if the <paramref name="texture" /> is a Unity Editor internal icon, false if it's a custom icon.</returns>
        static bool IsInternalIcon(Texture2D texture)
        {
            bool result = false;
            NgDebug.SilentCall(() =>
            {
                GUIContent iconContent = EditorGUIUtility.IconContent(texture.name);
                result = iconContent != null && iconContent.image != null;
            });
            return result;
        }

    }
}
