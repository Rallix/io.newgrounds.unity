﻿using io.newgrounds.unity;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.icons
{
    /// <summary> Displays small icons in Hieararchy window next to objects with Newgrounds components. </summary>
    [InitializeOnLoad]
    class HierarchyIcons
    {

        const NewgroundsIcons.UtilityIcons HierarchyIcon = NewgroundsIcons.UtilityIcons.Logo;

        static HierarchyIcons()
        {
            EditorApplication.hierarchyWindowItemOnGUI += OnItemInHierarchy;            
        }

        /// <summary> Displays icon next to marked objects on the right side of Hierarchy tab. </summary>
        /// <param name="instanceID">Instance ID of hierarchy object.</param>
        /// <param name="selectionRect">Area containing hierarchy object's label.</param>
        static void OnItemInHierarchy(int instanceID, Rect selectionRect)
        {
            var item = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
            if (item)
            {
                // Offset -> Align root gameobjects' icon with nested gameobjects' icon
                int offset = 10; // or 0
                Transform itemParent = item.transform.parent;
                while (itemParent != null)
                {
                    offset += 14;
                    itemParent = itemParent.transform.parent;
                }

                // Position the icon
                var rect = new Rect(selectionRect);
                rect.x = rect.width + offset;
                rect.width = 18;

                // Draw the icon if it's a Newgrounds component
                if (item.GetComponent<CoreComponent>() || item.GetComponent<NewgroundsComponent>())
                {
                    GUI.Label(rect, NewgroundsIcons.GetIcon(HierarchyIcon));
                }
            }
        }

    }
}
