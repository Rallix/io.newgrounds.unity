﻿using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.icons
{
    /// <summary> A debug tool for finding the names of built-in Unity icons, so they can later be loaded with <see cref="EditorGUIUtility.Load"/>. </summary>
    class UnityIconWindow : EditorWindow
    {
        const int RectSize = 40;
        const int Padding = 10;
        const int PageMax = 90;
        static int page = 0;

        // Debug: [MenuItem(NewgroundsMenuTools.BasePath + "Icons/Icon Preview")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            var window = GetWindowWithRect<UnityIconWindow>(new Rect(350,350,730,400),true,"Icon Preview", true);
            window.titleContent = new GUIContent("Icon Preview");
            page = default; // 730×400 
            window.Show();
        }

        void OnGUI()
        {
            int iconWidth = RectSize + Padding;
            int horizontally = Mathf.FloorToInt((position.width - Padding) / iconWidth);
            int currentPageLimit = page * PageMax + PageMax;
            int pageOffset = Mathf.Clamp(page*PageMax/horizontally, 0, int.MaxValue);
            currentPageLimit = Mathf.Clamp(currentPageLimit, 0, UnityIcons.AllIcons.Length);
            for (int i = page * PageMax; i < currentPageLimit; i += horizontally)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    int limit = Mathf.Clamp(i + horizontally, 0, UnityIcons.AllIcons.Length);
                    for (int j = i; j < limit; j++)
                    {
                        UnityIcons.InternalIcons unityIcon = UnityIcons.AllIcons[j];
                        string iconName = unityIcon.GetEnumDescription();
                        var iconLabel = new GUIContent(UnityIcons.GetIcon(unityIcon), iconName);
                        int horCount = i / horizontally;
                        var pos = new Vector2((j - i) * iconWidth + Padding, // horizontal position on a line
                                              iconWidth * (horCount - pageOffset) + Padding); // vertical position on a page

                        // Icon button drawn as a label
                        if (GUI.Button(new Rect(pos, new Vector2(RectSize, RectSize)), iconLabel, GUI.skin.label))
                        {
                            EditorGUIUtility.systemCopyBuffer = $"\"{iconName}\"";
                        }
                    }
                }
            }
            var buttonSize = new Vector2(75, 30);
            var basePosition = new Vector2(position.width - (buttonSize.x + Padding),
                                           position.height - (buttonSize.y + Padding));
            if (page >= 1 && GUI.Button(new Rect(basePosition - new Vector2(buttonSize.x + Padding, 0), buttonSize), "Back"))
            {
                page--;
            }
            using (new EditorGUI.DisabledGroupScope(currentPageLimit == UnityIcons.AllIcons.Length))
            {
                if (GUI.Button(new Rect(basePosition, buttonSize),"Next"))
                {
                    page++;
                }
            }
            
        }
    }
}
