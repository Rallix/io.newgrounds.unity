using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.icons
{

    /// <summary>A class providing access to all Newgrounds icons loaded in the Editor.</summary>
    [InitializeOnLoad] //! Necessary to load all icons before needing them
    static partial class NewgroundsIcons
    {
        /// <summary>The folder inside Resources containing all icons.</summary>
        const string IconFolder = "Icons";

        /// <summary>The suffix used by sprite sheets meant for the Editor's dark UI.</summary>
        const string DarkSkinSheetSuffix = "Pro";

        // TODO: Check paths in package

        const string SmallIconSheetPath = "Main/NewgroundsIcons";
        const string HdIconSheetPath = "Main/NewgroundsIconsHD";
        const string UtilityFolderPath = "Utility";
        const string ComponentsFolderPath = "Components";

        /// <summary>All defined icon library enums.</summary>
        static readonly IEnumerable<Type> IconEnums = EditorMethods.GetNestedEnums(typeof(NewgroundsIcons));

        /// <summary>A default icon returned when some icon can't be found.</summary>
        internal static readonly Texture2D DefaultIcon = EditorConstants.IsDarkSkin ? Texture2D.whiteTexture : Texture2D.blackTexture;

        /// <summary>A default icon returned when some icon can't be found, resized to specified size.</summary>
        internal static Texture2D GetScaledDefaultIcon(int size, Color? iconColor = null)
        {
            if (!iconColor.HasValue) iconColor = EditorConstants.IsDarkSkin ? Color.grey : Color.gray;
            var icon = new Texture2D(size, size);
            Color[] pixels = Enumerable.Repeat(iconColor.Value, size * size).ToArray();
            icon.SetPixels(pixels);
            icon.Apply(false);
            return icon;
        }

        static Dictionary<SmallIcons, Texture2D> SmallIconsLibrary = LoadIconsFromSheet<SmallIcons>(SmallIconSheetPath);
        static Dictionary<HdIcons, Texture2D> HdIconsLibrary = LoadIconsFromSheet<HdIcons>(HdIconSheetPath);
        static Dictionary<UtilityIcons, Texture2D> UtilityIconsLibrary = LoadIcons<UtilityIcons>(UtilityFolderPath);
        static Dictionary<ComponentIcons, Texture2D> ComponentIconsLibrary = LoadIcons<ComponentIcons>(ComponentsFolderPath); //? From Gizmos?

        static NewgroundsIcons()
        {
            RefreshIcons();

            EditorApplication.playModeStateChanged += OnPlayModeChanged;
        }

        static void OnPlayModeChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingPlayMode)
            {
                RefreshIcons();
            }
        }

        /// <summary>Reloads the icon library.</summary>        
        internal static void RefreshIcons()
        {
            SmallIconsLibrary = LoadIconsFromSheet<SmallIcons>(SmallIconSheetPath);
            HdIconsLibrary = LoadIconsFromSheet<HdIcons>(HdIconSheetPath);
            UtilityIconsLibrary = LoadIcons<UtilityIcons>(UtilityFolderPath);
            ComponentIconsLibrary = LoadIcons<ComponentIcons>(ComponentsFolderPath); //? From Gizmos?
        }

        /// <summary>Retrieves an icon from the <see cref="Resources" /> folder.</summary>
        /// <typeparam name="TEnum">A <see cref="NewgroundsIcons" /> <see langword="enum" /> type.</typeparam>
        /// <param name="iconType">A specific icon to find.</param>
        /// <returns>A newgrounds icon, or a <see cref="DefaultIcon" /> if it's not there.</returns>
        internal static Texture2D GetIcon<TEnum>(TEnum iconType) where TEnum : Enum
        {
            switch (iconType)
            {
                case SmallIcons smallIcon:
                    return SmallIconsLibrary.GetIconOrDefault(smallIcon);
                case UtilityIcons utilityIcon:
                    return UtilityIconsLibrary.GetIconOrDefault(utilityIcon);
                case ComponentIcons componentIcon:
                    return ComponentIconsLibrary.GetIconOrDefault(componentIcon);
                case HdIcons hdIcon:
                    return HdIconsLibrary.GetIconOrDefault(hdIcon);
                case UnityIcons.InternalIcons unityIcon:
                    Debug.LogWarning($"The method <i>{nameof(NewgroundsIcons)}.{nameof(GetIcon)}</i> " +
                                     $"is intended for Newgrounds icons only, not Unity internal icons.\n" +
                                     $"Did you mean to use <b>{nameof(UnityIcons)}.{nameof(UnityIcons.GetIcon)}</b>?");
                    break;
            }
            return DefaultIcon;
        }

        /// <summary> Returns the icon library corresponding to the type. </summary>
        /// <typeparam name="TEnum">A <see cref="NewgroundsIcons" /> <see langword="enum" /> type.</typeparam>
        /// <param name="iconType">A specific icon to find.</param>
        /// <returns>The icon library containing the icon based on the type.</returns>
        static IDictionary<TEnum, Texture2D> GetIconLibraryOf<TEnum>(TEnum iconType) where TEnum : Enum
        {
            switch (iconType)
            {
                case SmallIcons _:
                    return (IDictionary<TEnum, Texture2D>) SmallIconsLibrary;
                case UtilityIcons _:
                    return (IDictionary<TEnum, Texture2D>) UtilityIconsLibrary;
                case ComponentIcons _:
                    return (IDictionary<TEnum, Texture2D>) ComponentIconsLibrary;
                case HdIcons _:
                    return (IDictionary<TEnum, Texture2D>) HdIconsLibrary;
                default:
                    throw new ArgumentException($"{iconType} doesn't belong to any known icon library", nameof(iconType));
            }
        }

        /// <summary>Loads the icon library from a sprite sheet inside <see cref="IconFolder" /> folder.</summary>
        /// <param name="iconSheetPath">A path from the <see cref="IconFolder" /> to a single file containing multiple sprites.</param>
        /// <returns>A dictionary with icons, using icon names as keys.</returns>
        /// <remarks>Loads a different set of icons depending on the selected Editor skin.</remarks>
        static Dictionary<TEnum, Texture2D> LoadIconsFromSheet<TEnum>(string iconSheetPath) where TEnum : struct, Enum
        {
            var iconLibrary = new Dictionary<TEnum, Texture2D>();
            IEnumerable<Texture2D> icons = GetIconsFromSheet(string.Concat(IconFolder, "/", iconSheetPath));

            iconLibrary.AddIcons(icons);
            return iconLibrary;
        }

        /// <summary>Loads the icon library from the <see cref="IconFolder" /> folder.</summary>
        /// <param name="iconFolderPath">A path from the <see cref="IconFolder" /> to a single file containing multiple sprites.</param>
        /// <returns>A dictionary with icons, using icon names as keys.</returns>
        static Dictionary<TEnum, Texture2D> LoadIcons<TEnum>(string iconFolderPath) where TEnum : struct, Enum
        {
            var iconLibrary = new Dictionary<TEnum, Texture2D>();
            Texture2D[] icons = Resources.LoadAll<Texture2D>(string.Concat(IconFolder, "/", iconFolderPath));

            iconLibrary.AddIcons(icons);
            return iconLibrary;
        }

        /// <summary>Gets all sprites from a given sprite sheet with multiple sprites.</summary>
        /// <param name="resourcePath">Resources path to the icon spritesheet.</param>
        /// <returns>A collection of textures cropped from sprite sheet.</returns>
        /// <remarks>If the Unity skin is set to Dark, adds an outline around the icons.</remarks>
        static IEnumerable<Texture2D> GetIconsFromSheet(string resourcePath)
        {
            string skinSuffix = EditorConstants.IsDarkSkin ? DarkSkinSheetSuffix : string.Empty;

            // Get all sliced sprites in the texture and convert them to textures
            Sprite[] spriteAssets = Resources.LoadAll<Sprite>(resourcePath + skinSuffix);
            if (!string.IsNullOrEmpty(skinSuffix) && spriteAssets.Length == 0) spriteAssets = Resources.LoadAll<Sprite>(resourcePath);

            return spriteAssets.Select(sprite => sprite.ToTexture());
        }

        /// <summary>Gets all textures located in the Gizmos folder under Newgrounds namespace.</summary>
        /// <returns>All Newgrounds Gizmos icons.</returns>
        static IEnumerable<Texture2D> GetGizmosIcons()
        {
            string[] guids = AssetDatabase.FindAssets("t:Texture", new[] {NgEditorUtils.GizmosFolder});
            List<Texture2D> textures = guids.Select(AssetDatabase.GUIDToAssetPath).Select(AssetDatabase.LoadAssetAtPath<Texture2D>).ToList();
            textures.ForEach(texture =>
            {
                // "MedalComponent Icon" -> "Medal"
                const string gizmoSuffix = "Component Icon";
                if (texture.name.EndsWith(gizmoSuffix))
                {
                    int endIndex = texture.name.LastIndexOf(gizmoSuffix, StringComparison.Ordinal);
                    texture.name = texture.name.Substring(0, endIndex);
                }
            });
            return textures;
        }

        /// <summary>Gets an icon of a specified type.</summary>
        /// <typeparam name="TEnum">Icon library type.</typeparam>
        /// <param name="iconLibrary">The icon library to retrieve the icon from.</param>
        /// <param name="iconType">A specific icon to retrieve from the icon library.</param>
        /// <param name="silent">Display warning if an icon can't be found.</param>
        /// <returns>The desired icon or a default monochromatic texture.</returns>
        static Texture2D GetIconOrDefault<TEnum>(this IDictionary<TEnum, Texture2D> iconLibrary, TEnum iconType, bool silent = false) where TEnum : Enum
        {
            if (!iconLibrary.TryGetValue(iconType, out Texture2D icon) || icon == null)
            {
                RefreshIcons();
                iconLibrary = GetIconLibraryOf(iconType);

                if (!iconLibrary.TryGetValue(iconType, out icon) || icon == null)
                {
                    if (!silent) NgDebug.LogWarningFormat("The icon type <b>{0}</b> couldn't be found.", iconType);
                    icon = DefaultIcon;
                }
            }
            return icon;
        }

        /// <summary>Adds all given icons to the icon library (if they are defined in the enum) and checks for duplicates.</summary>
        /// <typeparam name="TEnum">Icon library type.</typeparam>
        /// <param name="iconLibrary">The icon library to put the icon into.</param>
        /// <param name="icons">The icons to add to the library.</param>
        /// <param name="silent">Display warning if an icon isn't defined or is already in there.</param>
        static void AddIcons<TEnum>(this IDictionary<TEnum, Texture2D> iconLibrary, IEnumerable<Texture2D> icons, bool silent = false) where TEnum : struct, Enum
        {
            foreach (Texture2D icon in icons)
            {
                if (Enum.TryParse(icon.name, true, out TEnum iconType))
                {
                    if (iconLibrary.ContainsKey(iconType) && !silent) NgDebug.LogWarningFormat(icon, "Duplicate icon: <i>{0}</i>", iconType);
                    else iconLibrary.Add(iconType, icon);
                }
                else
                {
                    if (!silent)
                        NgDebug.LogErrorFormat("The loaded icon <b>{0}</b> isn't defined in <i>{1}</i>.\n" +
                                               "Either the icon's name is incorrect or the value is missing from the enum.",
                                               icon.name, typeof(TEnum).Name);
                }
            }
        }

        /// <summary>Ensures a method is being used with a valid <see langword="enum" /> type.</summary>
        /// <typeparam name="TEnum">An <see langword="enum" /> defined in <see cref="IconEnums" />.</typeparam>
        /// <exception cref="InvalidOperationException">
        ///     <typeparamref name="TEnum" /> isn't an <see langword="enum" />; or the enum isn't defined within the
        ///     <see cref="NewgroundsIcons" /> class.
        /// </exception>
        static void CheckEnum<TEnum>()
        {
            Type enumType = typeof(TEnum);
            if (!enumType.IsEnum)
            {
                var exception = new InvalidOperationException("Generic type TEnum must be an enum: " + enumType.Name);
                NgDebug.LogException(exception);
                throw exception;
            }

            if (!IconEnums.Contains(enumType))
            {
                var exception = new InvalidOperationException("Generic type must be a valid NewgroundsIcons enum: " + enumType.Name);
                NgDebug.LogException(exception);
                throw exception;
            }
        }

    }

}
