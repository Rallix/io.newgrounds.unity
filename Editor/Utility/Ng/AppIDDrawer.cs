﻿using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor
{
    [CustomPropertyDrawer(typeof(AppIdAttribute))]
    class AppIdDrawer : PropertyDrawer
    {

        //! Since there's a warning box, this must override GetPropertyHeight
        // https://unity3d.com/learn/tutorials/topics/interface-essentials/property-drawers-custom-inspectors

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

        }

    }
}
