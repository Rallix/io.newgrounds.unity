﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniGifLibrary;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

using io.newgrounds.unity.editor.icons;
using io.newgrounds.unity.editor.utility;
using io.newgrounds.unity.editor.settings;

namespace io.newgrounds.unity.editor
{
    // TODO: Split into pieces
    // TODO: Remove 'Resources.Load' + 'Application.dataPath'

    // TODO: [InitializeOnLoadMethod] instead of [InitializeOnLoad] (class) which is triggered during every compilation

    /// <summary> All utilities directly related to Newgrounds. </summary>
    static class NgEditorUtils
    {
        /// <summary>The root folder of the Newgrounds.io runtime part of the package.</summary>
        internal static readonly string NewgroundsFolder = NgPath.CombinePackageRoot("Runtime");

        /// <summary>A fully qualified path in Assets/Gizmos folder.</summary>
        internal static readonly string GizmosFolder = GetNgGizmosFolderLocation();

        /// <summary>Component scripts to be loaded via button click.</summary>
        internal static readonly Dictionary<string, MonoScript> ComponentScripts = EditorMethods.LoadComponentScripts("Unity/Components");

        /// <summary>Gets the folder where component icons should be for the gizmo icons to take effect.</summary>
        /// <remarks>The path should be <c>Assets/Gizmos/Namespace</c>. This method considers the namespace of <see cref="CoreComponent"/>.</remarks>
        static string GetNgGizmosFolderLocation()
        {
            const string gizmosFolder = "Assets/Gizmos";

            string @namespace = typeof(CoreComponent).Namespace ?? string.Empty;
            return string.Concat(gizmosFolder, @namespace.Replace(".", "/"));
        }

        /// <summary> Searches the Newgrounds folder using the search filter string. </summary>
        /// <param name="filter"> The filter string can contain search data. </param>
        /// <param name="searchInFolders"> The folders inside the Newgrounds folder where the search will start. </param>
        /// <returns> Asset paths to the found assets. </returns>
        internal static string[] FindNgAssets(string filter, params string[] searchInFolders)
        {
            string[] ngFolders = searchInFolders.Select(folder => $"{NewgroundsFolder}/{folder}").ToArray();
            return AssetDatabase.FindAssets(filter, ngFolders).Select(AssetDatabase.GUIDToAssetPath).ToArray();
        }

        /// <summary>Creates a <see cref="GUIContent"/> with a given Newgrounds icon and optionally a tooltip.</summary>
        /// <param name="text">The text contained.</param>
        /// <param name="iconType">The icon contained.</param>
        /// <param name="tooltip">The tooltip of this element.</param>
        /// <returns>A new <see cref="GUIContent"/> with a given text and Newgrounds icon.</returns>
        internal static GUIContent GUIContentIcon<TEnum>(string text, TEnum iconType, string tooltip = "") where TEnum : Enum
        {
            return new GUIContent
            {
                    text = $"　{text}",                         // Typographic space to offset the text from the icon
                    image = NewgroundsIcons.GetIcon(iconType), // An icon to the left of the text
                    tooltip = tooltip                          // Tooltip displayed while hovering over the button
            };
        }

        /// <summary>Make a text field whose label has an icon in a box.</summary>
        /// <param name="ngIcon">The label icon.</param>
        /// <param name="label">A label to display in front of the text field.</param>
        /// <param name="text">The text to edit.</param>
        /// <param name="tooltip">The text displayed when the cursor hovers over the text label.</param>
        /// <param name="style">
        ///     The <see cref="GUIStyle" /> of the text field; defaults to the one defined by
        ///     <see cref="GUI.skin" />.
        /// </param>
        /// <param name="labelWidth">The reserved area for the label. The smaller it is, the wider is the text field.</param>
        /// <returns>The text entered by the user.</returns>
        internal static string IconTextField(NewgroundsIcons.SmallIcons ngIcon, string label, string text, GUIStyle style = null, string tooltip = "", float labelWidth = 145f)
        {
            Texture2D icon = NewgroundsIcons.GetIcon(ngIcon);
            return EditorMethods.IconTextField(icon, label, text, style, tooltip, labelWidth);
        }

        /// <summary>Gets a window and docks it to the window specified in <see cref="settings.NewgroundsSettings.dockableWindow" />.</summary>
        /// <typeparam name="T">The type of <see cref="EditorWindow" /> to create.</typeparam>
        /// <returns>A new, docked window.</returns>
        internal static T GetDockedWindow<T>() where T : EditorWindow
        {
            return EditorMethods.GetDockedWindow<T>(NgSettingsManager.Get<string>("editor.dockableWindow"));
        }

        internal static void DrawWebGLWarning()
        {
            EditorGUILayout.Space();
            EditorGUILayout.HelpBox("Your build target isn't set to WebGL which is required to publish on Newgrounds.", MessageType.Warning);
            if (EditorMethods.ColorButton(ColorPalette.NewgroundsOrange, "Set WebGL as the target platform"))
            {
                EditorMethods.SetTargetPlatform(BuildTarget.WebGL);
            }
        }

        /// <summary> Downloads an icon from an URL in the Editor. </summary>
        /// <param name="activeWindow"> An active <see cref="EditorWindow"/> to run a <see cref="EditorCoroutine"/> with. </param>
        /// <param name="iconUrl"> Icon to load. </param>
        /// <param name="iconLoaded"> A mandatory callback to use when the icon is loaded. </param>
        internal static IEnumerator DownloadIconEditor(this EditorWindow activeWindow, string iconUrl, Action<Texture2D> iconLoaded)
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(iconUrl))
            {
                yield return new WaitForWebRequest(www);

                if (www.isNetworkError || www.isHttpError)
                {
                    NgDebug.LogWarning(www.error);
                }
                else
                {
                    Texture2D icon = null;
                    const string gifExtension = ".gif"; // *Static* gif texture only
                    if (iconUrl.EndsWith(gifExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        //! Unity doesn't natively support GIF textures; only JPG and PNG
                        byte[] gifBytes = www.downloadHandler.data;

                        IEnumerator gifRoutine = UniGif.GetTextureListCoroutine(gifBytes, (gifTexList, loopCount, width, height) =>
                        {
                            icon = gifTexList[0].m_texture2d;
                        });

                        yield return activeWindow.StartCoroutine(gifRoutine);
                    }
                    else
                    {
                        //! JPG and PNG
                        icon = DownloadHandlerTexture.GetContent(www);
                    }
                    iconLoaded?.Invoke(icon);
                }
            }
        }
    }
}
