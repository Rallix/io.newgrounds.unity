﻿using UnityEditor;
using UnityEditor.SettingsManagement;

namespace io.newgrounds.unity.editor.settings
{
    //! Instead of EditorPrefs, use this file/json approach

    /// <summary> Newgrounds SettingsManager for adding an entry to the ProjectSettings folder. </summary>
    static class NgSettingsManager
    {
        static Settings instance;
        internal static Settings Instance
        {
            get
            {
                return instance ?? (instance = new Settings(new ISettingsRepository[]
                {
                        new PackageSettingsRepository(NgPath.RootNamespace, "NewgroundsSettings"),
                        new UserSettingsRepository()
                }));
            }
        }

        /* Common settings shortcuts */
        /// <summary>The global setting for the unique ID of your app as found in the 'API Tools' tab of your Newgrounds.com project.</summary>
        public static string GetAppId() => Get<string>("api.appId");
        /// <summary>The global setting for a base64-encoded, 128-bit AES encryption key as found in the 'API Tools' tab of your Newgrounds.com project.</summary>
        public static string GetEncryptonKey() => Get<string>($"api.aesBase64Key");
        
        //! Forward all save/load methods to the instance

        public static void Save()
        {
            Instance.Save();
        }

        public static T Get<T>(string key, SettingsScope scope = SettingsScope.Project, T fallback = default)
        {
            return Instance.Get(key, scope, fallback);
        }

         /// <summary> Sets a setting. </summary>
         /// <typeparam name="T"> The type of the setting. </typeparam>
         /// <param name="key"> A <see langword="string"/> key to save the setting under. </param>
         /// <param name="value"> The value to set the setting to. </param>
         /// <param name="scope"> Project or User setting? </param>
        public static void Set<T>(string key, T value, SettingsScope scope = SettingsScope.Project)
        {
            Instance.Set(key, value, scope);
        }

        /// <summary> Checks if a setting is present in the settings file. </summary>
        /// <typeparam name="T"> The type of the setting. </typeparam>
        /// <param name="key"> A <see langword="string"/> key under which the setting is saved. </param>
        /// <param name="scope"> Project or User setting? </param>
        /// <returns></returns>
        public static bool ContainsKey<T>(string key, SettingsScope scope = SettingsScope.Project)
        {
            return Instance.ContainsKey<T>(key, scope);
        }

        /// <summary> Deletes the settings file, effectively resetting the settings. </summary>
        public static void Reset()
        {
            // TODO: Test this
            FileUtil.DeleteFileOrDirectory(NgPath.ProjectRoot + "/ProjectSettings/Packages/" + NgPath.PackageRoot + "NewgroundsSettings.json");
        }
    }
}
