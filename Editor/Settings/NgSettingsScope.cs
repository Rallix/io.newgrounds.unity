﻿using UnityEditor;

namespace io.newgrounds.unity.editor.settings
{
    /// <summary> Creates a <see cref="EditorGUI.ChangeCheckScope"/> that saves settings when changes are made. </summary>
    public class NgSettingsScope : EditorGUI.ChangeCheckScope
    {
        protected override void CloseScope()
        {
            if (changed) NgSettingsManager.Save();
            base.CloseScope();
        }
    }
}
