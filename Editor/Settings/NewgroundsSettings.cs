﻿using UnityEditor;
using UnityEditor.SettingsManagement;
using UnityEngine;

namespace io.newgrounds.unity.editor.settings
{
    /// <summary> A class handling the Project Settings for Newgrounds. </summary>
    /// <remarks>
    ///     A partial class with nested classes each dealing with drawers for a specific category.
    /// </remarks>
    static partial class NewgroundsSettings
    {

        // TODO: Make sure 'searchContext' hides appropriate parts, including labels and help boxes
        //! Search context hides everything well, but only once

        /// <summary> The path to the settings page in Unity. </summary>
        internal const string SettingsPath = "Project/Newgrounds";

        /// <summary>Opens the Newgrounds settings.</summary>        
        [MenuItem(NewgroundsMenuTools.BasePath + "Open Settings", priority = -100)]
        internal static void OpenSettings()
        {
            SettingsService.OpenProjectSettings(SettingsPath);
        }

        const string BuildCategory = "Build";

        [UserSetting] static NgSettings<int> pageId = new NgSettings<int>($"build.{nameof(pageId)}", 0);
        [UserSetting] static NgSettings<bool> zipAfterBuild = new NgSettings<bool>($"build.{nameof(zipAfterBuild)}", true);

        [UserSettingBlock(BuildCategory)]
        static void BuildSettingsGUI(string searchContext)
        {
            bool buildSearchCheck = SearchableLayoutExtras.SearchCheck(searchContext, "Page ID");
            if (buildSearchCheck)
            {
                if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.WebGL)
                {
                    NgEditorUtils.DrawWebGLWarning();
                }
                BuildDrawers.DrawProjectSystemButton(pageId);
            }
            using (new NgSettingsScope())
            {
                pageId.SetValue(SettingsGUILayout.SearchableIntField(new GUIContent("Page ID", "The numeric ID of your game page: www.newgrounds.com/projects/games/(PAGE ID)/"), pageId, searchContext));
                zipAfterBuild.SetValue(SettingsGUILayout.SearchableToggle(new GUIContent("Zip after build", "When the project is built for the WebGL platform, zip the resulting build so it can be uploaded to Newgrounds."), zipAfterBuild, searchContext));
            }
        }

        const string ApiToolsCategory = "API Tools";

        /// <summary> The App ID used in the demo project. </summary>
        const string DemoAppId = "46284:tCpPGS1h"; //? 45896:URBzcIBT
        /// <summary> The encryption key used in the demo project. </summary>
        const string DemoEncryptionKey = "T+wkFrUzgffaj4i0x/cBJg=="; //? CREpvHlrrfRi+A/LFASUyA==

        [UserSetting] static NgSettings<string> appId = new NgSettings<string>($"api.{nameof(appId)}", DemoAppId);
        [UserSetting] static NgSettings<string> aesBase64Key = new NgSettings<string>($"api.{nameof(aesBase64Key)}", DemoEncryptionKey);

        /// <summary> Checks if neither of the API settings uses the default value. </summary>
        internal static bool IsApiDefault() => appId == DemoAppId || aesBase64Key == DemoEncryptionKey;
        /// <summary> Checks if neither of the API settings uses the default value. </summary>
        internal static bool IsApiEmpty() => string.IsNullOrWhiteSpace(appId) || string.IsNullOrWhiteSpace(aesBase64Key);
        /// <summary> Checks if neither of the API settings uses the default value or is empty. </summary>
        internal static bool IsApiEmptyOrDefault() => IsApiEmpty() || IsApiDefault();

        [UserSettingBlock(ApiToolsCategory)]
        static void ApiToolsSettingsGUI(string searchContext)
        {
            using (new NgSettingsScope())
            {
                appId.SetValue(ApiToolsDrawers.DrawAppID(appId, searchContext));
                aesBase64Key.SetValue(ApiToolsDrawers.DrawEncryptionKey(aesBase64Key, searchContext));
            }
        }

        const string EditorCategory = "Editor";

        // Dock to window
        // Default/2D/3D Behaviour
        // Color palette

        [UserSetting] static NgSettings<string> ngAssetLocation = new NgSettings<string>($"editor.{nameof(ngAssetLocation)}", Application.dataPath);
        [UserSetting] static NgSettings<int> dockableWindow = new NgSettings<int>($"editor.{nameof(dockableWindow)}", 0);
        [UserSetting] static NgSettings<bool> addCoreAutomatically = new NgSettings<bool>($"editor.{nameof(addCoreAutomatically)}", false);
		
        [UserSettingBlock(EditorCategory)]
        static void EditorSettingsGUI(string searchContext)
        {
            using (new NgSettingsScope())
            {
                ngAssetLocation.SetValue(EditorDrawers.DrawNgAssetLocation(ngAssetLocation, searchContext));
                dockableWindow.SetValue(EditorDrawers.DrawDockToWindow(dockableWindow, searchContext));
                addCoreAutomatically.SetValue(EditorDrawers.DrawAutoAddCore(addCoreAutomatically, searchContext));
            }
        }
    }
}
