using io.newgrounds.unity.editor.icons;
using UnityEngine;

namespace io.newgrounds.unity.editor.settings
{
    static partial class NewgroundsSettings
    {
        /// <summary> Draws the Newgrounds build settings category. </summary>
        static class BuildDrawers
        {
            /// <summary>Draws a button which links to the project page on Newgrounds and a related ID input field.</summary>
            internal static void DrawProjectSystemButton(int pageId)
            {
                var buttonStyle = new GUIStyle(GUI.skin.button) {alignment = TextAnchor.MiddleLeft, imagePosition = ImagePosition.ImageLeft};
                if (GUILayout.Button(NgEditorUtils.GUIContentIcon("Open Project System",
                                                                  NewgroundsIcons.SmallIcons.Projects,
                                                                  "Open the Project System page of your game in the browser."),
                                     buttonStyle))
                {
                    OpenProjectSystem(pageId);
                }
            }

            /// <summary> Opens the Project System in the default browser. </summary>
            static void OpenProjectSystem(int gamePageId = 0)
            {
                string url = (gamePageId <= 0)
                        ?  "https://www.newgrounds.com/projects/games"
                        : $"https://www.newgrounds.com/projects/games/{gamePageId}/details/";
                Application.OpenURL(url);
            }
        }
    }
}
