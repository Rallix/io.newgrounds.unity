﻿using UnityEditor.SettingsManagement;
using UnityEngine;

namespace io.newgrounds.unity.editor.settings
{
    static partial class NewgroundsSettings
    {
        /// <summary> Draws the Editor settings category for Newgrounds Project System. </summary>
        internal static class EditorDrawers
        {
            /// <summary> Names of a select few Editor windows to dock windows to. </summary>
            internal enum DockableWindows
            {
                None,
                SceneView,
                SceneHierarchyWindow,
                GameView,
                InspectorWindow
            }

            internal static int DrawDockToWindow(int dockableWindow, string searchContext)
            {
                var label = new GUIContent("Dock to Window", "Dock the Newgrounds Window and other tools next to this window.");
                // dockableWindow = (int) (DockableWindows) EditorGUILayout.EnumPopup(label, (DockableWindows) dockableWindow);
                dockableWindow = (int) (DockableWindows) SearchableLayoutExtras.SearchableEnumPopup(label, (DockableWindows) dockableWindow, searchContext);
                return dockableWindow;
            }

            internal static bool DrawAutoAddCore(bool addCore, string searchContext)
            {
                var label = new GUIContent("Add Core in Playmode", "Automatically adds a temporary Ngio Core (if needed) when the Editor enters Playmode.");
                addCore = SettingsGUILayout.SearchableToggle(label, addCore, searchContext);
                return addCore;
            }

            public static string DrawNgAssetLocation(string assetLocation, string searchContext)
            {
                var label = new GUIContent("Generated Assets Location", "The folder to place general Newgrounds asset files (medals, scoreboards) in.");
                assetLocation = SearchableLayoutExtras.FolderPathField(assetLocation, label, searchContext);
                return assetLocation;
            }
        }
    }
}
