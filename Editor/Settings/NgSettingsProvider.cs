﻿using UnityEditor;
using UnityEditor.SettingsManagement;

namespace io.newgrounds.unity.editor.settings
{
    /// <summary> Settings provider for the Preferences window. </summary>
    static class NgSettingsProvider
    {
        [SettingsProvider]
        static SettingsProvider CreateSettingsProvider()
        {
            var provider = new UserSettingsProvider(NewgroundsSettings.SettingsPath,
                                                    NgSettingsManager.Instance,
                                                    new[] {typeof(NgSettingsProvider).Assembly},
                                                    SettingsScope.Project);
            return provider;
        }
    }
}
