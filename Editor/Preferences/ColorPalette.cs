﻿using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace io.newgrounds.unity.editor
{
    static class ColorPalette
    {

        // TODO: Create color swatches / palette inspector | Scriptable Object palette

        /// <summary>The most common orange colour used on Newgrounds.</summary>
        internal static readonly Color NewgroundsOrange = new Color32(0xFD, 0xA2, 0x38, 0xFF);

        internal enum HtmlColors
        {

            /// <summary>Solid #FF0000 red.</summary>
            Red,

            /// <summary>Solid green and blue mixture #00FFFF.</summary>
            Cyan,

            /// <summary>Solid #0000FF blue.</summary>
            Blue,

            /// <summary>Low #00008B blue.</summary>
            DarkBlue,

            /// <summary>Clear #ADD8E6 blue.</summary>
            LightBlue,

            /// <summary>Red and blue #800080 equally mixed.</summary>
            Purple,

            /// <summary>Solid red and green #FFFF00 mixture.</summary>
            Yellow,

            /// <summary>Solid #00FF00 green.</summary>
            Lime,

            /// <summary>Solid red and green #FF00FF mixture; magenta.</summary>
            Fuchsia,

            /// <summary>Pure #FFFFFF white.</summary>
            White,

            /// <summary>Light #C0C0C0 grey.</summary>
            Silver,

            /// <summary>Regular #808080 grey.</summary>
            Grey,

            /// <summary>Solid #000000 black.</summary>
            Black,

            /// <summary>Regular #FFA500 orange.</summary>
            Orange,

            /// <summary>Regular #A52A2A brown.</summary>
            Brown,

            /// <summary>Dark #800000 red.</summary>
            Maroon,

            /// <summary>Regular #008000 green.</summary>
            Green,

            /// <summary>Regular #808000 red and green mixture.</summary>
            Olive,

            /// <summary>Dark #000080 blue.</summary>
            Navy,

            /// <summary>Dark #008080 cyan.</summary>
            Teal,

            /// <summary>Solid #00FFFF cyan.</summary>
            Aqua,

            /// <summary>Solid red and green #FF00FF mixture; magenta.</summary>
            Magenta

        }

        /// <summary>A list of the main colours Newgrounds uses throughout the site.</summary>
        internal enum NgColors
        {

            /// <summary>The main orange colour used on Newgrounds.</summary>
            Orange,

            /// <summary>The violet colour used for Under Judgement content on the Portal.</summary>
            UnderJudgement,

            /// <summary>The green colour used for popular content on the Portal.</summary>
            GreatScore,

            /// <summary>The red colour used for unpopular content on the Portal.</summary>
            VergeOfDeath,

            /// <summary>The blue colour used for the most popular content on the Portal.</summary>
            AwesomeScore,

            /// <summary>The red colour related to the Movies section of the site.</summary>
            Movies,

            /// <summary>The blue colour related to the Games section of the site.</summary>
            Games,

            /// <summary>The green colour related to the Audio section of the site.</summary>
            Audio,

            /// <summary>The pink colour related to the Art section of the site.</summary>
            Art,

            /// <summary>The violet colour related to the Portal section of the site.</summary>
            Portal,

            /// <summary>The orange colour related to the Community section of the site.</summary>
            Community,

            /// <summary>The yellow colour related to the Your Feed section of the site.</summary>
            YourFeed

        }

        /// <summary>The text colour of input fields which may contain incorrect values.</summary>
        internal static Color WarningColor
        {
            get { return EditorConstants.IsDarkSkin ? Color.yellow : GetHtmlColor(HtmlColors.Maroon); }
        }

        /// <summary>The text colour of correctly filled input fields.</summary>
        internal static Color FineColor
        {
            get { return EditorConstants.IsDarkSkin ? Color.green : GetHtmlColor(HtmlColors.Green); }
        }

        [Pure]
        internal static Color GetNgColor(NgColors colorType)
        {
            string htmlString;

            // Set the colours
            switch (colorType)
            {
                default:
                    return NewgroundsOrange;
                // htmlString = "#fda238"; // #d36c36 visited
                case NgColors.UnderJudgement:
                    htmlString = "#c767e5"; // #86489a visited
                    break;
                case NgColors.GreatScore:
                    htmlString = "#60b136"; // #417526 visited
                    break;
                case NgColors.VergeOfDeath:
                    htmlString = "#fda238";
                    break;
                case NgColors.AwesomeScore:
                    htmlString = "#3a94e0"; // #296395 visited
                    break;
                case NgColors.Movies:
                    htmlString = "#f62f36";
                    break;
                case NgColors.Games:
                    htmlString = "#2684de";
                    break;
                case NgColors.Audio:
                    htmlString = "#47b32a";
                    break;
                case NgColors.Art:
                    htmlString = "#f136a1";
                    break;
                case NgColors.Portal:
                    htmlString = "#c64be2";
                    break;
                case NgColors.Community:
                    htmlString = "#ec5216";
                    break;
                case NgColors.YourFeed:
                    htmlString = "#f1b859";
                    break;
            }

            return GetHtmlColor(htmlString);
        }

        /// <summary>Attempts to convert a html color string and returns <paramref name="defaultColor" /> if it fails.</summary>
        /// <param name="htmlString">Case insensitive html string like <c>#RRGGBB</c> to be converted into a color.</param>
        /// <param name="defaultColor">Default colour to use if the conversion fails.</param>
        /// <returns>The converted color or <paramref name="defaultColor" /> if the conversion fails.</returns>
        [Pure]
        internal static Color GetHtmlColor(string htmlString, Color defaultColor)
        {
            return ColorUtility.TryParseHtmlString(htmlString, out Color color) ? color : defaultColor;
        }

        /// <summary>Attempts to convert a html color string and returns <see cref="NewgroundsOrange" /> if it fails.</summary>
        /// <param name="htmlString">Case insensitive html string like <c>#RRGGBB</c> to be converted into a color.</param>
        /// <returns>The converted color or <see cref="NewgroundsOrange" /> if the conversion fails.</returns>
        [Pure]
        internal static Color GetHtmlColor(string htmlString)
        {
            return GetHtmlColor(htmlString, Color.magenta);
        }

        /// <summary>Gets a colour from a selection of named html colours.</summary>
        /// <param name="color">A named html color.</param>
        /// <returns>The converted color or <see cref="NewgroundsOrange" /> if no such color can be found.</returns>
        [Pure]
        internal static Color GetHtmlColor(HtmlColors color)
        {
            string htmlString = color.ToString().ToLower();
            return GetHtmlColor(htmlString);
        }

        /// <summary>Checks whether a colour is dark enough to be considered dark.</summary>
        /// <param name="color">A colour to inspect.</param>
        /// <param name="alphaThreshold">All colours whose opacity falls below this threshold will be ignored and return <c>false</c>.</param>
        /// <param name="greyThreshold">
        ///     A colour threshold determining the darkness of a colour component.
        ///     The smaller it is, the lower is the tolerance.
        /// </param>
        /// <returns>True if all three colour components are smaller than the threshold.</returns>
        [Pure]
        internal static bool IsDark(this Color color, float alphaThreshold = 0.85f, float greyThreshold = 0.6f)
        {
            if (color.a < Mathf.Clamp01(alphaThreshold)) return false;

            float[] components = {color.r, color.g, color.b};
            return components.All(value => value <= Mathf.Clamp01(greyThreshold));
        }

    }
}
