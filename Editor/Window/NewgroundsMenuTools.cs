using System.IO;
using SimpleJSON;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using io.newgrounds.unity.editor.setup;

namespace io.newgrounds.unity.editor
{
    /// <summary>All commands accessible through the top context menu.</summary>
    static class NewgroundsMenuTools
    {
        /// <summary>Base path to runnable commands via the top context menu.</summary>
        internal const string BasePath = "Window/Newgrounds/";

        [MenuItem(BasePath + "Open Welcome Window", priority = -100)]
        public static void OpenWelcomeWindow()
        {
            NewgroundsWelcome.Display().Show();
        }

        [MenuItem(BasePath + "Open Setup Wizard", priority = -100)]
        public static void OpenSetupWizard()
        {
            NewgroundsSetup.Display().Show();
        }

        [MenuItem(BasePath + "Add Core Prefab", false, -5)]
        static void InstantiateNgCore()
        {
            CoreComponent core = CoreInitializer.AddCorePrefab();
            Selection.activeObject = core;
        }

        [MenuItem(BasePath + "Add Core Prefab", true)]
        static bool NoNgCore()
        {
            //! Don't allow adding if there already is another core component
            return Object.FindObjectOfType<CoreComponent>() == null;
        }

        /// <summary>Add Component commands path.</summary>
        const string AddComponentPath = BasePath + "Add Component To Selection/";

        [MenuItem(AddComponentPath + "User", false, 5)]
        static void AddUserComponent()
        {
            EditorMethods.AddComponentToSelected<UserComponent>();
        }

        [MenuItem(AddComponentPath + "Login and Logout", false, 10)]
        static void AddLoginComponent()
        {
            EditorMethods.AddComponentToSelected<LoginComponent>();
        }

        [MenuItem(AddComponentPath + "Medal", false, 20)]
        static void AddMedalComponent()
        {
            EditorMethods.AddComponentToSelected<MedalComponent>();
        }

        [MenuItem(AddComponentPath + "Scoreboard", false, 25)]
        static void AddScoreboardComponent()
        {
            EditorMethods.AddComponentToSelected<ScoreboardComponent>();
        }

        [MenuItem(AddComponentPath + "Save and Load", false, 30)]
        static void AddSaveLoadComponent()
        {
            EditorMethods.AddComponentToSelected<SaveLoadComponent>();
        }

        [MenuItem(AddComponentPath + "Spyglass", false, 50)]
        static void AddSpyglassComponent()
        {
            EditorMethods.AddComponentToSelected<SpyglassComponent>();
        }

        [MenuItem(AddComponentPath + "User", true)]
        [MenuItem(AddComponentPath + "Login and Logout", true)]
        [MenuItem(AddComponentPath + "Medal", true)]
        [MenuItem(AddComponentPath + "Scoreboard", true)]
        // [MenuItem(AddComponentPath + "Save and Load", true)]
        [MenuItem(AddComponentPath + "Spyglass", true)]
        static bool ValidateSelection()
        {
            // Allow adding components only if an object is selected
            return Selection.gameObjects.Length > 0;
        }

        [MenuItem(AddComponentPath + "Save and Load", true, 30)]
        static bool ValidateDisable() => false;

        // TODO: #if UNITY_???? to disable this way of updating in newer versions of Unity

        /// <summary> Removes the package from the <see langword="packages-lock.json"/> file, which causes the package to be updated automatically. </summary>
        /// <remarks> This only works if the package was added via a link, not as an embedded package.</remarks>
        [MenuItem(BasePath + "Check for Updates", priority = 115)]
        static void CheckForUpdates()
        {
            if (!EditorUtility.DisplayDialog("Update Newgrounds.io package",
                                             "Do you wish to trigger an update (by removing the package lock)?\n\n" +
                                             "This will only work if the package was installed via a git link. " +
                                             "Otherwise, you need to update the package manually by downloading a new version from the project's repository.\n\n" +
                                             "In newer versions of Unity, the Update option is already available directly in the Package Manager.",
                                             "Confirm", "Cancel")) return;

            // string packageManifestPath = Path.Combine(NgPath.ProjectRoot, "Packages", "manifest.json");
            string packageLockPath = Path.Combine(NgPath.ProjectRoot, "Packages", "packages-lock.json");
            if (!File.Exists(packageLockPath))
            {
                Debug.LogError($"Can't find the package lock file:\n<i>{packageLockPath}</i>");
                return;
            }

            JObject packageLock = JSONDecoder.Decode(File.ReadAllText(packageLockPath));
            const string dependencies = "dependencies";
             
            if (!packageLock.ObjectValue.ContainsKey(dependencies) || 
                !packageLock.ObjectValue[dependencies].ObjectValue.ContainsKey(NgPath.RootNamespace))
            {
                Debug.LogError($"Can't find the package <b>{NgPath.RootNamespace}</b> in the package lock's list of dependencies.");
                return;
            }

            packageLock[dependencies][NgPath.RootNamespace].ObjectValue.Clear();

            using (var writer = new StreamWriter(packageLockPath))
            {
                var jsonWriter = new JSONStreamEncoder(writer);
                jsonWriter.WriteJObject(packageLock);
            }

            // Trigger package reimport
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh(); 
        }
    }
}
