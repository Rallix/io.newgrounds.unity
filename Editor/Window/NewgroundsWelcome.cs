﻿using System;
using System.Linq;
using io.newgrounds.unity.editor.setup;
using io.newgrounds.unity.editor.icons;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using ComponentIcons = io.newgrounds.unity.editor.icons.NewgroundsIcons.ComponentIcons;
using GameObject = UnityEngine.GameObject;

namespace io.newgrounds.unity.editor
{
    /// <summary>A welcome window automatically shown the very first time the package is imported.</summary>
    [InitializeOnLoad]
    class NewgroundsWelcome : EditorWindow
    {
        const string ShowOnStartKey = "Newgrounds.io Show Welcome";
        static bool showAtStart; // TODO: Only once a day

        /// <summary>Should the window be shown at the start of the editor (saved in the <see cref="EditorPrefs"/>)?</summary>
        static bool ShowAtStart
        {
            get { return EditorPrefs.GetBool(ShowOnStartKey, true); }
            set { EditorPrefs.SetBool(ShowOnStartKey, value); }
        }

        static NewgroundsWelcome()
        {
            EditorApplication.update += ShowWindowFirstTime;
        }

        static void ShowWindowFirstTime()
        {
            showAtStart = ShowAtStart;

            EditorApplication.update -= ShowWindowFirstTime;
            if (showAtStart && !Application.isPlaying)
            {
                Display().Show();
            }
        }

        internal static NewgroundsWelcome Display()
        {
            var window = GetWindow<NewgroundsWelcome>(false, "⠀Welcome");
            window.titleContent.image = NewgroundsIcons.GetIcon(NewgroundsIcons.UtilityIcons.Logo);
            return window;
        }

        /// <summary>Keeps track of the scroll position inside of the editor window.</summary>
        Vector2 scrollPosition;

        static readonly Vector2 InitialWindowSize = new Vector2(505, 630);

        void OnInspectorUpdate()
        {
            // Repaint the window even when out of focus (e.g. when object selection changes)
            Repaint();
        }

        void OnGUI()
        {
            position.Set(position.x, position.y, InitialWindowSize.x, InitialWindowSize.y);

            using (var scrollViewScope = new EditorGUILayout.ScrollViewScope(scrollPosition, false, false, GUIStyle.none, GUI.skin.verticalScrollbar, GUIStyle.none))
            {
                scrollPosition = scrollViewScope.scrollPosition;

                DrawLogo();
                DrawIntroText();
                DrawLinks();
            }
        }

        void DrawLogo()
        {
            Texture2D logo = NewgroundsIcons.GetIcon(NewgroundsIcons.UtilityIcons.Icon);
            var iconSize = new Vector2(position.width, 100);
            Color textColor = EditorConstants.IsDarkSkin ? Color.white : Color.black;

            var iconContent = new GUIContent
            {
                    image = logo,
                    text = "Newgrounds.io for Unity",
            };
            var iconStyle = new GUIStyle(GUI.skin.box)
            {
                    alignment = TextAnchor.MiddleLeft,
                    fixedWidth = iconSize.x,
                    fixedHeight = iconSize.y,

                    normal = { textColor = textColor },
                    active = { textColor = textColor },

                    fontSize = 24,
                    border = new RectOffset(5, 5, 5, 5),
                    fontStyle = FontStyle.Bold,
            };

            EditorGUILayout.LabelField(iconContent, iconStyle);
        }

        void DrawIntroText()
        {
            EditorMethods.HorizontalLine();
            EditorGUILayout.HelpBox("Welcome to Newgrounds.io for Unity!\n\n" +
                                    "Click the buttons below to quickly setup your project to use on Newgrounds.\n" +
                                    $"You can reopen this window anytime under {NewgroundsMenuTools.BasePath.Replace("/", " > ")}Open Welcome Window.", MessageType.None);
            if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.WebGL) NgEditorUtils.DrawWebGLWarning();
            EditorGUILayout.Space();
            DrawShowOnStartToggle();
            EditorMethods.HorizontalLine();
        }

        void DrawLinks()
        {
            EditorMethods.HeaderField("Setup");
            DrawSetupButtons();
            EditorMethods.HorizontalLine();

            EditorMethods.HeaderField("Components");
            DrawComponentGrid();
            EditorMethods.HorizontalLine();

            //! Add helpful links, preloader, metagame handler
            // EditorMethods.HeaderField("Additional tools");
            // DrawAdditionalTools();
            // EditorMethods.HorizontalLine();
        }

        void DrawSetupButtons()
        {
            const int buttonSize = 96;
            GUILayoutOption[] guiOptions = { GUILayout.Width(buttonSize), GUILayout.Height(buttonSize / 2f) };

            using (new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button(new GUIContent("Read\nIntroduction", "Introduction to the original Newgrounds.io library"), guiOptions))
                {
                    Application.OpenURL("https://www.newgrounds.io/");
                }
                if (GUILayout.Button(new GUIContent("Setup\nWizard", "One-time setup of a Newgrounds.io project"), guiOptions))
                {
                    NewgroundsSetup.Display().Show();
                }
                if (GUILayout.Button(new GUIContent("Open\nSettings", "Change the settings of your Newgrounds.io project"), guiOptions))
                {
                    settings.NewgroundsSettings.OpenSettings();
                }
                if (GUILayout.Button(new GUIContent("Show\nRepository", "Show the GitLab repository of the library"), guiOptions))
                {
                    Application.OpenURL("https://gitlab.com/Rallix/newgrounds-io-unity");
                }
                CoreInitializer.AddCoreButton(buttonSize);
            }
        }

        /// <summary>Which component button is currently selected.</summary>
        int gridSelection = -1;

        void DrawComponentGrid()
        {
            // Component (HD) icons are too large -> resize
            Vector2 componentIconSize = 16 * Vector2.one; // pixels

            // The layout of component button (keep space for an icon)
            GUILayoutOption[] layoutOptions = { GUILayout.MaxWidth(position.width - 6.5f) };

            // The style of component buttons with an icon
            var buttonStyle = new GUIStyle(GUI.skin.button)
            {
                    alignment = TextAnchor.MiddleLeft,
                    imagePosition = ImagePosition.ImageLeft,
                    richText = true
            };

            var components = new[]
            {
                    new GUIContent { text = "User", image = NewgroundsIcons.GetIcon(ComponentIcons.User) },
                    new GUIContent { text = "Spyglass", image = NewgroundsIcons.GetIcon(ComponentIcons.Spyglass) },
                    new GUIContent { text = "Medal", image = NewgroundsIcons.GetIcon(ComponentIcons.Medal) },
                    new GUIContent { text = "Scoreboard", image = NewgroundsIcons.GetIcon(ComponentIcons.Scoreboard) },
                    new GUIContent { text = "Login and Logout", image = NewgroundsIcons.GetIcon(ComponentIcons.Login) },
                    // new GUIContent { text = "Save and Load", image = NewgroundsIcons.GetIcon(ComponentIcons.SaveLoad) },
            };

            // Space between the text and the icon 
            foreach (GUIContent guiContent in components) guiContent.text = "　" + guiContent.text;

            using (new EditorGUIUtility.IconSizeScope(componentIconSize))
            {
                gridSelection = GUILayout.SelectionGrid(gridSelection, components, 3, buttonStyle, layoutOptions);
            }

            string helpBoxMessage = string.Empty;
            Type addComponentType = null;
            switch (gridSelection)
            {
                case 0:
                    helpBoxMessage = "The User component maintains the connection between a logged user and Newgrounds and also provides access to basic information, such as the username.";
                    addComponentType = typeof(UserComponent);
                    break;
                case 1:
                    helpBoxMessage = "The Spyglass component is used in combination with the user component and provides a slightly slower access to all information available from the user's userpage.";
                    addComponentType = typeof(SpyglassComponent);
                    break;
                case 2:
                    helpBoxMessage = "The Medal component handles unlocking medals made available through the Project System.\nCreate a Medal asset and find them in the Medal Inspector.";
                    addComponentType = typeof(MedalComponent);
                    break;
                case 3:
                    helpBoxMessage = "The Scoreboard component handles the player scores and submitting them to the scoreboard made available through the Project System.";
                    addComponentType = typeof(ScoreboardComponent);
                    break;
                case 4:
                    helpBoxMessage = "The Login and Logout component allows previously unlogged user to log via a pre-made login window.";
                    addComponentType = typeof(LoginComponent);
                    break;
                case 5:
                    helpBoxMessage = "The Save and Load component allows you to store certain player data.";
                    addComponentType = typeof(SaveLoadComponent);
                    break;
            }

            if (gridSelection >= 0 && gridSelection < components.Length) // Something is selected
            {
                DrawSelectedComponent(addComponentType, components[gridSelection].image, 
                                      helpBoxMessage, componentIconSize, buttonStyle, layoutOptions);
            }
        }

        static void DrawSelectedComponent(Type addComponentType, Texture componentIcon, 
                                          string helpBoxMessage, Vector2 componentIconSize, 
                                          GUIStyle buttonStyle, GUILayoutOption[] layoutOptions)
        {
                string selectedComponent = addComponentType?.Name.ReplaceFirst("Component", string.Empty);
                if (new[] { "Spyglass", "Login" }.Contains(selectedComponent)) selectedComponent = "User"; // all used here
                
                EditorGUILayout.HelpBox(helpBoxMessage, MessageType.Info);

                string demoFolderPath = "Assets/Samples/Newgrounds.io";
                string demoScenePath = string.Empty;

                string[] sceneGuids = { };
                if (AssetDatabase.IsValidFolder(demoFolderPath))
                {
                    //? Can cause problems if somebody adds more scenes to one folder
                    sceneGuids = AssetDatabase.FindAssets($"t:scene {selectedComponent}", new[] { demoFolderPath });
                    if (sceneGuids.Length > 0) demoScenePath = AssetDatabase.GUIDToAssetPath(sceneGuids[0]);
                }

                bool demoSceneExists = !string.IsNullOrEmpty(demoScenePath);

                Texture2D sceneAssetIcon = UnityIcons.GetIcon(UnityIcons.InternalIcons.SceneAssetIcon);
                var content = new GUIContent(demoSceneExists ? "　Open sample scene" : "　Sample scene was not imported or doesn't exist",
                                             sceneAssetIcon, "Open the provided scene with an example of usage of this component");

                using (new EditorGUIUtility.IconSizeScope(componentIconSize))
                {
                    using (new EditorGUI.DisabledGroupScope(!demoSceneExists))
                    {
                        if (GUILayout.Button(content, buttonStyle, layoutOptions))
                        {
                            if (sceneGuids.Length > 1)
                            {
                                string[] names = sceneGuids.Select(AssetDatabase.GUIDToAssetPath).Select(path => path.Substring(path.LastIndexOf('/') + 1)).ToArray();
                                NgDebug.LogWarningFormat("There are multiple scenes inside a demo folder when there should be just one: <b>{0}</b>", string.Join("</b>, <b>", names));
                            }
                            EditorSceneManager.OpenScene(demoScenePath);
                        }
                    }
                }

                EditorGUILayout.Space();

                using (new EditorGUIUtility.IconSizeScope(componentIconSize))
                {
                    bool somethingSelected = Selection.gameObjects.Length != 0;
                    string label = string.Format(somethingSelected ? "　Add <b>{0}</b> to selection" : "　Select an object to hold the <b>{0}</b>", addComponentType?.Name ?? string.Empty);
                    using (new EditorGUI.DisabledScope(!somethingSelected))
                    {
                        if (GUILayout.Button(new GUIContent(label, componentIcon), buttonStyle, layoutOptions))
                        {
                            foreach (GameObject gameObject in Selection.gameObjects) gameObject.AddComponent(addComponentType);
                        }
                    }
                }
        }

        void DrawAdditionalTools()
        {
            EditorGUILayout.HelpBox("UNDER CONSTRUCTION", MessageType.Warning);
            //? Icons, file templates, folder recolouring, hierarchy icon etc. (LOW PRIORITY)
        }


        void DrawShowOnStartToggle()
        {
            var label = new GUIContent("Show at start", "Should this window be shown every time the editor starts?");
            bool newValue = EditorGUILayout.ToggleLeft(label, showAtStart);

            if (newValue != showAtStart)
            {
                ShowAtStart = showAtStart = newValue;
            }
        }
    }
}
