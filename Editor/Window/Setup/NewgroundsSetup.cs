﻿using System.Linq;
using io.newgrounds.objects;
using UnityEditor;
using UnityEngine;
using io.newgrounds.unity.editor.icons;
using io.newgrounds.unity.editor.settings;

namespace io.newgrounds.unity.editor.setup
{
    /// <summary> A user-friendly setup wizard for Newgrounds components. </summary>
    partial class NewgroundsSetup : EditorWindow
    {
        /// <summary> An editor-only instance of <see cref="core"/> component. </summary>
        protected static core Core { get; private set; }
        protected static user User => Core.current_user?.name != null ? Core.current_user : default;

        protected static EditorWindow SetupWindow { get; private set; }

        /// <summary> The phases of the setup window. </summary>
        static readonly SetupTab[] SetupTabs =
        {
                new SettingsTab(),
                new CoreTab(),
                new MedalsTab(),
                new ScoreboardTab(),
                // new TipsTab()
        };

        static int currentTabIndex = 0;
        /// <summary> Checks if the user is currently connected to the Internet. </summary>
        protected static bool InternetReachability => EditorMethods.CheckConnectivity();

        /// <summary> Returns the number of available tabs of the setup window. </summary>
        static int TabCount => SetupTabs.Length;

        /// <summary> Opens Setup window. </summary>        
        internal static NewgroundsSetup Display()
        {
            var window = NgEditorUtils.GetDockedWindow<NewgroundsSetup>();
            window.titleContent = new GUIContent("　Setup Wizard",
                                                 NewgroundsIcons.GetIcon(NewgroundsIcons.UtilityIcons.Logo));
            SetupWindow = window;
            return window;
        }

        void OnEnable()
        {
            SetupWindow = this;
            Core = AddTemporaryCore();
            if (!InternetReachability)
                Debug.LogWarning("You don't seem to be connected to the Internet, therefore" +
                                 " the setup wizard might not function properly.");

            var medalsTab = (MedalsTab)SetupTabs.FirstOrDefault(t => t.GetType() == typeof(MedalsTab));
            medalsTab?.PreloadNgObjectsAsync();
            var scoreboardTab = (ScoreboardTab)SetupTabs.FirstOrDefault(t => t.GetType() == typeof(ScoreboardTab));
            scoreboardTab?.PreloadNgObjectsAsync();
            /*
            foreach (SetupTab setupTab in SetupTabs.Where(t => t.GetType() == typeof(AssetTab<,>)))
            {
                var assetTab = setupTab as AssetTab<NgData, Model>;
                assetTab?.PreloadNgObjectsAsync();
            }*/
        }

        void OnDisable()
        {
            //! Destroy the working 'core' object
            if (Core) DestroyImmediate(Core.gameObject);
        }

        /// <summary> Initializes a temporary <see cref="core"/> instance when the window is opened. </summary>
        /// <param name="visible"> Hide in hierarchy? </param>
        /// <remarks> Allows for th <see cref="core"/> component to be used in a similar way as during runtime. </remarks>
        static core AddTemporaryCore(bool visible = false)
        {
            const string coreEditorName = "Core (Editor)";

            var coreObject = new GameObject(coreEditorName, typeof(core))
            {
                    tag = "EditorOnly",
                    hideFlags = !visible ? HideFlags.HideAndDontSave : HideFlags.DontSave
            };
            var core = coreObject.GetComponent<core>();
            core.Start();
            core.onReady(() =>
            {
                core.app_id = NgSettingsManager.Get<string>("api.appId");
                core.session_id = core.getSessionLoader().session_id;
                core.aes_base64_key = NgSettingsManager.Get<string>("api.aesBase64Key");

                if (InternetReachability) core.checkLogin(); // this 'refreshes' the user if they're already logged in
            });
            return core;
        }

        void OnGUI()
        {
            string[] tabsContent = SetupTabs.Select(t => t.GetName()).ToArray();
            using (new EditorGUI.DisabledGroupScope(true))
            {
                GUILayout.Toolbar(currentTabIndex, tabsContent);
                GUILayout.Space(10);
            }
            SetupTabs[currentTabIndex].Draw();
            DrawPreviousNextButton();
        }

        void DrawPreviousNextButton()
        {
            GUILayout.FlexibleSpace();
            using (new GUILayout.HorizontalScope())
            {
                int oldTabIndex = currentTabIndex;
                GUILayoutOption[] buttonOptions = { GUILayout.MaxWidth(100) };
                GUILayout.FlexibleSpace();
                if (currentTabIndex != 0 && GUILayout.Button("Back", buttonOptions)) currentTabIndex--;
                else if (currentTabIndex != TabCount - 1 && GUILayout.Button("Next", buttonOptions)) currentTabIndex++;
                else if (currentTabIndex == TabCount - 1 && GUILayout.Button("Finish", buttonOptions))
                {
                    currentTabIndex = 0;
                    Close();
                }

                if (oldTabIndex != currentTabIndex)
                {
                    SetupTabs[oldTabIndex].Close();
                    SetupTabs[currentTabIndex].Init();
                }
            }
        }
    }
}
