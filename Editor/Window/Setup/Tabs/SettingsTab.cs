﻿using UnityEditor;
using UnityEngine;
using Unity.EditorCoroutines.Editor;

using io.newgrounds.unity.editor.utility;
using io.newgrounds.unity.editor.settings;

namespace io.newgrounds.unity.editor.setup
{
    partial class NewgroundsSetup
    {
        class SettingsTab : SetupTab
        {
            EditorCoroutine iconCoroutine;
            EditorCoroutine loginStatusCoroutine;
            Texture2D userIcon;

            public override void Draw()
            {
                DrawLoginStatus();
                DrawSettings();
            }

            static void DrawSettings()
            {
                EditorMethods.HeaderField("Settings");
                using (new EditorGUILayout.HorizontalScope())
                {
                    string addedInfo = string.Empty;
                    var messageType = MessageType.Info;
                    if (NewgroundsSettings.IsApiEmpty())
                    {
                        messageType = MessageType.Error;
                        addedInfo = "They're currently empty.";
                    }
                    else if (NewgroundsSettings.IsApiDefault())
                    {
                        messageType = MessageType.Warning;
                        addedInfo = "You're using the default values provided for demonstration purposes. They won't work with your own project.";
                    }

                    EditorGUILayout.HelpBox("This is the current state of your settings (read-only). " + addedInfo +
                                            "\nMake sure the app ID and encryption key are the ones found in the API Tools tab in your Project System.", messageType);
                    if (GUILayout.Button(new GUIContent("Open Settings", "Opens the Newgrounds Project Settings section."),
                                         GUILayout.ExpandHeight(true)))
                    {
                        settings.NewgroundsSettings.OpenSettings();
                    }
                }
                using (new EditorGUI.DisabledScope(true))
                {
                    settings.NewgroundsSettings.ApiToolsDrawers.DrawAppID(NgSettingsManager.GetAppId(), string.Empty, false);
                    settings.NewgroundsSettings.ApiToolsDrawers.DrawEncryptionKey(NgSettingsManager.GetEncryptonKey(), string.Empty, false);
                }
            }

            /// <summary> Draws login status with login info. </summary>
            void DrawLoginStatus()
            {
                EditorMethods.HeaderField("Login status");
                using (var h = new EditorGUILayout.HorizontalScope())
                {
                    const int iconSize = 36;
                    GUILayout.Box(userIcon ? userIcon : GUI.skin.box.normal.background,
                                  GUILayout.Height(iconSize), GUILayout.Width(iconSize));
                    GUILayout.Label(User != null ? User.name : "Unknown", new GUIStyle(GUI.skin.label)
                    {
                            alignment = TextAnchor.MiddleLeft,
                            fontSize = 18
                    }, GUILayout.Height(iconSize), GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                }
                GUILayout.Space(10);
                DrawLoginInfo();
            }

            /// <summary> Draws a <see cref="EditorGUILayout.HelpBox(string, MessageType)"/> based on the available information. </summary>
            void DrawLoginInfo()
            {
                if (User == null)
                {
                    using (new EditorGUILayout.HorizontalScope(GUILayout.ExpandHeight(true)))
                    {
                        string loggedMessage = "You are not logged in";
                        loggedMessage += InternetReachability ? "." : " and not connected to the Internet.";
                        EditorGUILayout.HelpBox($"{loggedMessage}\nTo make the setup process as easy as possible, " +
                                                "it's recommended that you log in.", MessageType.Warning);

                        if (GUILayout.Button("Log in", GUILayout.ExpandHeight(true)))
                        {
                            LogIn();
                        }
                    }
                }
                else
                {
                    if (userIcon == null)
                    {
                        iconCoroutine = SetupWindow.StartCoroutine
                                (DownloadIcon(User.icons.medium, icon => userIcon = icon));
                    }

                    using (new EditorGUILayout.HorizontalScope(GUILayout.ExpandHeight(true)))
                    {
                        EditorGUILayout.HelpBox("You are properly logged in.\nIf you intend to have medals or scoreboards in your game, it's easier to add them now.", MessageType.Info);

                        if (GUILayout.Button("Log out", GUILayout.ExpandHeight(true)))
                        {
                            LogOut();
                        }
                    }
                }
            }

            void LogIn()
            {
                if (!InternetReachability)
                {
                    Debug.LogWarning("You can't log in when you are not connected to the Internet.");
                    return;
                }

                Core.checkLogin(loggedIn =>
                {
                    if (!loggedIn)
                    {
                        Core.onPassportLoaded += OnPassportLoaded;
                        Core.requestLogin(() =>
                        {
                            // Load the icon when the user logs in
                            iconCoroutine = SetupWindow.StartCoroutine(SetupWindow.DownloadIconEditor(User.icons.medium, icon => userIcon = icon));
                            Core.onPassportLoaded -= OnPassportLoaded;
                        });

                        void OnPassportLoaded()
                        {
                            // Start checking for login when the passport window is opened
                            loginStatusCoroutine = SetupWindow.StartCoroutine(new WaitForLogin(Core, 30f));
                        }
                    }
                });
            }

            void LogOut()
            {
                userIcon = null;
                Core.logOut();
            }

            /// <summary> Called when the tab is closed. The default behaviour is to do nothing. </summary>
            public override void Close()
            {
                // They may become ownerless, don't use this editor window to stop them
                if (iconCoroutine != null) EditorCoroutineUtility.StopCoroutine(iconCoroutine);
                if (loginStatusCoroutine != null) EditorCoroutineUtility.StopCoroutine(loginStatusCoroutine);
            }
        }
    }
}
