﻿using System.Collections.Generic;
using System.Linq;
using io.newgrounds.objects;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.setup
{
    partial class NewgroundsSetup
    {
        class MedalsTab : AssetTab<MedalData, medal>
        {
            Dictionary<int, Texture2D> medalIcons = new Dictionary<int, Texture2D>();

            /// <summary> Returns the message to display in an infobox at the top of the tab. </summary>
            protected override string GetInfoBoxMessage()
            {
                return string.Empty;
            }

            /// <summary> Attempts to asynchronously (re)load the list of all medals. </summary>
            protected override void UpdateNgObjectsAsync()
            {
                var medalList = new components.Medal.getList();
                medalList.callWith(Core, result =>
                {
                    foreach (medal medal in result.medals)
                    {
                        ngObjects.Add(medal);
                        SetupWindow.StartCoroutine
                                (DownloadIcon(medal.icon, icon =>
                                {
                                    if (!medalIcons.ContainsKey(medal.id)) medalIcons.Add(medal.id, icon);
                                }));
                    }
                    loaded = true;
                });
            }
            
            protected override MedalData UpdateNgAsset(MedalData ngData, medal medal)
            {
                ngData.SetMedalData(ngData.Icon, medal.name, (MedalData.Difficulty) medal.difficulty);
                if (!ngData.name.Contains(medal.name)) // allow "Medal (1)"
                {
                    AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(ngData), medal.name);
                }
                return ngData;
            }

            protected override MedalData CreateNgAsset(string relativePath, medal medal)
            {
                string assetPath = $"{relativePath}/{medal.name}.asset";
                string uniqueAssetPath = AssetDatabase.GenerateUniqueAssetPath(assetPath); // "scoreboard (1)" if taken

                var newAsset = CreateInstance<MedalData>();
                newAsset.SetRawMedalData(medal);
                AssetDatabase.CreateAsset(newAsset, uniqueAssetPath);
                
                return newAsset;
            }

            /// <summary> Returns the label of a displayed Newgrounds asset. </summary>
            protected override GUIContent GetAssetLabel(medal medal)
            {
                return new GUIContent($"　{medal.name}", medalIcons?[medal.id], medal.description);
            }

            /// <summary> Returns the matching <see cref="MedalData"/> asset from the project. </summary>
            /// <param name="medal"> Actual medal to look for, using the ID. </param>
            /// <param name="medalData"> The returned data asset, if found. </param>
            protected override bool TryGetMatchingAsset(medal medal, out MedalData medalData)
            {
                medalData = ngAssets.FirstOrDefault(asset => asset.Id == medal.id);
                return medalData != null;
            }
        }
    }
}
