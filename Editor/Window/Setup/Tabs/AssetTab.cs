﻿using System.Collections.Generic;
using System.Linq;
using io.newgrounds.unity.editor.settings;
using UnityEditor;
using UnityEngine;
using MessageType = UnityEditor.MessageType;

namespace io.newgrounds.unity.editor.setup
{
    partial class NewgroundsSetup
    {
        abstract class AssetTab<TData, TNg> : SetupTab
                where TData : NgData
                where TNg : Model
        {
            /// <summary> Newgrounds objects loaded via a negrounds.io call. </summary>
            protected List<TNg> ngObjects = new List<TNg>();
            /// <summary> Data assets referencing <see cref="ngObjects"/> found in the project. </summary>
            protected List<TData> ngAssets = new List<TData>();

            /// <summary> Have the assets been loaded already? When <see langword="null"/>, their loading didn't start yet. </summary>
            protected bool? loaded = null;
            /// <summary> Scroll position inside the tab's scroll view. </summary>
            Vector2 scrollPosition;
            /// <summary> The path to the folder used to save Newgrounds assets. </summary>
            protected const string NgAssetLocationKey = "editor.ngAssetLocation";

            public override void Init()
            {
                if (!loaded.HasValue) PreloadNgObjectsAsync();
                ngAssets = EditorFileUtils.GetNgDataAssets<TData>().ToList();
            }

            public void PreloadNgObjectsAsync()
            {
                if (!InternetReachability) return;
                if (ngObjects == null) ngObjects = new List<TNg>();
                else ngObjects.Clear();
                loaded = false;
                UpdateNgObjectsAsync();
            }

            /// <summary> Asynchronously updates the list of <see cref="ngObjects"/>, setting <see cref="loaded"/> in the process.</summary>
            protected abstract void UpdateNgObjectsAsync();

            /// <summary> Draw a tab of the setup process. </summary>
            public override void Draw()
            {
                DrawInfoBox();
                DrawAssetSavePath();
                using (new EditorGUILayout.HorizontalScope())
                {
                    GUILayout.FlexibleSpace();
                    bool create = ShouldCreateOrUpdateAsset();
                    string verb = loaded.HasValue ? (create ? "Create" : "Update") : "Loading";
                    var assetButton = new GUIContent($"{verb} Assets{(loaded.HasValue ? string.Empty : "…")}",
                                                     $"Creates new data assets at the specified location, and updates existing ones (except icons).");
                    using (new EditorGUI.DisabledScope(!(loaded is true)))
                    {
                        if (GUILayout.Button(assetButton)) CreateOrUpdateNgAssets();
                    }
                }
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                DrawNgObjects();
            }

            /// <summary> Returns the message to display in an infobox at the top of the tab. </summary>
            protected virtual string GetInfoBoxMessage() => string.Empty;

            /// <summary> Draws a message from <see cref="GetInfoBoxMessage"/> to display in an infobox at the top of the tab, unless the message is empty. </summary>
            void DrawInfoBox()
            {
                string message = GetInfoBoxMessage();
                if (string.IsNullOrWhiteSpace(message)) return;
                
                EditorGUILayout.HelpBox(message, MessageType.Info);
            }

            /// <summary> Counts the number of objects that already have an existing matching data asset in the project. </summary>
            int CountMatchingAssets()
            {
                return ngObjects.Count(ngObject => TryGetMatchingAsset(ngObject, out _));
            }
            /// <summary> Determines if there are any <see cref="ngObjects"/> that don't have matching assets in the project. </summary>
            /// <returns> True if there are any assets to create, otherwise it's only necessary to update the existing ones. </returns>
            bool ShouldCreateOrUpdateAsset() => !loaded.HasValue || CountMatchingAssets() < ngObjects.Count;

            /// <summary> Attempts to finds the data asset which matches with the supplied object. </summary>
            protected abstract bool TryGetMatchingAsset(TNg ngObject, out TData ngData);

            /// <summary> Displays an editable folder path field for setting the location where new assets will be stored. </summary>
            static void DrawAssetSavePath()
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    string savePath = NgSettingsManager.Get<string>(NgAssetLocationKey);
                    savePath = SearchableLayoutExtras.FolderPathField(savePath, new GUIContent("Save To", "The folder to generate new medal assets in."));
                    if (check.changed) NgSettingsManager.Set(NgAssetLocationKey, savePath);
                }
            }

            void CreateOrUpdateNgAssets()
            {
                var newAssets = new List<TData>();
                string folderPath = NgSettingsManager.Get<string>(NgAssetLocationKey);
                
                foreach (TNg ngObject in ngObjects)
                {
                    if (TryGetMatchingAsset(ngObject, out TData asset))
                    {
                        //! Update
                        UpdateNgAsset(asset, ngObject);
                        continue;
                    }

                    //! Create
                    string relativePath = EditorFileUtils.GetPathFromAbsolute(folderPath);
                    if (!AssetDatabase.IsValidFolder(relativePath))
                    {
                        Debug.LogWarning($"Cannot create a Newgrounds data data asset because the folder path is not valid:\n<i>{folderPath}</i>");
                        continue;
                    }

                    TData newAsset = CreateNgAsset(relativePath, ngObject);
                    if (newAsset) newAssets.Add(newAsset);
                }

                AssetDatabase.SaveAssets();
                if (newAssets.Any())
                {
                    ngAssets.AddRange(newAssets);
                    EditorUtility.FocusProjectWindow();
                    Selection.objects = newAssets.ToArray<Object>();
                    Selection.activeObject = newAssets.FirstOrDefault();
                    Debug.Log($"Created {newAssets.Count} new data asset{(newAssets.Count > 1 ? "s" : "")} in the project.");
                    return;
                }
                Debug.Log("Updated data assets in the project.");
            }

            protected abstract TData UpdateNgAsset(TData ngData, TNg ngObject); // TODO : Fix renaming
            protected abstract TData CreateNgAsset(string relativePath, TNg ngObject);

            /// <summary> Displays a scrollable vertical list of Newgrounds objects and their matching data assets. </summary>
            void DrawNgObjects()
            {
                if (!InternetReachability)
                {
                    EditorGUILayout.HelpBox("You don't seem to be connected to the internet.", MessageType.Warning);
                }
                if (!loaded.HasValue) return;

                if (loaded.Value)
                {
                    using (new EditorGUILayout.VerticalScope())
                    {
                        using (var scrollView = new EditorGUILayout.ScrollViewScope(scrollPosition))
                        {
                            scrollPosition = scrollView.scrollPosition;
                            foreach (TNg ngObject in ngObjects) DrawNgObject(ngObject);
                        }
                    }
                }
                else if (InternetReachability)
                {
                    EditorGUILayout.HelpBox("The list is loading…", MessageType.Info);
                }
            }

            /// <summary> Returns the label of a displayed Newgrounds asset. </summary>
            protected abstract GUIContent GetAssetLabel(TNg ngObject);

            /// <summary> Display a single Newgrounds object in the editor window. </summary>
            void DrawNgObject(TNg ngObject)
            {
                GUIContent ngLabel = GetAssetLabel(ngObject);
                EditorGUILayout.LabelField(ngLabel);

                TryGetMatchingAsset(ngObject, out TData matchingAsset);
                EditorGUILayout.ObjectField(GUIContent.none, matchingAsset, typeof(MedalData), false);
                EditorGUILayout.Space();
            }
        }
    }
}
