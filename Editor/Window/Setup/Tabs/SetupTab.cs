﻿using System;
using System.Collections;
using UnityEngine;

namespace io.newgrounds.unity.editor.setup
{
    partial class NewgroundsSetup
    {
        /// <summary> A tab in the Newgrounds setup process. </summary>
        private protected abstract class SetupTab
        {
            const string TypeSuffix = "Tab";

            public virtual string GetName()
            {
                string typeName = GetType().Name;
                return !typeName.EndsWith(TypeSuffix)
                        ? typeName
                        : typeName.Substring(0, typeName.Length - TypeSuffix.Length);
            }

            /// <summary> Draw a tab of the setup process. </summary>
            public abstract void Draw();

            /// <summary> Called when the tab is opened. The default behaviour is to do nothing. </summary>
            public virtual void Init() { }

            /// <summary> Called when the tab is closed. The default behaviour is to do nothing. </summary>
            public virtual void Close() { }

            /// <summary> Downloads an icon from an URL, using the <see cref="SetupWindow"/>. </summary>
            /// <param name="iconUrl"> Icon to load. </param>
            /// <param name="iconLoaded"> A mandatory callback to use when the icon is loaded. </param>
            protected static IEnumerator DownloadIcon(string iconUrl, Action<Texture2D> iconLoaded)
            {
                return SetupWindow.DownloadIconEditor(iconUrl, iconLoaded);
            }
        }
    }
}
