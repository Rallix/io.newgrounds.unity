﻿using System.Linq;
using io.newgrounds.objects;
using io.newgrounds.unity.editor.icons;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.setup
{
    partial class NewgroundsSetup
    {
        class ScoreboardTab : AssetTab<ScoreboardData, scoreboard>
        {
            /// <summary> Returns the message to display in an infobox at the top of the tab. </summary>
            protected override string GetInfoBoxMessage()
            {
                return string.Empty;
            }
            
            /// <summary> Asynchronously updates the list of <see cref="AssetTab{TData,TNg}.ngObjects"/>, setting <see cref="AssetTab{TData,TNg}.loaded"/> in the process.</summary>
            protected override void UpdateNgObjectsAsync()
            {
                var scoreboardList = new components.ScoreBoard.getBoards();
                scoreboardList.callWith(Core, result =>
                {
                    foreach (scoreboard scoreboard in result.scoreboards) ngObjects.Add(scoreboard);
                    loaded = true;
                });
            }

            /// <summary> Attempts to finds the data asset which matches with the supplied object. </summary>
            protected override bool TryGetMatchingAsset(scoreboard ngObject, out ScoreboardData ngData)
            {
                ngData = ngAssets.FirstOrDefault(asset => asset.Id == ngObject.id);
                return ngData != null;
            }

            protected override ScoreboardData UpdateNgAsset(ScoreboardData scoreboardData, scoreboard scoreboard)
            {
                scoreboardData.name = scoreboard.name;
                if (!scoreboardData.name.Contains(scoreboard.name)) // allow "Scoreboard (1)"
                {
                    AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(scoreboardData), scoreboard.name);
                }
                return scoreboardData;
            }

            protected override ScoreboardData CreateNgAsset(string relativePath, scoreboard scoreboard)
            {
                string assetPath = $"{relativePath}/{scoreboard.name}.asset";
                string uniqueAssetPath = AssetDatabase.GenerateUniqueAssetPath(assetPath); // "scoreboard (1)" if taken

                var newAsset = CreateInstance<ScoreboardData>();
                newAsset.SetRawScoreboardData(scoreboard);
                AssetDatabase.CreateAsset(newAsset, uniqueAssetPath);
                
                return newAsset;
            }


            /// <summary> Returns the label of a displayed Newgrounds asset. </summary>
            protected override GUIContent GetAssetLabel(scoreboard scoreboard)
            {
                return new GUIContent($"　{scoreboard.name}", 
                                      NewgroundsIcons.GetIcon(NewgroundsIcons.ComponentIcons.Scoreboard));
            }
        }
    }
}
