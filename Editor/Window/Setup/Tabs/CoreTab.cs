﻿using io.newgrounds.unity.editor.icons;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor.setup {
    partial class NewgroundsSetup {
        class CoreTab : SetupTab
        {

            //! An entire section consists of just one button
            //? Move to Settings tab

            public override void Draw()
            {
                var infoContent = new GUIContent("　Core component is the main component maintaining the connection to Newgrounds.\n" +
                                             "　It needs to be active in every scene in which you intend to access/update other Newgrounds components.",
                                             NewgroundsIcons.GetIcon(NewgroundsIcons.ComponentIcons.Core));

                EditorGUILayout.HelpBox(infoContent);
                EditorGUILayout.HelpBox("When the \"Don't destroy on load\" option is enabled, it will be passed between scenes, " +
                                        "so you only need to add it to the first one.", MessageType.Info);

                //! Center horizontally and vertically in the remaining space
                using (new GUILayout.VerticalScope())
                {
                    GUILayout.FlexibleSpace();
                    using (new GUILayout.HorizontalScope())
                    {
                        GUILayout.FlexibleSpace();
                        CoreInitializer.AddCoreButton(96);
                        GUILayout.FlexibleSpace();
                    }
                    GUILayout.FlexibleSpace();
                }
            }
        }
    }
}