﻿using System;
using UnityEngine;
using UnityEditor;

namespace io.newgrounds.unity.editor
{

    // TODO: Aprime

    /// <summary> A class handling setting WebGL Templates. </summary>
    class NewgroundsWebGLTemplates
    {
        /// <summary> The path to the templates folder required by Unity. </summary>
        const string TemplatesPath = "Assets/WebGLTemplates";
        /// <summary> The name of the Newgrounds WebGL template. </summary>
        const string NewgroundsTemplateName = "Newgrounds";

        /// <summary> The prefix used for the built-in templates in <see cref="PlayerSettings"/>. </summary>
        const string BuiltInTemplatePrefix = "APPLICATION";
        /// <summary> The prefix used for the custom templates in <see cref="PlayerSettings"/>. </summary>
        const string CustomTemplatePrefix = "PROJECT";

        internal enum BuiltInTemplates
        {
            Default,
            Minimal
        }

        /// <summary> Sets the WebGL Template. </summary>
        /// <param name="template">The template name in WebGLTemplates folder.</param>
        internal static void SetTemplate(string template)
        {
            if (!TemplateExists(template))
            {
                throw new ArgumentException($"The template <b>{template}</b> you're trying to set doesn't exist.\n" +
                                            $"Make sure there's a folder <i>{TemplatesPath}/{template}<i> with <b>index.html</b> in it.", nameof(template));
            }
            PlayerSettings.WebGL.template = $"{CustomTemplatePrefix}:{template}";
        }

        /// <summary> Sets the WebGL Template to Newgrounds. </summary>
        internal static void SetNewgroundsTemplate()
        {
            SetTemplate(NewgroundsTemplateName);
        }

        internal static void SetBuiltInTemplate(BuiltInTemplates template)
        {
            PlayerSettings.WebGL.template = $"{BuiltInTemplatePrefix}:{template}";
        }

        internal static bool IsNewgroundsTemplate()
        {
            return PlayerSettings.WebGL.template == $"{CustomTemplatePrefix}:{NewgroundsTemplateName}";
        }

        /// <summary> Checks whether a custom WebGLTemplate exists within a project. </summary>
        /// <param name="template">The name of the template</param>
        static bool TemplateExists(string template)
        {
            if (!AssetDatabase.IsValidFolder($"{TemplatesPath}/{template}")) return false;
            string[] templateRoot = AssetDatabase.FindAssets("index", new[] {$"{TemplatesPath}/{template}"});
            return templateRoot.Length > 0 && 
                   AssetDatabase.GUIDToAssetPath(templateRoot[0]).EndsWith(".html", StringComparison.OrdinalIgnoreCase);
        }
    }
}
