﻿using UnityEngine;
using UnityEditor;

namespace io.newgrounds.unity.editor
{
    /// <summary> Makes sure copied gizmos have the same settings as the original ones. </summary>
    class GizmosPreprocessor : AssetPostprocessor
    {
        void OnPreprocessTexture()
        {
            if (assetPath.Contains(GizmosFolderCopy.GizmosPath))
            {
                var textureImporter = (TextureImporter) assetImporter;
                textureImporter.textureType = TextureImporterType.Default;
                textureImporter.alphaSource = TextureImporterAlphaSource.FromInput;
                textureImporter.alphaIsTransparency = true;
                
                textureImporter.npotScale = TextureImporterNPOTScale.ToNearest;
                textureImporter.isReadable = true;
            }
            
        }
    } 
}