﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.unity.editor
{
    static class NgPath
    {
        /// <summary> The default namespace of the project based on the <see cref="CoreComponent"/>, most likely <see langword="io.newgrounds.unity"/>. Also the package name.</summary>
        /// <remarks> The root namespace is also important for package management and should match the package name as specified in package.json. </remarks>
        internal static readonly string RootNamespace = $"{typeof(CoreComponent).Namespace}";
        /// <summary> The path to the package root relative to the <see cref="ProjectRoot"/>. </summary>
        internal static readonly string PackageRoot = $"Packages/{RootNamespace}";
        /// <summary> The root folder of the Unity project. </summary>
        internal static readonly string ProjectRoot = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf('/'));

        /// <summary> Joins paths with a forward slash separator. </summary>
        /// <remarks> Unity sometimes refuses to accept <see cref="Path.Combine(string[])"/>.</remarks>
        internal static string Combine(params string[] paths)
        {
            return string.Join("/", paths);
        }

        /// <summary> Creates a path to a file inside the Newgrounds.io package folder. </summary>
        /// <param name="paths">Parts of a single path to the file from the root of the Newgrounds.io package folder.</param>
        /// <remarks>
        ///     A simple <see cref="string.Join(string, string[])"/> with the package's root path.
        /// </remarks>
        internal static string CombinePackageRoot(params string[] paths)
        {
            string[] allPaths = new string[paths.Length + 1];
            allPaths[0] = PackageRoot;
            Array.Copy(paths, 0, allPaths, 1, paths.Length);
            return NgPath.Combine(allPaths); // Path.Combine(allPaths) doesn't work very well in Unity
        }

        /// <summary> Returns the first asset object type at the given path. </summary>
        /// <typeparam name="T">Data type of the asset.</typeparam>
        /// <param name="assetPath">Path of the asset to load.</param>
        /// <returns></returns>
        internal static T LoadAssetAtPackagePath<T>(string assetPath) where T : UnityEngine.Object
        {
            return AssetDatabase.LoadAssetAtPath<T>(CombinePackageRoot(assetPath));
        }

        /// <summary> Transforms a path within the Assets folder to the same path within Packages. </summary>
        /// <param name="assetPath"> A relative or absolute path inside Assets folder. </param>
        /// <remarks> Expects the Unity assets folder to be the only folder in the path named "Assets". </remarks>
        /// <exception cref="ArgumentException"> Thrown when the structure of the given path can't be resolved. </exception>
        /// <example>
        ///     If the Asset path was "…/Project/Assets/Gizmos/Icon.png", 
        ///     then the package path will be "…/Project/Packages/Gizmos/Icon.png".
        /// </example>
        internal static string AssetToPackagePath(string assetPath)
        {
            return PathToPathPath(assetPath, "Assets", PackageRoot);
        }

        /// <summary> Transforms a path within the Packages folder to the same path within Assets. </summary>
        /// <param name="packagePath"> A relative or absolute path inside Packages folder. </param>
        /// <remarks> Expects the Unity package folder to be the only folder in the path named "Packages". </remarks>
        /// <exception cref="ArgumentException"> Thrown when the structure of the given path can't be resolved. </exception>
        /// <example>
        ///     If the Asset path was "…/Project/Assets/Gizmos/Icon.png", 
        ///     then the package path will be "…/Project/Packages/Gizmos/Icon.png".
        /// </example>
        internal static string PackageToAssetPath(string packagePath)
        {
            return PathToPathPath(packagePath, PackageRoot, "Assets");
        }

        /// <summary> Converts an absolute project path to a relative one (in Assets folder). </summary>
        /// <param name="absolutePath"> An absolute path leading to a location inside the Assets folder. </param>
        internal static string MakeRelative(string absolutePath)
        {
            if (string.IsNullOrEmpty(absolutePath)) return string.Empty;
            if (!absolutePath.Contains(Application.dataPath))
            {
                NgDebug.LogException(new ArgumentException("Relative path only works for paths already inside the Assets folder.", nameof(absolutePath)));
            }
            return absolutePath.Replace(Application.dataPath, string.Empty);
        }

        /// <summary> Replaces a folder name with a different folder name in a path. </summary>
        static string PathToPathPath(string path, string folderName1, string folderName2)
        {
            string assetsFolderName = $"{folderName1}/";

            int index = path.IndexOf(assetsFolderName, StringComparison.InvariantCultureIgnoreCase);
            if (index == -1) index = path.IndexOf($"{folderName1}\\", StringComparison.InvariantCultureIgnoreCase);
            if (index == -1)
            {
                NgDebug.LogException(new ArgumentException($"The path doesn't seem to be within the <b>{folderName1}</b> folder:\n {path}", nameof(path)));
                return string.Empty;
            }
            else if (index != path.LastIndexOf(assetsFolderName, StringComparison.InvariantCultureIgnoreCase) &&
                     index != path.LastIndexOf($"{folderName1}\\", StringComparison.InvariantCultureIgnoreCase))
            {
                NgDebug.LogException(new ArgumentException($"There are several folders in the path named <b>{folderName1}</b>:\n {path}", nameof(path)));
                return string.Empty;
            }

            string pathEnding = path.Substring(index + assetsFolderName.Length); // without "…/Assets/"
            return index == 0
                    ? Path.Combine(folderName2, pathEnding)               // relative path
                    : Path.Combine(ProjectRoot, folderName2, pathEnding); // absolute path
        }
    }
}
