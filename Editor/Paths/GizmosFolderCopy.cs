﻿using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;

namespace io.newgrounds.unity.editor
{
    /// <summary> A class for moving Gizmos between the Packages and Assets folder. </summary>
    /// <remarks>
    ///     Since packages currently can't have their own Gizmos folder, this copies it to the Assets folder.
    /// </remarks>
    static class GizmosFolderCopy
    {
        /// <summary> A path to a folder expected to contain ALL the gizmos, either directly or in subfolders. </summary>
        internal static readonly string GizmosPath = $"Gizmos/{NgPath.RootNamespace.Replace('.', '/')}";

        /// <summary> Copies the contents of the package's <see cref="GizmosPath"/> folder to the local Gizmos folder. </summary>
        /// <remarks>
        ///     Does not copy meta files, so GUIDs will be different.
        /// </remarks>
        [InitializeOnLoadMethod]
        static void CopyAllGizmos()
        {
            string sourceFolder = NgPath.CombinePackageRoot("Gizmos"); //! Packages/io.newgrounds.unity/Gizmos
            if (!Directory.Exists(sourceFolder)) return;

            int totalCopied = Directory.EnumerateFiles(sourceFolder, "*", SearchOption.AllDirectories)
                                       .Where(file => !file.EndsWith(".meta"))
                                       .Count(CopyGizmo);
            if (totalCopied > 0)
            {
                NgDebug.Log(totalCopied == 1
                                    ? "Created a gizmo for Newgrounds components."
                                    : $"Created {totalCopied} gizmos for Newgrounds components.");
            }
        }

        /// <summary> Copies a gizmo at the given path to the local Gizmos folder. </summary>
        /// <param name="gizmoIconPath"> An absolute path to the file in the package's Gizmos folder. </param>
        /// <returns> True if the file has been copied. </returns>
        static bool CopyGizmo([NotNull] string gizmoIconPath)
        {
            string gizmoIconName = Path.GetFileName(gizmoIconPath);
            string sourceFile = gizmoIconPath;
            if (File.Exists(sourceFile))
            {
                string fileGizmosPath = NgPath.PackageToAssetPath(gizmoIconPath);
                string gizmoDirectory = fileGizmosPath.Substring(0, fileGizmosPath.LastIndexOfAny(new[] {'/', '\\'}));

                string destinationFolder = Path.Combine(NgPath.ProjectRoot, gizmoDirectory);
                if (!Directory.Exists(destinationFolder)) Directory.CreateDirectory(destinationFolder);

                string destinationFile = Path.Combine(destinationFolder, gizmoIconName);
                if (!File.Exists(destinationFile)
                 // || (File.GetLastWriteTime(destinationFile) < File.GetLastWriteTime(sourceFile)
                 || (new FileInfo(destinationFile).Length != new FileInfo(sourceFile).Length
                  && (File.GetAttributes(destinationFile) & FileAttributes.ReadOnly) == 0))
                {
                    if (!Directory.Exists(Path.GetDirectoryName(destinationFile)))
                    {
                        string dest = Path.GetDirectoryName(destinationFile);
                        if (!string.IsNullOrWhiteSpace(dest)) Directory.CreateDirectory(dest);
                    }
                    File.Copy(sourceFile, destinationFile, true);
                    File.SetAttributes(destinationFile, File.GetAttributes(destinationFile) & ~FileAttributes.ReadOnly);
                    return true;
                }
            }
            return false;
        }
    }
}
