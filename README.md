# Newgrounds.io for Unity

###### Package

This is the repository for the package.
Please note this is just an extension to the original [Newgrounds.io](http://www.newgrounds.io) library.

## Installation 

Open the Package Manager in Unity and then return to this repository and choose one of the following:

* Download a *zip*, extract it and add it (Add package from disk...)
* Add the package via the *git* link (Add package from git URL...)

The first option should always work and even allows you to point multiple projects to the same package folder.
The second option requires you to have [git](https://git-scm.com/) installed, after which the installation and updating the package is greatly simplified.

## Documentation

See the [documentation](https://rallix.gitlab.io/io.newgrounds.unity) for further details.
