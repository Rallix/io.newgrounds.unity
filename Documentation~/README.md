### Newgrounds.io for Unity documentation

This is simple the subtree for storing the [documentation](https://rallix.gitlab.io/io.newgrounds.unity/).
It's all automatically deployed to GitLab Pages.
