## Samples

The package contains several demo scenes showcasing its different components.

#### Overview

##### Lightweight

If you prefer that, this shows how to use the original library alone without additional scripts from the extension.

##### User Data

How to gather information about the user from the user session and their profile page.

##### Medal (simple)

Shows the main way of unlocking medals and an example of queuing.

##### Medal (advanced)

Contains a script with an example of a defining a medal condition for being unlocked at a later time.

##### Scoreboard

A basic example of a button which posts score when clicked.