---
uid: getting_started
---

## Getting Started

The simplest way to initialize the package to be ready to go is to use the Setup Wizard at **Window/Newgrounds/Open Setup Wizard**.  


<a name="topic"/>

It will start a several step process during which you'll login, link the game, medals and scoreboards and add a *Core* manager prefab to your first scene. You can skip any step and return to it later, or edit specific settings directly; although it's recommended to do as much as you can during the inital setup.
