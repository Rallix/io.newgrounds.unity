---
uid: index
---

## Newgrounds.io

An unofficial extension of the [Newgrounds.io for Unity](https://bitbucket.org/newgrounds/newgrounds.io-for-unity-c/) library more tightly integrated into Unity and hopefully more user-friendly.
This adds GUI, editor tools and automated setup tools on top of the original library; making this package easier to setup and with some new functionality as well.  

Most of the editor scripts, gizmos and editor resources are stripped from the final build by design in Unity, so you shouldn't see noticeable spikes in build sizes, nor in speed (since the original library is used underneath to handle the core functionality anyway).  

Installing the extension as a package also means it's easier to keep it up-to-date as new versions are released and won't visibly clutter your project. 

<a name="Installation"/>

### Installation

#### Pre-requisites
This library is made with Unity `2019.4` LTS version, which is a minimal requiremnent.
The new version of API Tools also must be enabled in your **[Project System](https://www.newgrounds.com/projects/games)**.

> Project System > *[Your Game]* > Api Tools > I agree, show me the tools! > Click here to use the Newgrounds.io API for this game!


![Enable API Tools in Project System](images/Setup/Enable_API_1.jpg)  

![Enable API Tools in Project System](images/Setup/Enable_API_2.jpg)

Note down the `App ID` and the `AES-128 Base64 encryption key`, as you'll be needing it later.

![App ID and encryption key in API Tools](images/Setup/App_and_Encryption.jpg)
 
#### Package Manager
Provided you have [git](https://git.com) installed, you add the package via `Add package from git URL…` from the *Package Manager* window, 
or directly to your project's **Packages/**`manifest.json` file like this:

```json
{
   "dependencies": {
      "io.newgrounds.unity": "https://ngio-read:zsqnheWKRExWzG5Two11@gitlab.com/Rallix/io.newgrounds.unity.git"
   }
}
```

By design, Unity locks the latest version of a custom package when you first add it. Remove the lock every time you wish to update the package, or have the **Window/Newgrounds/Check for Updates** option do it for you.

```json
{
   "lock": {
      "io.newgrounds.unity": {
         "revision": "HEAD",
         "hash": "ngdqib2lj9ahxw8ervsl6wna0262fklxcasscwlw"
      }
   }
}
```

If you don't have ```git```, you can download the package manually and use the *Add package from disk…* option. 
Don't forget to edit your ```manifest.json``` file with the current version number.

```json
{
   "dependencies": {
      "io.newgrounds.unity": "0.7.2"
   }
}
```
<a name="QuickStart"/>

### Quick Start

After you install the package, the simplest way to initialize it to be ready to go is to use the Setup Wizard at: 
**Window/Newgrounds/Open Setup Wizard**. 

It will start a several step process during which you'll login, link the game, medals and scoreboards and add a *Core* manager prefab to your first scene. You can skip any step and return to it later, or edit specific settings directly; although it's recommended to do as much as you can during the inital setup, in bulk.

For more detailed information, visit the [Quick Start](Setup/GettingStarted.md) section, or the [Samples](Samples/Samples.md) overview page.

<a name="Troubleshooting"/>

### Troubleshooting

#### Possible Problems

The package uses a couple of precompiled assemblies, and you will receive an error upon installation if they were already in your project (in case something else is using them).
```
PrecompiledAssemblyException: Multiple precompiled assemblies with the same name NAME.dll included for the current platform. Only one assembly with the same name is allowed per platform.
```
If you get this error, delete one of the identical assemblies.

#### Leave Feedback

Feel free to send me a [message](https://www.newgrounds.com/pm/send/rallyx) on Newgrounds in case you need help, have suggestions or encountered a bug.

<a name="Technical"/>

### Technical details

#### Requirements

This version of *Newgrounds.io* is compatible with the following versions of the Unity Editor:

* `2019.4` and later (recommended)

It's also strongly recommended to have [git](https://git.com) installed, so you can very simply download and update the package.
And to be able to use the package at all, you first need to enable the new version of API Tools for your project.

#### Dependencies

See below for libraries and other packages bundled with this one.

Most importantly, this is all an extension to the official library:

* The official [**Newgrounds.io for Unity**](https://bitbucket.org/newgrounds/newgrounds.io-for-unity-c/) library

Additionally, the following Unity packages are used:

* [**Settings Manager**](https://docs.unity3d.com/Packages/com.unity.settings-manager@1.0/) package for a more robust way to store user/project preferences
* [**Editor Coroutines**](https://docs.unity3d.com/Packages/com.unity.editorcoroutines@0.0/) package for handling login and loading project information in Editor during setup

And these external libraries:

* [**UniGif**](https://github.com/WestHillApps/UniGif) for downloading user icons in GIF format
* [**DotNetZip**](https://archive.codeplex.com/?p=dotnetzip) for automatically creating a ZIP from the build

#### Documentation history
|Date|Reason|
|---|---|
|September 23, 2021|Updated minimal required version.|
|May 3, 2020|Just remembered this section exists.|
|March 6, 2020|Updated the ```git``` link.|
|November 20, 2019|Added sections covering Quick Start and the basics.|
|October 13, 2019|Document created. Matches package version 0.7.1-preview.|
