---
uid: api_index
---

#### API Documentation

Welcome to the API Documentation. 

Please note only the extension of the [original library](https://bitbucket.org/newgrounds/newgrounds.io-for-unity-c/) is documented; as for the rest, please refer [here](http://www.newgrounds.io/help/).
