using UnityEngine;

/* Use as: [ReadonlyField] public float number; */

namespace io.newgrounds.unity
{

    public class ReadonlyFieldAttribute : PropertyAttribute { }

}
