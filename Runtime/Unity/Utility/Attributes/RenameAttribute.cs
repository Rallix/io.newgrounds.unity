using UnityEngine;

namespace io.newgrounds.unity
{
    /// <summary> Change the variable name displayed in the Inspector. </summary>
    public class RenameAttribute : PropertyAttribute
    {
        public string NewName { get; private set; }

        public RenameAttribute(string name)
        {
            NewName = name;
        }
    }
}
