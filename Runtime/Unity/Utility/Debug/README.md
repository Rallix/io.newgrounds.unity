The DLL contains the **NgDebug** static class.  
These debug messages are shown *only* in the Editor and in Development Builds, not in the final built game.

* `NgDebug.Log`
* `NgDebug.LogFormat`
* `NgDebug.LogWarning`
* `NgDebug.LogWarningFormat`
* `NgDebug.LogError`
* `NgDebug.LogErrorFormat`
* `NgDebug.LogException`

Having it inside a DLL allows you to double-click on Log messages in the editor Console in order to be taken directly to the line in code using the logging method.