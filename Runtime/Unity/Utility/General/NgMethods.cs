﻿using System;
using UnityEngine;

namespace io.newgrounds.unity
{
    /// <summary>Provides a handful of all-purpose helpful Runtime methods.</summary>
    static class NgMethods
    {
        /// <summary> Checks if the user is currently playing on Newgrounds. </summary>
        internal static bool CheckIfNewgrounds()
        {
            const string newgroundsUrl = "uploads.ungrounded.net";

            string absoluteUrl = Application.absoluteURL;
            if (string.IsNullOrWhiteSpace(absoluteUrl)) return false;
            var uri = new Uri(Application.absoluteURL);
            return uri.Host.Contains(newgroundsUrl);
        }
    }
}