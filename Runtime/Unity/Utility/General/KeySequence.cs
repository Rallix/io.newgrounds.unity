﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace io.newgrounds.unity.utility
{

    /// <summary> Trigger a function when a key sequence is entered. </summary>
    [HelpURL("http://answers.unity.com/answers/592471/view.html")]
    public class KeySequence : MonoBehaviour
    {

        [SerializeField, Tooltip("The maximum delay allowed between pressing the keys.")]
        float allowedDelay = 1f;
        [SerializeField, Tooltip("The key sequence to press")] KeyCode[] sequence = KonamiCode;
        [SerializeField] UnityEvent onComplete = default;

        float delayTimer;
        int index = 0;

        /// <summary> The event triggered when the key sequence in entered correctly. </summary>
        public UnityEvent OnComplete
        {
            get { return onComplete; }
        }

        /// <summary> Sets the required sequence to press in order to trigger the event. </summary>
        /// <param name="keySequence">A sequence of key codes.</param>
        public void Sequence(IEnumerable<KeyCode> keySequence) => sequence = keySequence.ToArray();

        /// <summary> Sets the required sequence to press in order to trigger the event. </summary>
        /// <param name="value">A string to convert in a sequence of key codes.</param>
        public void Sequence(string value) => sequence = value.ToCharArray().Select(CharToKeyCode).ToArray();

        /// <summary> The traditional cheat code from the console era. </summary>
        static readonly KeyCode[] KonamiCode =
        {
                KeyCode.UpArrow, KeyCode.UpArrow,
                KeyCode.DownArrow, KeyCode.DownArrow,
                KeyCode.LeftArrow, KeyCode.RightArrow,
                KeyCode.LeftArrow, KeyCode.RightArrow,
                KeyCode.B, KeyCode.A
        };

        void Reset()
        {
            index = 0;
            delayTimer = 0f;
        }

        void Update()
        {
            delayTimer += Time.deltaTime;

            if (delayTimer > allowedDelay) Reset();

            if (Input.anyKeyDown)
            {
                if (Input.GetKeyDown(sequence[index]))
                {
                    index++;
                    delayTimer = 0f;
                }
                else if (Event.current is Event e && e.isKey)
                {
                    // Reset for the keyboard, not mouse
                    Reset();
                }
            }

            if (index == sequence.Length)
            {
                Reset();
                onComplete.Invoke();
            }
        }

        /// <summary> Converts a character into the matching key code. </summary>
        /// <param name="character"> A keyboard character. </param>
        /// <returns>A key code representing the character</returns>
        KeyCode CharToKeyCode(char character)
        {        
            var resultingCode = KeyCode.Question;
            if (char.IsLetter(character))
            {
                if (!Enum.TryParse($"{character}", true, out resultingCode))
                {
                    Debug.LogWarning($"Unrecognized letter: '<b>{character}</b>'");
                }
                return resultingCode;
            }
            if (char.IsDigit(character))
            {
                // Alphanumberic; Keypad isn't used
                if (!Enum.TryParse($"Alpha{character}", true, out resultingCode))
                {
                    Debug.LogWarning($"Unrecognized digit: '<b>{character}</b>'");
                }
                return resultingCode;
            }

            switch (character) {
                // TODO: For the remaining special characters
                case '.':
                    return KeyCode.Period;
                case '=':
                    return KeyCode.Equals;
                case '?':
                    return KeyCode.Question;
                case '!':
                    return KeyCode.Exclaim;
                case ';':
                    return KeyCode.Semicolon;
                default:
                    return resultingCode;
            }
        }

    }

}
