﻿using System;
using System.Collections.Generic;

namespace io.newgrounds.unity.utility
{

    /// <summary> A <see cref="ShuffleBag{T}"/> for lowercase letters, based on their frequency in the specified language. </summary>
    public class LetterBag : ShuffleBag<char>
    {

        public enum Language
        {

            English,
            French,
            German,
            Spanish,
            Portuguese,
            Esperanto,
            Italian,
            Turkish,
            Swedish,
            Polish,
            Dutch,
            Danish,
            Icelandic,
            Finnish,
            Czech

        }

        #region Frequencies

        Dictionary<char, double> GetLanguageFreqencies(Language language)
        {
            switch (language)
            {
                case Language.English:
                    return FrequenciesEnglish;
                case Language.French:
                    return FrequenciesFrench;
                case Language.German:
                    return FrequenciesGerman;
                case Language.Spanish:
                    return FrequenciesSpanish;
                case Language.Portuguese:
                    return FrequenciesPortuguese;
                case Language.Esperanto:
                    return FrequenciesEsperanto;
                case Language.Italian:
                    return FrequenciesItalian;
                case Language.Turkish:
                    return FrequenciesTurkish;
                case Language.Swedish:
                    return FrequenciesSwedish;
                case Language.Polish:
                    return FrequenciesPolish;
                case Language.Dutch:
                    return FrequenciesDutch;
                case Language.Danish:
                    return FrequenciesDanish;
                case Language.Icelandic:
                    return FrequenciesIcelandic;
                case Language.Finnish:
                    return FrequenciesFinnish;
                case Language.Czech:
                    return FrequenciesCzech;
                default:
                    throw new ArgumentOutOfRangeException(nameof(language), language, $"Unsupported language: {language}");
            }
        }

        /// <summary> The percentual distribution of letters in the English alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesEnglish = new Dictionary<char, double>
        {
                {'a', 8.167}, {'b', 1.492}, {'c', 2.782}, {'d', 4.253}, {'e', 12.702}, {'f', 2.228}, {'g', 2.015}, {'h', 6.094}, {'i', 6.966}, {'j', 0.153}, {'k', 0.772}, {'l', 4.025}, {'m', 2.406}, {'n', 6.749}, {'o', 7.507}, {'p', 1.929}, {'q', 0.095}, {'r', 5.987}, {'s', 6.327}, {'t', 9.056}, {'u', 2.758}, {'v', 0.978}, {'w', 2.360}, {'x', 0.150}, {'y', 1.974}, {'z', 0.074}
        };
        /// <summary> The percentual distribution of letters in the French alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesFrench = new Dictionary<char, double>
        {
                {'a', 7.636}, {'b', 0.901}, {'c', 3.260}, {'d', 3.669}, {'e', 14.715}, {'f', 1.066}, {'g', 0.866}, {'h', 0.737}, {'i', 7.529}, {'j', 0.613}, {'k', 0.074}, {'l', 5.456}, {'m', 2.968}, {'n', 7.095}, {'o', 5.796}, {'p', 2.521}, {'q', 1.362}, {'r', 6.693}, {'s', 7.948}, {'t', 7.244}, {'u', 6.311}, {'v', 1.838}, {'w', 0.049}, {'x', 0.427}, {'y', 0.128}, {'z', 0.326}, {'à', 0.486}, {'â', 0.051}, {'œ', 0.018}, {'ç', 0.085}, {'è', 0.271}, {'é', 1.504}, {'ê', 0.218}, {'ë', 0.008}, {'î', 0.045}, {'ï', 0.005}, {'ô', 0.023}, {'ù', 0.058}, {'û', 0.060}
        };
        /// <summary> The percentual distribution of letters in the German alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesGerman = new Dictionary<char, double>
        {
                {'a', 6.516}, {'b', 1.886}, {'c', 2.732}, {'d', 5.076}, {'e', 16.396}, {'f', 1.656}, {'g', 3.009}, {'h', 4.577}, {'i', 6.550}, {'j', 0.268}, {'k', 1.417}, {'l', 3.437}, {'m', 2.534}, {'n', 9.776}, {'o', 2.594}, {'p', 0.670}, {'q', 0.018}, {'r', 7.003}, {'s', 7.270}, {'t', 6.154}, {'u', 4.166}, {'v', 0.846}, {'w', 1.921}, {'x', 0.034}, {'y', 0.039}, {'z', 1.134}, {'ä', 0.578}, {'ö', 0.443}, {'ß', 0.307}, {'ü', 0.995}
        };
        /// <summary> The percentual distribution of letters in the Spanish alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesSpanish = new Dictionary<char, double>
        {
                {'a', 11.525}, {'b', 2.215}, {'c', 4.019}, {'d', 5.010}, {'e', 12.181}, {'f', 0.692}, {'g', 1.768}, {'h', 0.703}, {'i', 6.247}, {'j', 0.493}, {'k', 0.011}, {'l', 4.967}, {'m', 3.157}, {'n', 6.712}, {'o', 8.683}, {'p', 2.510}, {'q', 0.877}, {'r', 6.871}, {'s', 7.977}, {'t', 4.632}, {'u', 2.927}, {'v', 1.138}, {'w', 0.017}, {'x', 0.215}, {'y', 1.008}, {'z', 0.467}, {'á', 0.502}, {'é', 0.433}, {'í', 0.725}, {'ñ', 0.311}, {'ó', 0.827}, {'ú', 0.168}, {'ü', 0.012}
        };
        /// <summary> The percentual distribution of letters in the Portuguese alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesPortuguese = new Dictionary<char, double>
        {
                {'a', 14.634}, {'b', 1.043}, {'c', 3.882}, {'d', 4.992}, {'e', 12.570}, {'f', 1.023}, {'g', 1.303}, {'h', 0.781}, {'i', 6.186}, {'j', 0.397}, {'k', 0.015}, {'l', 2.779}, {'m', 4.738}, {'n', 4.446}, {'o', 9.735}, {'p', 2.523}, {'q', 1.204}, {'r', 6.530}, {'s', 6.805}, {'t', 4.336}, {'u', 3.639}, {'v', 1.575}, {'w', 0.037}, {'x', 0.253}, {'y', 0.006}, {'z', 0.470}, {'à', 0.072}, {'â', 0.562}, {'á', 0.118}, {'ã', 0.733}, {'ç', 0.530}, {'é', 0.337}, {'ê', 0.450}, {'í', 0.132}, {'ô', 0.635}, {'ó', 0.296}, {'õ', 0.040}, {'ú', 0.207}, {'ü', 0.026}
        };
        /// <summary> The percentual distribution of letters in the Esperanto alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesEsperanto = new Dictionary<char, double>
        {
                {'a', 12.117}, {'b', 0.980}, {'c', 0.776}, {'d', 3.044}, {'e', 8.995}, {'f', 1.037}, {'g', 1.171}, {'h', 0.384}, {'i', 10.012}, {'j', 3.501}, {'k', 4.163}, {'l', 6.104}, {'m', 2.994}, {'n', 7.955}, {'o', 8.779}, {'p', 2.755}, {'r', 5.914}, {'s', 6.092}, {'t', 5.276}, {'u', 3.183}, {'v', 1.904}, {'z', 0.494}, {'ĉ', 0.657}, {'ĝ', 0.691}, {'ĥ', 0.022}, {'ĵ', 0.055}, {'ŝ', 0.385}, {'ŭ', 0.520}
        };
        /// <summary> The percentual distribution of letters in the Italian alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesItalian = new Dictionary<char, double>
        {
                {'a', 11.745}, {'b', 0.927}, {'c', 4.501}, {'d', 3.736}, {'e', 11.792}, {'f', 1.153}, {'g', 1.644}, {'h', 0.636}, {'i', 10.143}, {'j', 0.011}, {'k', 0.009}, {'l', 6.510}, {'m', 2.512}, {'n', 6.883}, {'o', 9.832}, {'p', 3.056}, {'q', 0.505}, {'r', 6.367}, {'s', 4.981}, {'t', 5.623}, {'u', 3.011}, {'v', 2.097}, {'w', 0.033}, {'x', 0.003}, {'y', 0.020}, {'z', 1.181}, {'à', 0.635}, {'è', 0.263}, {'ì', 0.030}, {'í', 0.030}, {'ò', 0.002}, {'ù', 0.166}, {'ú', 0.166}
        };
        /// <summary> The percentual distribution of letters in the Turkish alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesTurkish = new Dictionary<char, double>
        {
                {'a', 12.920}, {'b', 2.844}, {'c', 1.463}, {'d', 5.206}, {'e', 9.912}, {'f', 0.461}, {'g', 1.253}, {'h', 1.212}, {'i', 9.600}, {'j', 0.034}, {'k', 5.683}, {'l', 5.922}, {'m', 3.752}, {'n', 7.987}, {'o', 2.976}, {'p', 0.886}, {'r', 7.722}, {'s', 3.014}, {'t', 3.314}, {'u', 3.235}, {'v', 0.959}, {'y', 3.336}, {'z', 1.500}, {'ç', 1.156}, {'ğ', 1.125}, {'ı', 5.114}, {'ö', 0.777}, {'ş', 1.780}, {'ü', 1.854}
        };
        /// <summary> The percentual distribution of letters in the Swedish alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesSwedish = new Dictionary<char, double>
        {
                {'a', 9.383}, {'b', 1.535}, {'c', 1.486}, {'d', 4.702}, {'e', 10.149}, {'f', 2.027}, {'g', 2.862}, {'h', 2.090}, {'i', 5.817}, {'j', 0.614}, {'k', 3.140}, {'l', 5.275}, {'m', 3.471}, {'n', 8.542}, {'o', 4.482}, {'p', 1.839}, {'q', 0.020}, {'r', 8.431}, {'s', 6.590}, {'t', 7.691}, {'u', 1.919}, {'v', 2.415}, {'w', 0.142}, {'x', 0.159}, {'y', 0.708}, {'z', 0.070}, {'å', 1.338}, {'ä', 1.797}, {'ö', 1.305}
        };
        /// <summary> The percentual distribution of letters in the Polish alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesPolish = new Dictionary<char, double>
        {
                {'a', 10.503}, {'b', 1.740}, {'c', 3.895}, {'d', 3.725}, {'e', 7.352}, {'f', 0.143}, {'g', 1.731}, {'h', 1.015}, {'i', 8.328}, {'j', 1.836}, {'k', 2.753}, {'l', 2.564}, {'m', 2.515}, {'n', 6.237}, {'o', 6.667}, {'p', 2.445}, {'r', 5.243}, {'s', 5.224}, {'t', 2.475}, {'u', 2.062}, {'v', 0.012}, {'w', 5.813}, {'x', 0.004}, {'y', 3.206}, {'z', 4.852}, {'ą', 0.699}, {'ć', 0.743}, {'ę', 1.035}, {'ł', 2.109}, {'ń', 0.362}, {'ó', 1.141}, {'ś', 0.814}, {'ź', 0.078}, {'ż', 0.706}
        };
        /// <summary> The percentual distribution of letters in the Dutch alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesDutch = new Dictionary<char, double>
        {
                {'a', 7.486}, {'b', 1.584}, {'c', 1.242}, {'d', 5.933}, {'e', 18.91}, {'f', 0.805}, {'g', 3.403}, {'h', 2.380}, {'i', 6.499}, {'j', 1.46}, {'k', 2.248}, {'l', 3.568}, {'m', 2.213}, {'n', 10.032}, {'o', 6.063}, {'p', 1.57}, {'q', 0.009}, {'r', 6.411}, {'s', 3.73}, {'t', 6.79}, {'u', 1.99}, {'v', 2.85}, {'w', 1.52}, {'x', 0.036}, {'y', 0.035}, {'z', 1.39}
        };
        /// <summary> The percentual distribution of letters in the Danish alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesDanish = new Dictionary<char, double>
        {
                {'a', 6.025}, {'b', 2.000}, {'c', 0.565}, {'d', 5.858}, {'e', 15.453}, {'f', 2.406}, {'g', 4.077}, {'h', 1.621}, {'i', 6.000}, {'j', 0.730}, {'k', 3.395}, {'l', 5.229}, {'m', 3.237}, {'n', 7.240}, {'o', 4.636}, {'p', 1.756}, {'q', 0.007}, {'r', 8.956}, {'s', 5.805}, {'t', 6.862}, {'u', 1.979}, {'v', 2.332}, {'w', 0.069}, {'x', 0.028}, {'y', 0.698}, {'z', 0.034}, {'å', 1.190}, {'æ', 0.872}, {'ø', 0.939}
        };
        /// <summary> The percentual distribution of letters in the Icelandic alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesIcelandic = new Dictionary<char, double>
        {
                {'a', 10.110}, {'b', 1.043}, {'d', 1.575}, {'e', 6.418}, {'f', 3.013}, {'g', 4.241}, {'h', 1.871}, {'i', 7.578}, {'j', 1.144}, {'k', 3.314}, {'l', 4.532}, {'m', 4.041}, {'n', 7.711}, {'o', 2.166}, {'p', 0.789}, {'r', 8.581}, {'s', 5.630}, {'t', 4.953}, {'u', 4.562}, {'v', 2.437}, {'x', 0.046}, {'y', 0.900}, {'á', 1.799}, {'æ', 0.867}, {'ð', 4.393}, {'é', 0.647}, {'í', 1.570}, {'ö', 0.777}, {'ó', 0.994}, {'þ', 1.455}, {'ú', 0.613}, {'ý', 0.228}
        };
        /// <summary> The percentual distribution of letters in the Finnish alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesFinnish = new Dictionary<char, double>
        {
                {'a', 12.217}, {'b', 0.281}, {'c', 0.281}, {'d', 1.043}, {'e', 7.968}, {'f', 0.194}, {'g', 0.392}, {'h', 1.851}, {'i', 10.817}, {'j', 2.042}, {'k', 4.973}, {'l', 5.761}, {'m', 3.202}, {'n', 8.826}, {'o', 5.614}, {'p', 1.842}, {'q', 0.013}, {'r', 2.872}, {'s', 7.862}, {'t', 8.750}, {'u', 5.008}, {'v', 2.250}, {'w', 0.094}, {'x', 0.031}, {'y', 1.745}, {'z', 0.051}, {'å', 0.003}, {'ä', 3.577}, {'ö', 0.444}
        };
        /// <summary> The percentual distribution of letters in the Czech alphabet. </summary>
        /// <remarks> Total sum of percentages is 99.965%. Contains lowercase keys. </remarks>
        static readonly Dictionary<char, double> FrequenciesCzech = new Dictionary<char, double>
        {
                {'a', 8.421}, {'b', 0.822}, {'c', 0.740}, {'d', 3.475}, {'e', 7.562}, {'f', 0.084}, {'g', 0.092}, {'h', 1.356}, {'i', 6.073}, {'j', 1.433}, {'k', 2.894}, {'l', 3.802}, {'m', 2.446}, {'n', 6.468}, {'o', 6.695}, {'p', 1.906}, {'q', 0.001}, {'r', 4.799}, {'s', 5.212}, {'t', 5.727}, {'u', 2.160}, {'v', 5.344}, {'w', 0.016}, {'x', 0.027}, {'y', 1.043}, {'z', 1.503}, {'á', 0.867}, {'č', 0.462}, {'ď', 0.015}, {'é', 0.633}, {'ě', 1.222}, {'í', 1.643}, {'ň', 0.007}, {'ó', 0.024}, {'ř', 0.380}, {'š', 0.688}, {'ť', 0.006}, {'ú', 0.045}, {'ů', 0.204}, {'ý', 0.995}, {'ž', 0.721}
        };

        #endregion

        /// <summary> Creates a lowercase letter shuffle bag with the standard letter frequency of a specified language. </summary>        
        /// <remarks> Uses the letter frequency from <a href="https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_letters_in_other_languages">Wikipedia</a>.
        /// For custom distribution of letters, use the base class's <see cref="ShuffleBag{T}.Add"/> method instead.</remarks>        
        /// <param name="amount"> How many times should each letter be added to the bag? </param>
        /// <param name="language"> Which language letter frequency to use? Non-English languages also contain accents. </param>
        public LetterBag(int amount, Language language = Language.English)
        {            
            var letterBag = new ShuffleBag<char>();
            foreach (var letter in GetLanguageFreqencies(language))
            {
                int letterCount = (int) letter.Value * amount;
                letterBag.Add(letter.Key, letterCount);
            }
        }

    }

}
