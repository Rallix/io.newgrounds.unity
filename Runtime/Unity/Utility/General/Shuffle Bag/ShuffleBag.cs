﻿using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace io.newgrounds.unity.utility
{

    /// <summary> A collection to randomly choose items from with
    /// a more fair and even distribution than with <see cref="UnityEngine.Random"/> class. </summary>
    /// <remarks> Underlying collection is a <see cref="List{T}"/> to preserve order of the elements. </remarks>
    public class ShuffleBag<T>
    {

        List<T> data;

        T currentItem;
        int currentPosition = -1;

        int Capacity => data.Capacity;
        public int Size => data.Count;

        /// <summary> Creates a new shuffle bag collection. </summary>
        public ShuffleBag() => data = new List<T>();

        /// <summary> Adds a value into the shuffle bag. </summary>
        /// <param name="item">Value to add.</param>
        /// <param name="amount">How many times to add the value.</param>
        public void Add(T item, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                data.Add(item);
            }
            currentPosition = Size - 1;
        }

        /// <summary> Pulls the next value from the shuffle bag. </summary>
        public T Next()
        {
            if (currentPosition < 1)
            {
                currentPosition = Size - 1;
                currentItem = data[0];

                return currentItem;
            }

            int position = Random.Range(0, currentPosition);

            currentItem = data[position];
            data[position] = data[currentPosition];
            data[currentPosition] = currentItem;
            currentPosition--;

            return currentItem;
        }

    }

}
