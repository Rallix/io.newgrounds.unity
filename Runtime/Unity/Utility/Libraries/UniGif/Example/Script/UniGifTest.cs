﻿/*
UniGif
Copyright (c) 2015 WestHillApps (Hironari Nishioka)
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*/

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UniGifLibrary {
    public class UniGifTest : MonoBehaviour
    {
        [SerializeField] InputField m_inputField = default;
        [SerializeField] UniGifImage m_uniGifImage = default;

        private bool m_mutex;

        public void OnButtonClicked()
        {
            if (m_mutex || m_uniGifImage == null || string.IsNullOrEmpty(m_inputField.text))
            {
                return;
            }

            m_mutex = true;
            StartCoroutine(ViewGifCoroutine());
        }

        private IEnumerator ViewGifCoroutine()
        {
            yield return StartCoroutine(m_uniGifImage.SetGifFromUrlCoroutine(m_inputField.text));
            m_mutex = false;
        }
    }
}