﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

// TODO: Bind with Spyglass extension.

namespace io.newgrounds.unity
{
    /// <summary> A component mantaining the user's login and logout. 
    /// Use <see cref="SpyglassComponent"/> in addition to pull more complex information. </summary>
    [AddComponentMenu("Newgrounds/User", 10)]
    [HelpURL("https://www.newgrounds.io/help/objects/#user")]
    public class UserComponent : NewgroundsComponent
    {
        // User's name
        public const string DefaultName = "Tranquillity"; // Existing level 1 fallback user
        const float LoginCheckInterval = 3f;

        // User currently playing the game
        public delegate void LoggedIn(objects.user user);
        public event LoggedIn OnLoggedIn;
        [SerializeField, ReadonlyField, UsedImplicitly] string username = default;
        
        /// <summary> The <see cref="objects.user"/> associated with this component. </summary>
        public objects.user User
        {
            [CanBeNull] get 
            {
                if (user == null) StartCoroutine(KeepCheckingForLogin());
                return user; // null if missing
            }
        }
        objects.user user = null;

        /// <summary> The username of the player, or <see cref="DefaultName"/>. </summary>
        public string Username
        {
            get { return User == null ? DefaultName : User.name; }
        }

        IEnumerator KeepCheckingForLogin()
        {
            bool isLoggedIn = false;
            while (!isLoggedIn)
            {
                NgioCore.checkLogin((loggedIn) =>
                {
                    if (loggedIn)
                    {
                        user = NgioCore.current_user;
                        username = Username;
                        OnLoggedIn?.Invoke(user);
                        isLoggedIn = true;
                    }
                });
                // Check for login every once in a while
                yield return new WaitForSecondsRealtime(LoginCheckInterval);
            }
        }

        protected override void StartReady()
        {
            StartCoroutine(KeepCheckingForLogin());
            if (user != null) username = Username;
        }
    }
}
