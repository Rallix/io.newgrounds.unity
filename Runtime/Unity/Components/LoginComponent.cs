﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

namespace io.newgrounds.unity
{

    // TODO: Merge with UserComponent (does the same, really)

    // TODO: Separate from a button

    /// <summary>
    /// Defines the <see cref="LoginComponent" />.
    /// </summary>
    [AddComponentMenu("Newgrounds/Login", 20)]
    [HelpURL("http://www.newgrounds.io/help/passport")]
    public class LoginComponent : NewgroundsComponent
    {

        [SerializeField] Button loginButton = default;
        [SerializeField, Tooltip("Button text to refect the login state. Can be left empty.")]
        Text buttonText = default;
        [SerializeField] Text usernameText = default;

        [Space, Tooltip("An action triggered when the user logs in.")] public UnityEvent onLoggedIn = new UnityEvent();
        [Tooltip("An action triggered when the user logs out.")] public UnityEvent onLoggedOut = new UnityEvent();

        protected override void StartReady()
        {
            base.StartReady();
            
            if (!loginButton) NgDebug.Log("There is no login button assigned.", gameObject);

            NgioCore.checkLogin(loggedIn => ApplyLogin(loggedIn));    // TODO: On NG, a user might not be logged-in
            // if (CoreComponent.IsOnNewgrounds) gameObject.SetActive(false); // TODO: Remove from the button
        }

        /// <summary>Applies the login state to the UI components.</summary>
        /// <param name="loggedIn">Is the user currently logged in?</param>
        /// <param name="now"> Did the user log in or out just now, or is it just a check?</param>
        void ApplyLogin(bool loggedIn, bool now = false)
        {
            if (usernameText) usernameText.text = loggedIn ? NgioCore.current_user.name : "<color=yellow>Unknown User</color>";
            if (buttonText) buttonText.text = !loggedIn ? "Log in" : "Log out";
            if (now)
            {
                if (loggedIn) OnLoggedIn();
                else OnLoggedOut();
            }
        }

        /// <summary>Logs the user in or logs him out if he already was logged in.</summary>
        public void LoginLogout()
        {
            if (NgioCore.current_user.name != null) LogOutUser();
            else LogInUser();
        }

        /// <summary>If a user isn't already logged in, request login.</summary>
        public void LogInUser()
        {
            NgioCore.checkLogin(loggedIn =>
            {
                if (!loggedIn)
                {
                    NgioCore.requestLogin(() => ApplyLogin(true, true),
                                          () => Debug.LogWarning("Attempt to log in to Newgrounds failed."),
                                          () => Debug.Log("Attempt to log in to Newgrounds was cancelled."));
                }
                else
                {
                    if (NgioCore.current_user.name != null)
                    {
                        ApplyLogin(true);
                    }
                }
            });
        }

        /// <summary> Changes the Text label of the login button. </summary>
        /// <param name="text"> The new label of the button. </param>
        public void ChangeButtonText(string text)
        {
            var loginText = loginButton.GetComponentInChildren<Text>();
            if (loginText) loginText.text = text;
        }

        /// <summary>Logs out the current user.</summary>
        public void LogOutUser()
        {
            NgioCore.logOut();
            ApplyLogin(false, true);
        }

        /// <summary> An event triggered on logging in. </summary>
        void OnLoggedIn()
        {
            onLoggedIn.Invoke();
        }

        /// <summary> An event triggered on logging in. </summary>
        void OnLoggedOut()
        {
            onLoggedOut.Invoke();
        }

        /// <summary> A simple test to see if it works. </summary>
        public void Scream()
        {
            Debug.Log("AAAAAAAAAAAAAAAAAAA!");
        }

    }

}
