﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace io.newgrounds.unity
{
    public class CoroutineQueue
    {

        readonly Queue<IEnumerator> actions = new Queue<IEnumerator>();

        readonly MonoBehaviour owner;
        Coroutine processingCoroutine;

        bool running;

        /// <summary> Set up a coroutine queue. </summary>
        /// <param name="coroutineOwner">MonoBehaviour script queuing actions. Usually just <see langword="this" />.</param>
        public CoroutineQueue(MonoBehaviour coroutineOwner)
        {
            owner = coroutineOwner;
        }

        /// <summary> Start running queued coroutines. </summary>
        public void StartLoop()
        {
            processingCoroutine = owner.StartCoroutine(Process());
        }

        /// <summary> Interrupts queued coroutines and discards the rest. </summary>
        public void StopLoop()
        {
            owner.StopCoroutine(processingCoroutine);
            processingCoroutine = null;
            actions.Clear();
        }

        /// <summary>
        ///     Enqueue a coroutine.
        ///     Use <see cref="SignalStart" /> and <see cref="SignalEnd" /> to wrap the coroutine you want to enqueue.
        /// </summary>
        /// <param name="action">A coroutine to enqueue.</param>
        public void EnqueueAction(IEnumerator action)
        {
            actions.Enqueue(action);
        }

        /// <summary> Check if the coroutine queue is empty. </summary>
        /// <returns><see langword="True" /> if there are no queued actions.</returns>
        public bool IsEmpty()
        {
            return actions.Count == 0;
        }

        /// <summary> Counts currently queued actions. </summary>
        /// <returns> The Number of currently queued actions. </returns>
        public int Count()
        {
            return actions.Count;
        }

        /// <summary>
        ///     Signal the start of a coroutine.
        ///     No other coroutine will run until <see cref="SignalEnd" /> is called.
        /// </summary>
        public void SignalStart()
        {
            running = true;
        }

        /// <summary> Signal the end of a coroutine. </summary>
        public void SignalEnd()
        {
            running = false;
        }

        /// <summary> Run queued coroutines one by one. </summary>
        IEnumerator Process()
        {
            while (true)
                if (actions.Count > 0 && !running)
                    yield return owner.StartCoroutine(actions.Dequeue());
                else
                    yield return null;
            // ReSharper disable once IteratorNeverReturns
        }

        /// <summary> Enqueue a coroutine which simply waits for a specified time. </summary>
        /// <param name="waitTime">How long to wait.</param>
        /// ///
        /// <param name="realtime">Wait for realtime seconds instead.</param>
        public void EnqueueWait(float waitTime, bool realtime = false)
        {
            actions.Enqueue(Wait(waitTime, realtime));
        }


        static IEnumerator Wait(float waitTime, bool realtime = false)
        {
            if (!realtime) yield return new WaitForSeconds(waitTime);
            else yield return new WaitForSecondsRealtime(waitTime);
        }

    }
}
