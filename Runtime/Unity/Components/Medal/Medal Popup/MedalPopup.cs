﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace io.newgrounds.unity
{

    /// <summary>A medal popup shown in an overlaying canvas when a medal is unloced.</summary>
    [RequireComponent(typeof(Animator))]
    public class MedalPopup : MonoBehaviour
    {

        //! USAGE:
        // Call ShowPopup(Sprite medalIcon, string medalName, int medalPoints) method of the medal popup

        [Header("Animation and sound")] [SerializeField] AnimationClip medalAppearAnimation = default;
        [SerializeField] AudioClip medalAppearSound = default;
        [Space(10)] [SerializeField] AnimationClip medalDisappearAnimation = default;
        [SerializeField] AudioClip medalDisappearSound = default;

        Animator animator;
        bool animationTransition = false;
        static readonly int AppearTrigger = Animator.StringToHash("Appear");
        static readonly int DisappearTrigger = Animator.StringToHash("Disappear");
        static readonly int NameDisplayBool = Animator.StringToHash("Name won't fit");
        static readonly int AppearSpeedMutliplier = Animator.StringToHash("Appear Speed Multiplier");
        static readonly int DisappearSpeedMutliplier = Animator.StringToHash("Disappear Speed Multiplier");

        [Header("UI elements")] [SerializeField] Image medalIconUI = default; //! 50×50 pixels
        [SerializeField] Image medalNameMaskUI = default;
        [SerializeField] Text medalNameUI = default;   //! Max 64 characters
        [SerializeField] Text medalPointsUI = default; //! Easy|5, Moderate|10, Challenging|25, Difficult|50, Brutal|100 – Max 500 points

        float visibleNameWidth;

        CoroutineQueue medalQueue; // For multiple medals unlocked at once

        void Start()
        {
            animator = GetComponent<Animator>();
            medalQueue = new CoroutineQueue(this);
            visibleNameWidth = medalNameMaskUI.rectTransform.rect.width;

            MatchAnimationSpeedWithWithSoundLength(medalAppearAnimation, medalAppearSound, AppearSpeedMutliplier);
            MatchAnimationSpeedWithWithSoundLength(medalDisappearAnimation, medalDisappearSound, DisappearSpeedMutliplier);

            Reset(); // Default values
        }

        /// <summary>
        /// Call this to show a popup and start its animation.
        /// </summary>
        /// <param name="medalIcon">Icon of the medal.</param>
        /// <param name="medalName">Name of the medal.</param>
        /// <param name="medalPoints">Point value of the medal.</param>
        public void ShowPopup(Sprite medalIcon, string medalName, int medalPoints)
        {
            if (medalQueue.IsEmpty())
            {
                medalQueue.StartLoop();
            }
            medalQueue.EnqueueAction(PopupAnimation(medalIcon, medalName, medalPoints));
        }

        /// <summary>
        /// Call this with a medalData structure to show a popup and start its animation.
        /// </summary>
        /// <param name="medalData">Structure containing all the medal's data.</param>
        public void ShowPopup(MedalData medalData)
        {
            ShowPopup(medalData.Icon, medalData.Name, medalData.Points);
        }

        IEnumerator PopupAnimation(Sprite medalIcon, string medalName, int medalPoints)
        {
            medalQueue.SignalStart();

            // Before the animation starts           
            medalIconUI.sprite = medalIcon;
            medalNameUI.text = medalName;
            medalPointsUI.text = medalPoints.ToString();

            // Text length
            //? Move text already in "appear" state
            // FROM +visibleNameWidth TO -currentNameWidth
            float currentNameWidth = GetPrefferedWidth(medalNameUI);
            bool nameWontFit = visibleNameWidth < currentNameWidth;
            animator.SetBool(NameDisplayBool, nameWontFit);

            //! Appear animation
            animator.SetTrigger(AppearTrigger);
            PlaySound(medalAppearSound);
            yield return new WaitForSeconds(medalAppearSound.length);

            //! Idle animation
            //? Calculate idle duration from character count        
            yield return new WaitUntil(() => animationTransition);

            //! Disappear animation
            animator.SetTrigger(DisappearTrigger);
            PlaySound(medalDisappearSound);
            yield return new WaitForSeconds(medalDisappearSound.length);

            // After the animation finishes
            Reset();
            if (medalQueue.IsEmpty())
            {
                medalQueue.StopLoop();
            }
            medalQueue.SignalEnd();
        }

        // TODO: Find a better way to check animation state start/end?

        /// <summary>
        /// Triggered at the START of each animation state by event.
        /// </summary>
        void StartState()
        {
            animationTransition = false;
        }

        /// <summary>
        /// Triggered at the END of each animation state by event.
        /// </summary>
        void EndState()
        {
            animationTransition = true;
        }

        /// <summary>
        /// Gets the correct width of a recently edited text field with dynamic width value which might not have been properly applied yet.
        /// </summary>
        /// <param name="textField">Text field to get width from.</param>
        /// <returns>Width of the  text field.</returns>
        static float GetPrefferedWidth(Text textField)
        {
            var textGen = new TextGenerator();
            TextGenerationSettings generationSettings = textField.GetGenerationSettings(textField.rectTransform.rect.size);
            return textGen.GetPreferredWidth(textField.text, generationSettings);
        }

        void MatchAnimationSpeedWithWithSoundLength(AnimationClip anim, AudioClip medalSound, int floatSpeedMultiplierHash)
        {
            animator.SetFloat(floatSpeedMultiplierHash, anim.length / medalSound.length);
        }

        /// <summary>
        /// Play a sound to the first active audio listener. Plays nothing when there's none.
        /// </summary>
        /// <param name="sound">Sound to play.</param>
        static void PlaySound(AudioClip sound)
        {
            var listener = FindObjectOfType<AudioListener>();
            if (listener)
            {
                AudioSource.PlayClipAtPoint(sound, listener.transform.position, AudioListener.volume);
            }
        }

        /// <summary> Remove last medal's info and prepare for the next one. </summary>
        void Reset()
        {
            medalIconUI.sprite = null;
            medalNameUI.text = "Medal";
            medalPointsUI.text = "0";
        }

    }
}
