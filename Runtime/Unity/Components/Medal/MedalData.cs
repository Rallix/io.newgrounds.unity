﻿using System.Collections.Generic;
using System.Linq;
using io.newgrounds.objects;
using JetBrains.Annotations;
using UnityEngine;

namespace io.newgrounds.unity
{

    /// <summary> An asset for keeping and passing medal data to <see cref="MedalComponent"/>. </summary>
    /// <remarks> Doesn't contain all information from the website; just those necessary to display a medal popup.</remarks>
    [CreateAssetMenu(fileName = "Medal", menuName = "Newgrounds/Medal Data")]
    public class MedalData : NgData
    {
        /// <summary> Medal difficulty setting. </summary>
        public enum Difficulty
        {
            /// <summary> 5 points. </summary>
            Easy,
            /// <summary> 10 points. </summary>
            Moderate, 
            /// <summary> 25 points. </summary>
            Challenging,
            /// <summary> 50 points. </summary>
            Difficult,
            /// <summary> 100 points. </summary>
            Brutal
        }

        static readonly Dictionary<Difficulty, int> DifficultyPointMap = new Dictionary<Difficulty, int>
        {
                {Difficulty.Easy, 5},
                {Difficulty.Moderate, 10},
                {Difficulty.Challenging, 25},
                {Difficulty.Difficult, 50},
                {Difficulty.Brutal, 100}
        };

        /// <summary> Returns the appropriate number of points based on difficulty. </summary>
        public static int GetPoints(Difficulty dif)
        {
            return DifficultyPointMap.TryGetValue(dif, out int value) ? value : 0;
        }
        
        /// <summary> Returns the appropriate difficulty based on the amount of points. </summary>
        public static Difficulty GetDifficulty(int points)
        {
            return DifficultyPointMap.Keys.FirstOrDefault(diff => DifficultyPointMap[diff] == points);
        }

        /// <summary> Sets the properties of the medal data asset. </summary>
        /// <param name="icon"> The icon of the medal. </param>
        /// <param name="title"> The displayed title of the medal. </param>
        /// <param name="dif"> The difficulty of the medal (determining points). </param>
        /// <param name="description"> The description of the medal (or your custom notes), not shown in the game. </param>
        public void SetMedalData(Sprite icon, string title, Difficulty dif, string description = "")
        {
            Icon = icon;
            Name = title;
            this.difficulty = dif;
            if (!string.IsNullOrWhiteSpace(description)) this.customDescription = description;
        }

        /// <summary> Sets the basic properties of the medal data asset from raw medal object. </summary>
        /// <param name="medal"> The raw, returned medal object. </param>
        public void SetRawMedalData(medal medal)
        {
            id = medal.id;
            Name = medal.name;
            this.difficulty = (Difficulty) medal.difficulty;
            this.customDescription = medal.description;
        }

        /// <summary> The icon of the medal. </summary>
        public Sprite Icon
        {
            get { return medalIcon; }
            private set { medalIcon = value; }
        }

        /// <summary> The points awarded for unlocking the medal. </summary>
        public int Points
        {
            get { return GetPoints(difficulty); }
        }
        
        [SerializeField, Tooltip("The icon of the medal.")] Sprite medalIcon;
        [SerializeField, Tooltip("The difficulty of the medal.")] Difficulty difficulty = Difficulty.Easy;

        [Space, SerializeField, TextArea, UsedImplicitly, Tooltip("Optional description to remind you what to do with the medal. Doesn't need to match the API Tools.")]
        string customDescription;
        
    }

}
