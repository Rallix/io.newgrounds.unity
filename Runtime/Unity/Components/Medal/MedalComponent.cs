﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using io.newgrounds.objects;
using UnityEngine;
using UnityEngine.Events;
using io.newgrounds.results.Medal;
using JetBrains.Annotations;

namespace io.newgrounds.unity
{
    // TODO: Add "IsUnlocked" property and run it after initialization, and then change it after unlocking

    [AddComponentMenu("Newgrounds/Medal", 30)]
    [HelpURL("http://www.newgrounds.io/help/objects/#medal")]
    public class MedalComponent : NewgroundsComponent
    {
        /// <summary> The medal associated with this component. </summary>
        public MedalData medalData = default; // TODO: Display error message when there's no medal AND NO OTHER UNLOCKING METHOD + warning for wrong values

        [SerializeField, ReadonlyField] int id = 0; // TODO: Update every time the inspector is opened
        [Space] public UnityEvent onMedalUnlocked;

        // Show the medal popup when the medal is unlocked?
        bool showPopup = true;

        // TODO: Get 'already unlocked' status at the beginning of the game (and let it be disabled)

        MedalPopup popup;
        Func<bool> unlockingPredicate;

        // ReSharper disable once CollectionNeverQueried.Local
        List<Coroutine> unlockingCoroutines = new List<Coroutine>();

        /// <summary> The numeric ID of the current medal. </summary>
        public int Id => medalData != null ? medalData.Id : 0;

        /// <summary> The name of the medal. </summary>
        [UsedImplicitly] public string Name => medalData != null ? medalData.Name : string.Empty;

        void OnEnable()
        {
            // TODO: Get rid of this if a custom editor is created
            if (medalData) id = medalData.Id;
        }

        void OnValidate()
        {
            RefreshId(); // TODO: Get rid of this if a custom editor is created
        }

        /// <summary> Updates the ID from the medal data. </summary>
        void RefreshId()
        {
            if (medalData) id = medalData.Id;
        }

        void Start()
        {
            // TODO: Can be 'null' if using other UnlockMethods
            if (medalData == null) enabled = false;
            Debug.Assert(medalData != null, "There is no medal set to unlock.", gameObject);
            Debug.Assert(medalData.Id != 0, $"The medal '{medalData.name}' you're trying to unlock doesn't have a valid ID." +
                                            "See the API Tools in the Project System of your game and assign a valid ID", medalData);
            popup = CoreComponent.ngioCore.GetComponentInChildren<MedalPopup>();
            if (showPopup)
            {
                Debug.Assert(popup != null, "The active core component doesn't seem to contain a medal popup UI to show.", CoreComponent.ngioCore);
            }
        }

        public void Unlock()
        {
            UnlockMedal(medalData);
        }

        public void UnlockWhen(Func<bool> predicate)
        {
            if (unlockingPredicate != null)
            {
                // TODO: An array of predicates to allow multiple options
                Debug.LogError("There's already an unlocking predicate defined.", gameObject);
                return;
            }
            unlockingPredicate = predicate;
            Coroutine unlock = StartCoroutine(ConditionalUnlock(unlockingPredicate));
            unlockingCoroutines.Add(unlock);
        }

        IEnumerator ConditionalUnlock(Func<bool> predicate)
        {
            yield return new WaitUntil(predicate);
            UnlockMedal(medalData);
            // TODO: Remove from unlocking coroutines
        }

        void UnlockMedal(MedalData data)
        {
            var medalList = new components.Medal.getList();
            medalList.callWith(CoreComponent.ngioCore, OnGetMedalList);
            // TODO: Only unlock if it wasn't unlocked before
            
            var medalUnlock = new components.Medal.unlock {id = medalData.Id};            
            NgDebug.LogFormat("Trying to unlock medal '{0}' with {1} for user '{2}'.", medalData.Id, CoreComponent.ngioCore.name, CoreComponent.ngioCore.current_user.name);
            medalUnlock.callWith(CoreComponent.ngioCore, OnMedalUnlocked);
        }

        void OnGetMedalList(getList getList)
        {
            List<medal> medals = getList.medals.Cast<medal>().ToList();
            medal current = medals.FirstOrDefault(m => m.id == id);
            if (current == null)
            {
                //! Wrong ID or bad cast
                if (medals.Count >= 1 && id != 0)
                {
                    // Good cast, bad ID
                    NgDebug.LogWarning($"Trying to unlock a non-existent medal with ID: <b>{id}</b>.\nCheck your <i>Project Settings</i> on Newgrounds; and make sure you created a matching Medal asset in Unity.");
                }
                return;
            }
            // Debug.Log($"Medal '{current.name}' is {(current.unlocked ? "already unlocked." : "locked → unlocking.")}", gameObject);
            if (!current.unlocked) // TODO: Avoid re-unlocking for a player without an account
            {
                var medalUnlock = new components.Medal.unlock {id = current.id};
                // NgDebug.LogFormat("Trying to unlock medal '{0}' with {1} for user <b>'{2}'</b>.", current.id, CoreComponent.ngioCore.name, CoreComponent.ngioCore.current_user.name);
                try
                {
                    medalUnlock.callWith(CoreComponent.ngioCore, OnMedalUnlocked);
                }
                catch (InvalidCastException)
                {
                    //! Failed to unlock (see enable network data log)
                    // TODO: Log specific problem
                    NgDebug.LogWarning("Failed to unlock the medal. Try enabling the network data option of 'Core' prefab to see more details.");
                }
            }
        }

        /// <summary> An event called when the medal is unlocked. </summary>
        /// <param name="result"> Medal information. </param>
        void OnMedalUnlocked(unlock result)
        {
            if (showPopup) popup.ShowPopup(medalData);
            onMedalUnlocked?.Invoke(); // TODO: Doesn't trigger if the medal isn't real, or the connection is unsuccessful
        }

        /// <summary> Tries to find a given medal in a scene and unlocks it if possible. </summary>        
        public static void UnlockMedal(string name)
        {
            MedalComponent medal = FindMedal(name);
            if (!medal) Debug.LogWarning($"A medal could not be unlocked because there is no medal named '{name}' in the current scene.");
            else medal.Unlock();
        }

        /// <summary> Tries to find a given medal in a scene and unlocks it if possible. </summary>        
        public static void UnlockMedalWhen(string name, Func<bool> predicate)
        {
            MedalComponent medal = FindMedal(name);
            if (!medal) Debug.LogWarning($"No medal named '{name}' was found in the current scene.");
            else medal.UnlockWhen(predicate);
        }

        static MedalComponent FindMedal(string name)
        {
            MedalComponent[] medals = FindObjectsOfType<MedalComponent>();
            return medals.SingleOrDefault(m => !string.IsNullOrWhiteSpace(m.Name) &&
                                               m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
