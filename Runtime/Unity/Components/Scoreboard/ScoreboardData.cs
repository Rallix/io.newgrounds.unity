﻿using io.newgrounds.objects;
using JetBrains.Annotations;
using UnityEngine;

namespace io.newgrounds.unity
{

    /// <summary> An asset for keeping and passing scoreboard data to <see cref="ScoreboardComponent"/>. </summary>
    [CreateAssetMenu(fileName = "Scoreboard", menuName = "Newgrounds/Scoreboard Data")]
    public class ScoreboardData : NgData
    {

        /// <summary> The type of the score. </summary>
        public enum ScoreType
        {

            /// <summary> Scores will be posted individually. </summary>
            Standard,
            /// <summary> New scores are added to user's existing score value. </summary>
            Incremental

        }

        /// <summary> The order of scores being displayed. </summary>
        public enum SortScores
        {

            /// <summary> Displays the scores from highest to lowest (e.g. points). </summary>
            HighestFirst,
            /// <summary> Displays the scores from lowest to highest (e.g. time). </summary>
            LowestFirst

        }

        /// <summary> The format in which to display the scores. </summary>
        /// <remarks> All score formats are internally saved as integers on Newgrounds which requires a conversion. </remarks>
        public enum ScoreFormat
        {

            /// <summary> A simple integer value (1234567 is displayed as 1,234,567). </summary>
            Simple,
            /// <summary> A simple 2-place decimal value (1234567 is displayed as 12,345.67). </summary>
            Decimal,
            /// <summary> A dollar value based on total cents (1234567 is displayed as $12,345.67). </summary>
            Currency,
            /// <summary> A stopwatch time based on milliseconds (123456789 is displayed as 34:17:36.79). </summary>
            Time,
            /// <summary> A measurement in feet/inches based on total inches (123 is displayed as 10' 03"). </summary>
            Distance,
            /// <summary> A measurement in meters based on total centimeters (123456 is displayed as 1,234.56m). </summary>
            MetricDistance_m,
            /// <summary> A measurement in kilometers based on total meters (123456 is displayed as 1,234.56km) </summary>
            MetricDistance_km,

        }

        /// <summary> The type of the score. </summary>
        [UsedImplicitly] public ScoreType Type
        {
            get { return type; }
            private set { type = value; }
        }

        /// <summary> The order of scores being displayed. </summary>
        [UsedImplicitly] public SortScores Sorting
        {
            get { return sorting; }
            private set { sorting = value; }
        }

        /// <summary> The order of scores being displayed. </summary>
        [UsedImplicitly] public ScoreFormat Format
        {
            get { return format; }
            private set { format = value; }
        }

        [SerializeField, Tooltip("The type of the score.")] ScoreType type = ScoreType.Standard;
        [SerializeField, Tooltip("The order of scores being displayed.")] SortScores sorting = SortScores.HighestFirst;
        [SerializeField, Tooltip("The format in which to display the scores.")] ScoreFormat format = ScoreFormat.Simple;

        public void SetScoreboardData(string boardName, ScoreType scoreType, SortScores sortType, ScoreFormat scoreFormat)
        {
            Name = boardName;
            Type = scoreType;
            Sorting = sortType;
            Format = scoreFormat;
        }
        
        /// <summary> Sets the basic properties of the scoreboard data asset from raw scoreboard object. </summary>
        /// <param name="scoreboard"> The raw, returned scoreboard object. </param>
        public void SetRawScoreboardData(scoreboard scoreboard)
        {
            id = scoreboard.id;
            Name = scoreboard.name;
        }

        

    }

}
