﻿using io.newgrounds.results.ScoreBoard;

namespace io.newgrounds.unity {
    using UnityEngine;

    /// <summary> A component allowing you to handle connection to a Newgrounds scoreboard. </summary>
    [AddComponentMenu("Newgrounds/Scoreboard", 40)]
    [HelpURL("http://www.newgrounds.io/help/objects/#scoreboard")]
    public class ScoreboardComponent : NewgroundsComponent
    {

        // TODO: This is a simplistic version. Add more + another script to display the data.

        public ScoreboardData scoreboard;

        public void PostScore(int score)
        {
            var scoreboard = new io.newgrounds.components.ScoreBoard.postScore
            {
                    id = this.scoreboard.Id,
                    value = score
            };

            scoreboard.callWith(NgioCore, OnScorePost);
        }

        void OnScorePost(postScore post)
        {
            NgDebug.Log($"Post '{post.score.formatted_value}' score to scoreboard '{post.scoreboard.name}'.");
        }
    }
}