﻿using System.Text;
using System.Linq;
using UnityEngine;

namespace io.newgrounds.unity
{
    // TODO: UnityEvents (OnReady, OnLogin, OnMedalUnlocked etc.)

    // TODO: InvalidCastException when the API is wrong

    /// <summary> This is the core class for making calls to the Newgrounds.io server. </summary>
    [HelpURL("https://bitbucket.org/newgrounds/newgrounds.io-for-unity-c/")]
    [AddComponentMenu("Newgrounds/Core", -100)]
    public class CoreComponent : core
    {

        /// <summary> Sets the behaviour of the core component depending on how much control over the proccess you want. </summary>
        public static CoreMode coreMode = CoreMode.Automatic; // TODO: Get rid of this variable. It's used only in the Editor. Use settings instead.

        /// <summary> If true, the GameObject with this component attached will be passed between scenes. </summary>
        public bool dontDestroyOnLoad; // TODO: + add 'allow only one' option

        /// <summary> If true, the GameObject with this component won't be visible in the scene hierarchy. </summary>
        public bool hideInHierarchy;

        /// <summary> The instance of a <see cref="CoreComponent"/>. Should be just one per scene. </summary>
        public static CoreComponent ngioCore;

        /// <summary> How long should the component keep trying to initialize. </summary>
        const float TimeLimitToPing = 60 * 5f; // TODO: 'Ping?' bool in settings

        /// <summary>Behaviour of the <see cref="CoreComponent"/>.</summary>
        public enum CoreMode
        {

            /// <summary> Adds a core instance in the first scene and keeps it through the whole game. 
            /// In the editor, it's added whenever you enter play mode. </summary>
            Automatic,

            /// <summary> Let's you handle adding and removing core instance(s) on your own. 
            /// Without a core component, you won't be connected to Newgrounds and most other components won't work properly. </summary>
            Manual

        }

        /// <summary> The unique ID of your app as found in the 'API Tools' tab of your Newgrounds.com project. </summary> 
        public static string AppId
        {
            get { return ngioCore.app_id; }
            set { ngioCore.app_id = value; }
        }

        /// <summary> The unique ID of your app as found in the 'API Tools' tab of your Newgrounds.com project. </summary> 
        public static string EncryptionKey
        {
            get { return ngioCore.aes_base64_key; }
            set { ngioCore.aes_base64_key = value; }
        }

        /// <summary>
        /// Applies editor settings, e.g. makes sure there's only one instance which will be passed between scenes.
        /// </summary>
        public override void Start()
        {
            base.Start();
            // Editor switches
            if (dontDestroyOnLoad && Application.isPlaying)
            {
                // Check if instance already exists – if not, set instance to this
                if (ngioCore == null) ngioCore = this;
                // If instance already exists and it's not this – destroy this, so there can only ever be one instance
                else if (ngioCore != this) Destroy(gameObject);
                // Sets this to not be destroyed when reloading scene
                DontDestroyOnLoad(gameObject);
            }
            else ngioCore = this;
            if (hideInHierarchy)
            {
                gameObject.hideFlags |= HideFlags.HideInHierarchy;
            }            
        }        

        #if UNITY_EDITOR
        void OnDisable()
        {
            //! Warn if there are possible problems
            if (UnityEditor.EditorApplication.isPlaying && !this.enabled) DependencyWarning();
        }

        /// <summary>Find all objects dependant on this core instance.</summary>
        /// <returns>An array of dependant objects.</returns>
        public GameObject[] FindDependencies()
        {
            return FindObjectsOfType<NewgroundsComponent>()
                  .Where(component => component.GetLinkedCoreID() == GetInstanceID())
                  .Select(component => component.gameObject)
                  .Distinct()
                  .ToArray();
        }

        /// <summary>Checks whether any object still requires the <see cref="CoreComponent"/> when it's disabled or destroyed.</summary>
        void DependencyWarning()
        {
            GameObject[] dependencies = FindDependencies();
            if (dependencies.Length == 0) return; // The core is gone, but nothing depended on it in the current scene
            bool justOne = dependencies.Length == 1;
            
            //! Construct the warning message
            var result = new StringBuilder($"The core component has become inactive, but {dependencies.Length} object");
            if (!justOne) result.Append("s");
            result.Append(" still depend");
            if (justOne) result.Append("s");
            result.Append(" on it:\n<b>");
            for (int i = 0; i < dependencies.Length - 1; i++) result.Append(dependencies[i].name + ", ");
            result.Append(dependencies[dependencies.Length - 1].name + "</b>");

            NgDebug.LogWarning(result);
        }
        #endif

        /// <summary> Renews an existing session so that it stays alive even when inactive. </summary>
        [ContextMenu(nameof(Ping))]
        void Ping()
        {
            var ping = new components.Gateway.ping();
            ping.callWith(ngioCore, result => NgDebug.Log($"Ping was successful: {result.success}"));
        }
        [ContextMenu(nameof(Ping), true)]
        bool PingValidate()
        {
            return ngioCore != null && isReady();
        }
        
    }
}
