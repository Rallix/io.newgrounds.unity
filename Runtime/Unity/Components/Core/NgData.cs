﻿using UnityEngine;

namespace io.newgrounds.unity
{
    /// <summary> A base class asset for keeping and passing preset data to related components. </summary>
    public abstract class NgData : ScriptableObject
    {
        [SerializeField, Tooltip("The numeric ID as found in the API Tools on Newgrounds.")]
        protected int id = default;

        /// <summary> The numeric ID as found in the API Tools on Newgrounds. </summary>
        public int Id => id;

        [SerializeField, Tooltip("The title of the target.")] new string name;
        
        /// <summary> The title of the target. </summary>
        public string Name
        {
            get { return name; }
            protected set { name = value; }
        }

    }
}
