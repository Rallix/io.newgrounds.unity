﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace io.newgrounds.unity
{
    /// <summary>Waits until <see cref="CoreComponent.ngioCore"/> is initialized – or stops with an error if the timer expires.</summary>
    /// <remarks>Use as: <c>yield return new WaitForNgioCore(timeLimit)</c></remarks>
    class WaitForNgioCore : CustomYieldInstruction
    {
        // TODO: Wait for login
        // TODO: Check if this works properly, and optionally add "Success" result

        readonly float timeLimit;
        float timeRemaining;

        public WaitForNgioCore(float timeLimit = 15f)
        {
            this.timeLimit = timeRemaining = timeLimit;
        }

        /// <summary>Indicates if coroutine should be kept suspended.</summary>
        public override bool keepWaiting
        {
            get
            {
                if (CoreComponent.ngioCore == null) CoreComponent.ngioCore = Object.FindObjectOfType<CoreComponent>(); //? Every frame?
                bool result = CoreComponent.ngioCore && CoreComponent.ngioCore.isReady(); // TODO: Null propagation operator in C# 6.0
                if (result)
                {
                    return false;
                }
                else
                {
                    timeRemaining -= Time.deltaTime;
                    if (timeRemaining <= 0)
                    {
                        TimerExpired();
                        return false;
                    }
                }
                return true;
            }
        }

        void TimerExpired()
        {
            string problem = (CoreComponent.ngioCore) ? "initialize" : "find";
            NgDebug.LogWarningFormat("<b>{0}</b> didn't {1} the <i>CoreComponent</i> in {2} seconds.", GetType().Name, problem, timeLimit);
        }

    }
}
