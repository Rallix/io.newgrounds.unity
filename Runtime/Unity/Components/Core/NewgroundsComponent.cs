﻿using System;
using System.Collections;
using System.Net.NetworkInformation;
using io.newgrounds.results.Gateway;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace io.newgrounds.unity
{
    /// <summary>
    ///     A component which makes sure the core is properly initialized before allowing the derived components to operate.
    ///     Therefore, in <see cref="StartReady" /> method it's safe to assume ngio_core is in
    ///     the scene and properly linked.
    /// </summary>
    public abstract class NewgroundsComponent : MonoBehaviour // TODO: + abstract 'NgDataComponent'
    {
        /// <summary> How long should the component keep trying to initialize. </summary>
        const float TimeLimitToInitialize = 30f; // TODO: To settings

        bool endedWithError;
        bool hasStarted;

        float timeRemaining;
        protected bool waitForLogin;
        protected CoreComponent NgioCore
        {
            get { return CoreComponent.ngioCore; }
            private set { CoreComponent.ngioCore = value; }
        }

        /// <summary>
        ///     Similar to <see cref="Start" />,  this method is guaranteed to run only
        ///     when a component is linked to an active and ready Newgrounds <see cref="CoreComponent" />. Does not guarantee that
        ///     the user is logged in.
        /// </summary>
        protected virtual void StartReady() { }

        /// <summary> Get the instance ID of linked Core Component. </summary>
        /// <returns>Linked core component's instance ID.</returns>
        public int GetLinkedCoreID()
        {
            if (NgioCore == null) return -1;
            return NgioCore.GetInstanceID();
        }
        
        void Start()
        {
            Restart();
        }

        /// <summary> Look for a core component again. </summary>
        public void Restart()
        {
            timeRemaining = TimeLimitToInitialize;
            hasStarted = false;
            endedWithError = false;
            StartCoroutine(InitializeComponentOnReady());
        }

        void Update()
        {
            if (!hasStarted) timeRemaining -= Time.deltaTime;
        }

        IEnumerator InitializeComponentOnReady()
        {
            // TODO: yield return new WaitForNgioCore();
            yield return new WaitUntil(() =>
            {
                // NgDebug.LogFormat("<b>{0}</b>: Waiting for core initialization: {1}", GetType().Name, timeRemaining);
                // Try find if none is present
                if (NgioCore == null) NgioCore = FindObjectOfType<CoreComponent>();

                // Check if it isn't taking too long
                if (timeRemaining <= 0)
                {
                    TimerExpired();
                    return true;
                }
                return NgioCore && NgioCore.isReady();
            });
            if (!endedWithError)
            {
                hasStarted = true;
                StartReady();
            }
        }

        void OnEnable()
        {
            if (endedWithError) Restart(); // Restart
        }

        void TimerExpired()
        {
            string problem = NgioCore ? "initialize" : "find";
            NgDebug.LogWarningFormat("<b>{0}</b> didn't {1} the <i>Core</i> in {2} seconds, disabling this.\n" +
                                     "Re-enable the component to try again.", GetType().Name, problem, TimeLimitToInitialize);
            StopCoroutine(InitializeComponentOnReady());
            enabled = false;
            endedWithError = true;
            timeRemaining = 0;
        }
    }
}
