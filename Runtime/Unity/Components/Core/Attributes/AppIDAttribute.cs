﻿using UnityEngine;

namespace io.newgrounds.unity
{
    public class AppIdAttribute : PropertyAttribute { }
}
