using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace io.newgrounds.unity
{
    public static class UserStats
    {

        const RegexOptions Options = RegexOptions.Singleline | RegexOptions.ExplicitCapture;
        static readonly Regex UserStatsRegex = new Regex(@"<div class=""pod"" id=""userstats"">(?<userStats>.*)<div .*?id=""sortable_sections"">", Options);

        public enum Stats
        {

            Username,
            UserID,

            Quote,

            Age,
            Gender,
            Occupation,
            Education,
            Location,
            Calendar,

            Level,
            ExpPoints,
            ExpPointsNextLevel,
            ExpRank,
            VotePower,

            Rank,
            GlobalRank,
            Blams,
            Saves,

            Whistle,

            Throphies,
            Medals,
            Supporter,
            Gear

        }

        public static string GetCommunityStat(string pageText, Stats stat)
        {
            string userStatsArea = UserStatsRegex.Match(pageText).Groups["userStats"].Value;

            var regex = new Regex(string.Empty); // default in case it isn't found
            switch (stat)
            {                

                case Stats.Quote:
                    regex = new Regex(@"<blockquote.*?>(?<stat>.*?)</blockquote>");
                    break;

                case Stats.Age:
                    regex = PersonStatRegex("user", @"(?<stat>\d+).*?,?");
                    break;
                case Stats.Gender:
                    regex = PersonStatRegex("user", @"\d+, (?<stat>.*?)");
                    break;
                case Stats.Occupation:
                    regex = PersonStatRegex("wrench");
                    break;
                case Stats.Education:
                    regex = PersonStatRegex("graduation-cap");
                    break;
                case Stats.Location:
                    regex = PersonStatRegex("map-marker");
                    break;
                case Stats.Calendar:
                    regex = PersonStatRegex("calendar");
                    break;

                case Stats.Level:
                    regex = NgStatRegex("Level");
                    break;
                case Stats.ExpPoints:
                    regex = NgStatRegex("Exp Points", @"(?<stat>.*?) / .*?");
                    break;
                case Stats.ExpPointsNextLevel:
                    regex = NgStatRegex("Exp Points", @".*? / (?<stat>.*?)");
                    break;
                case Stats.ExpRank:
                    regex = NgStatRegex("Exp Rank");
                    break;
                case Stats.VotePower:
                    regex = NgStatRegex("Vote Power", @"(?<stat>.*?) .*?");
                    break;

                case Stats.Rank:
                    regex = NgStatRegex("Rank");
                    break;
                case Stats.GlobalRank:
                    regex = NgStatRegex("Global Rank");
                    break;
                case Stats.Blams:
                    regex = NgStatRegex("Blams");
                    break;
                case Stats.Saves:
                    regex = NgStatRegex("Saves");
                    break;

                case Stats.Whistle:
                    regex = NgStatRegex("Whistle");
                    break;

                case Stats.Throphies:
                    regex = NgStatRegex("Trophies");
                    break;
                case Stats.Medals:
                    regex = NgStatRegex("Medals");
                    break;
                case Stats.Supporter:
                    regex = NgStatRegex("Supporter");
                    break;
                case Stats.Gear:
                    regex = NgStatRegex("Gear");
                    break;
            }

            Match match = regex.Match(userStatsArea);
            string result = match.Success ? match.Groups[1].Value.Trim() : string.Empty;
            return result;
        }

        /// <summary> The aura chosen by the user. </summary>
        public enum Aura
        {
            BlankSlate = 0,

            Audiophile,
            Musician,

            GameDeveloper,
            Gamer,

            Animator,
            Filmmaker,
            MovieBuff,

            Artist,
            ArtLover,

            Melancholy,

            Programmer,

            VoiceActor,

            Writer,
            Reader,

            Moderator
        }
        /// <summary> Mapping CSS class specifiers to corresponding user auras. </summary>
        static readonly Dictionary<string, Aura> AuraMap = new Dictionary<string, Aura>
        {
            // TODO: Values are correct, but aura isn't inspected in the code above^
                ["a1"] = Aura.Audiophile,
                ["a2"] = Aura.Musician,
                ["g1"] = Aura.GameDeveloper,
                ["g2"] = Aura.Gamer,
                ["m1"] = Aura.Animator,
                ["m2"] = Aura.Filmmaker,
                ["m3"] = Aura.MovieBuff,
                ["i1"] = Aura.Artist,
                ["i2"] = Aura.ArtLover,
                ["d1"] = Aura.BlankSlate,
                ["d2"] = Aura.Melancholy,
                ["g3"] = Aura.Programmer,
                ["a3"] = Aura.VoiceActor,
                ["w1"] = Aura.Writer,
                ["w2"] = Aura.Reader,
                ["mod"] = Aura.Moderator,

        };

        /// <summary>Creates a <see cref="Regex"/> for one of the stats from the infobox with grey icons.</summary>
        /// <param name="name">The name of a stat to look for.</param>
        /// <param name="capture">Define a single capture group between the <dd> and </dd> tags.</param>
        /// <returns>The regex to use when searching.</returns>
        static Regex PersonStatRegex(string name, string capture = @"(?<stat>.*?)")
        {
            if (Regex.Matches(capture, @"\(\?\<").Count != 1) NgDebug.Log("There should be exactly one capture group in " + name + " stat.");
            return new Regex(string.Concat(@"<p>\s*?<i class=""fa fa-", name, @"""></i>\s+?", capture, @"\s*?</p>"));
        }

        /// <summary>Creates a <see cref="Regex"/> for one of the experience-related stats.</summary>
        /// <param name="name">The name of a stat to look for.</param>
        /// <param name="capture">Define a single capture group between the <dd> and </dd> tags.</param>
        /// <returns>The regex to use when searching.</returns>
        static Regex NgStatRegex(string name, string capture = @"(?<stat>.*?)")
        {
            if (Regex.Matches(capture, @"\(\?\<").Count != 1) NgDebug.Log("There should be exactly one capture group in " + name + " stat.");
            return new Regex(string.Concat(@"<dt .*?>", name, @":</dt>\s+<dd>", capture, @"</dd>"));
        }

    }
}
