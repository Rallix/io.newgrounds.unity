﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using io.newgrounds.objects;
using UnityEngine;
using UnityEngine.Networking;
using UniGifLibrary;
using UnityEngine.Events;

namespace io.newgrounds.unity
{
    [Serializable] public class Texture2DEvent : UnityEvent<Texture2D> { }

    /// <summary>
    ///     Using player's username and <see cref="UnityWebRequest"/> class, this component will fetch and parse their userpage and retrieve account
    ///     info. If you only need to know the username, the <see cref="UserComponent"/> already provides you with the information.
    /// </summary>
    [AddComponentMenu("Newgrounds/Spyglass", 100), RequireComponent(typeof(UserComponent))]
    public class SpyglassComponent : NewgroundsComponent
    {
        // TODO: Reverse – let UserComponent have Spyglass as an optional component | HideInInspector
        // TODO: Better structure, separate UI tests and functionality. Remove 'Test'. | Split into two components

        /// <summary>Use this to force override username instead of getting the actual username.</summary>
        [SerializeField, Tooltip("Enter a username to get stats about")] string usernameOverride = default;

        /// <summary>The icon of the specified user.</summary>
        Texture2D userIcon;

        public Texture2D Icon => userIcon ? userIcon : Texture2D.whiteTexture;

        /// <summary>A dictionary with the user stats. Initialized with <see cref="string.Empty" />.</summary>
        public Dictionary<UserStats.Stats, string> UserStats { get; } = Enum.GetValues(typeof(UserStats.Stats))
                                                                            .Cast<UserStats.Stats>()
                                                                            .ToDictionary(stat => stat, stat => string.Empty);
        // Called when the player info has been successfully downloaded
        public delegate void StatsLoaded();
        /// <summary> All textual information about the player is loaded. </summary>
        public event StatsLoaded OnStatsLoaded;

        /// <summary> Called when the icon finishes loading. </summary>
        [Space] public Texture2DEvent onIconLoaded;

        /// <summary> A component mantaining the user's login and logout.</summary>
        UserComponent userComponent;

        /// <summary>
        ///     Access the userpage through a proxy adding CORS headers to cicumvent the Same Origin Policy.
        ///     This allows you to access other pages via scripts, e.g. to load user's icon or parse stats.
        /// </summary>
        /// <remarks>
        ///     See the <see href="https://docs.unity3d.com/Manual/webgl-networking.html">WebGL Networking</see> page for more info.
        /// </remarks>
        const string CorsProxy = "https://api.allorigins.win/raw?url=https://"; // TODO: Replace with a project setting, with a default and 'Do not change' warning

        /// <summary> A protocol prefix which prepends the <see cref="CorsProxy"/> on the WebGL platform. </summary>
        static readonly string UrlPrefix = (Application.platform == RuntimePlatform.WebGLPlayer) ? CorsProxy : "https://";

        /// <summary>The template link to the Newgrounds userpage. Replace {username} with an username of your choice.</summary>
        static string BaseUserpageUrl { get; } = $"{UrlPrefix}{{username}}.newgrounds.com/";

        void Awake()
        {
            if (onIconLoaded == null) onIconLoaded = new Texture2DEvent();
        }

        public void OnLoggedIn(user user)
        {
            UserStats[unity.UserStats.Stats.Username] = userComponent.Username;
            UserStats[unity.UserStats.Stats.UserID] = userComponent.User.id.ToString();
            LoadStats(string.IsNullOrWhiteSpace(usernameOverride) ? user.name : usernameOverride);
        }

        public void LoadStats(string username)
        {
            string userpageUrl = Regex.Replace(BaseUserpageUrl, "{username}", username);
            StartCoroutine(LoadPage(userpageUrl, ProcessStats));
        }

        /// <summary>
        ///     Loads all text from an url and after it finishes downloading, calls a method to parse the text.
        /// </summary>
        /// <param name="url">Url to load text from.</param>
        /// <param name="pageProcessing">Method to use to parse the obtained text.</param>
        /// <returns>All source code text.</returns>
        static IEnumerator LoadPage(string url, params Action<string>[] pageProcessing)
        {
            UnityWebRequest userpage;
            try
            {
                userpage = UnityWebRequest.Get(url);
            }
            catch (UriFormatException)
            {
                url = Regex.Replace(BaseUserpageUrl, "{username}", UserComponent.DefaultName.ToLower());
                NgDebug.LogWarningFormat("The url you are trying to load is invalid. Loading default user <color=orange>{0}</color> from:\n{1}.", UserComponent.DefaultName, url);
                userpage = UnityWebRequest.Get(url);
            }

            yield return userpage.SendWebRequest();
            
            #if UNITY_2020_2_OR_NEWER
                if (userpage.result == UnityWebRequest.Result.ConnectionError)
            #else
                if (userpage.isNetworkError)
            #endif
            {
                NgDebug.LogError($"<b>{userpage.error}</b>: {url}");
            }
            else 
            {
                foreach (Action<string> processor in pageProcessing)
                {
                    processor(userpage.downloadHandler.text); 
                }
            }
        }

        /// <summary> Obtains all user stats from the full user page. </summary>
        /// <param name="pageText"> The downloaded user profile page. </param>
        void ProcessStats(string pageText)
        {
            List<UserStats.Stats> statsEnum = Enum.GetValues(typeof(UserStats.Stats))
                                                  .Cast<UserStats.Stats>()
                                                  .Except(new[] {unity.UserStats.Stats.Username, unity.UserStats.Stats.UserID}).ToList();
            foreach (UserStats.Stats stat in statsEnum) UserStats[stat] = unity.UserStats.GetCommunityStat(pageText, stat);
            ProcessIcon(pageText); // start loading icon as well
            OnStatsLoaded?.Invoke();
        }

        /// <summary> Obtains the user avatar the full user page. </summary>
        /// <param name="pageText"> The downloaded user profile page. </param>
        void ProcessIcon(string pageText)
        {
            var iconRegex = new Regex(@"<a class=""user-header-icon"".*?background-image: url\('.*?\/\/(?<icon>.*?)\?.*?'\)"">");
            Match iconMatch = iconRegex.Match(pageText);
            if (iconMatch.Success)
            {
                string iconUrl = $"{UrlPrefix}{iconMatch.Groups[1].Value}";
                if (Uri.IsWellFormedUriString(iconUrl, UriKind.Absolute))
                {
                    StartCoroutine(DownloadIcon(iconUrl));
                }
                else NgDebug.LogFormat("The icon url isn't valid: {0}", iconUrl);
            }
            else
            {
                NgDebug.Log($"The user icon couldn't be found where expected: <b>{iconRegex}</b>");
            }
        }

        IEnumerator DownloadIcon(string iconUrl)
        {
            using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(iconUrl))
            {
                yield return request.SendWebRequest();
                
                #if UNITY_2020_2_OR_NEWER
                    if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
                #else
                    if (request.isNetworkError || request.isHttpError)
                #endif
                {
                    NgDebug.LogWarning(request.error);
                }
                else
                {
                    Texture2D texture = null;
                    //! Unity doesn't natively support GIF textures; only JPG and PNG
                    if (iconUrl.EndsWith(".gif", StringComparison.OrdinalIgnoreCase))
                    {
                        byte[] gifBytes = request.downloadHandler.data;
                        yield return StartCoroutine(UniGif.GetTextureListCoroutine(gifBytes, (gifTexList, loopCount, width, height) =>
                        {
                            texture = gifTexList[0].m_texture2d; // *Static* gif texture only
                        }));
                    }
                    else
                    {
                        //! JPG and PNG
                        texture = DownloadHandlerTexture.GetContent(request);
                    }
                    userIcon = texture;
                    onIconLoaded.Invoke(texture);
                }
            }
        }

        /// <summary>Checks whether the user has a stat.</summary>
        public bool HasStat(UserStats.Stats stat)
        {
            return UserStats.ContainsKey(stat) && !string.IsNullOrEmpty(UserStats[stat]);
        }

        protected override void StartReady()
        {
            base.StartReady();
            
            userComponent = GetComponent<UserComponent>();
            userComponent.OnLoggedIn += OnLoggedIn;
        }
    }
}
