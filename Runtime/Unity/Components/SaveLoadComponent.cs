﻿using System;
using UnityEngine;
using UnityEngine.Android;

namespace io.newgrounds.unity
{
    /// <summary> A component for saving the data for later use and loading them when needed. </summary>
    // [AddComponentMenu("Newgrounds/SaveLoad", 50)]
    [HelpURL("https://docs.unity3d.com/ScriptReference/PlayerPrefs.html")]
    // [Obsolete("This component is obsolete because all it did was achieved by 3p0ch's MetaGame Handler.")]
    public class SaveLoadComponent : NewgroundsComponent
    {
        // All functionality removed for now.
        
        // See 3p0ch's solution instead
        // https://3p0ch.newgrounds.com/news/post/1086279

        protected override void StartReady()
        {
            base.StartReady();
            
            NgDebug.Log("Start ready");
        }

    }
}
