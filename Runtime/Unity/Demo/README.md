### Showcase scene

This is a selection screen taking you to different component demos, made for the demo.  
It's not included in the final Newgrounds.io package, because all the demo scenes would have to be needlessly added to the Build Settings.

After the showcase scene is no longer necessary, all the used icons can be moved from *Resources* folder to *Editor/Resources* to avoid including them in builds.