﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace io.newgrounds.unity.samples
{
    [System.Serializable]
    struct SceneButton
    {

        public Button button;
        [Scene] public string scene;

		public SceneButton(Button button, string scene) {
			this.button = button;
			this.scene = scene;
		}
    }

    /// <summary>A container for buttons which load a specified scene when clicked.</summary>
    public class LoadOnClick : MonoBehaviour
    {

        [SerializeField] List<SceneButton> sceneButtons = default;

        void Awake()
        {
            AddListenersOrDisable();
        }

        void AddListenersOrDisable()
        {
            foreach (SceneButton sceneButton in sceneButtons)
            {
                if (!sceneButton.button) continue;

                string scene = sceneButton.scene;
                if (!string.IsNullOrEmpty(scene))
                {
                    sceneButton.button.onClick.AddListener(() => { SceneManager.LoadScene(scene); });
                }
                else
                {
                    sceneButton.button.interactable = false;
                }
            }
        }

    }
}
